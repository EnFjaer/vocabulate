﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using Vocabulate.QuizEngine;
using Vocabulate.QuizEngine.ChoosersAndGenerators;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.StyleHelper;

namespace Vocabulate.CommonViewModels.ViewModels
{
    public abstract class QuizViewModel : MvxViewModel, IQuizViewModel
    {

        private bool _lockState;

        private IQuizMaster _quizMaster;

        private string _quizName;

        private string _question;

        private List<IAlternativeViewModel> _alternatives;

        private MvxCommand<IAlternativeViewModel> _guessCommand;

        private GuessState _currentGuessState;

        private IAnswerReceiver _answerReceiver;

        private IQuizQuestion _quizQuestion;

        private bool _showAlternatives;

        public void Setup(IQuestionCollection quizCollection, IQuizMaster quizMaster, IAnswerReceiver answerReceiver, int nrOfAlternatives = 8,
            int waitTime = 800, bool hideAlternatives = false)
        {
            WaitTime = waitTime;

            QuizName = quizCollection.CollectionName;

            _answerReceiver = answerReceiver;

            _quizMaster = quizMaster;

            _lockState = false;

            HideAlternatives = hideAlternatives;

            Update();
        }

        public void Setup(IQuestionCollection quizCollection, IAlternativeGenerator alternativeGenerator,
            IQuestionChooser questionChooser, IAnswerReceiver answerReceiver, int nrOfAlternatives = 8, int waitTime = 800, bool hideAlternatives = false)
        {
            var quizMaster = new QuizMaster(
                alternativeGenerator,
                questionChooser,
                nrOfAlternatives);

            Setup(quizCollection, quizMaster, answerReceiver, nrOfAlternatives, waitTime, hideAlternatives);
        }

        public void Setup(IQuestionCollection quizCollection, IAnswerReceiver answerReceiver, int nrOfAlternatives = 8, int waitTime = 800, bool hideAlternatives = false)
        {
            var questionChooser = new QuestionChooser(quizCollection);
            Setup(quizCollection, new AlternativeGenerator(questionChooser, quizCollection), questionChooser, answerReceiver, nrOfAlternatives, waitTime, hideAlternatives);
        }

        public void Setup(IQuestionCollection quizCollection, int nrOfAlternatives = 8, int waitTime = 800, bool hideAlternatives = false)
        {
            var questionChooser = new QuestionChooser(quizCollection);
            Setup(quizCollection, new AlternativeGenerator(questionChooser, quizCollection), questionChooser, new DummyAnswerReceiver(), nrOfAlternatives, waitTime, hideAlternatives);
        }

        public bool HideAlternatives { get; set; }

        public GuessState CurrentGuessState {
            get => _currentGuessState;
            private set
            {
                _currentGuessState = value;
                OnCurrenGuessStateChange();
                RaisePropertyChanged(() => CurrentGuessState);
            }
        }

        public string QuizName {
            get => _quizName;
            private set
            {
                _quizName = value;
                RaisePropertyChanged(() => QuizName);
            }
        }

        public string Question
        {
            get => _question;
            private set
            {
                _question = value;
                RaisePropertyChanged(() => Question);
            }
        } 

        public virtual List<IAlternativeViewModel> Alternatives {
            get {
                if (!_showAlternatives)
                {
                    IAlternativeViewModel emptyModel = new AlternativeViewModel(string.Empty, false);
                    return Enumerable.Repeat(emptyModel, _alternatives.Count).ToList();
                }

                return _alternatives;
            }
            private set
            {
                _alternatives = value;
                RaisePropertyChanged(() => Alternatives);
            }
        }

        public int WaitTime { get; set; }

        public void Update()
        {
            _quizQuestion = _quizMaster.GetQuestion();
            Question = _quizQuestion.Question;

            _showAlternatives = !HideAlternatives;

            Alternatives = WrapAlternativeInViewModel(_quizQuestion);

            CurrentGuessState = GuessState.NoGuess;
        }

        public MvxCommand<IAlternativeViewModel> GuessCommand => _guessCommand = _guessCommand ?? 
            new MvxCommand<IAlternativeViewModel>(async (item) => await Guess(item), (item)  => !_lockState);

        public async Task Guess(IAlternativeViewModel guess)
        {
            if (!_showAlternatives)
            {
                _showAlternatives = true;
                RaisePropertyChanged(() => Alternatives);
                return;
            }

            _lockState = true;
            _answerReceiver?.RegisterAnswer(_quizQuestion, guess.Name);

            guess.Guessed();
            CurrentGuessState = guess.IsCorrect ? GuessState.Correct : GuessState.Incorrect;
            await UpdateAfterMilliseconds(WaitTime);
            _lockState = false;
        }

        private async Task UpdateAfterMilliseconds(int ms)
        {
            await Task.Delay(ms);
            Update();
        }

        private List<IAlternativeViewModel> WrapAlternativeInViewModel(IQuizQuestion quizQuestion)
        {
            var alternativeViewmodels = new List<IAlternativeViewModel>();
            for (var i = 0; i < quizQuestion.Alternatives.Count; i++)
            {
                alternativeViewmodels.Add(new AlternativeViewModel(quizQuestion.Alternatives[i], i == quizQuestion.CorrectAnswerIndex));
            }
            return alternativeViewmodels;
        }

        private void OnCurrenGuessStateChange()
        {
            foreach (var alternative in Alternatives)
            {
                alternative.SetGuessState(CurrentGuessState);
            }
        }
    }
}
