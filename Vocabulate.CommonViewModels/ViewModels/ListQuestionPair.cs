﻿using System;
using System.Linq;
using MvvmCross.Core.ViewModels;
using Vocabulate.QuizEngine;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.QuestionHistory;

namespace Vocabulate.CommonViewModels.ViewModels
{
    public class ListQuestionPair : MvxViewModel, IListQuestionPair
    {
        private readonly IAnswerHistory _answerHistory;

        private const int MaxHistoryUseForConfidence = 5;

        private readonly IQuestionPair _questionPair;
        private readonly IQuestionPair _flippedQuestionPair;

        public ListQuestionPair(IQuestionPair questionPair, IAnswerHistory answerHistory)
        {
            _answerHistory = answerHistory;
            Question = questionPair.Question;
            Answer = questionPair.Answer;
            _questionPair = new QuestionPair(questionPair);
            _flippedQuestionPair = new QuestionPair(questionPair);
            _flippedQuestionPair.IsFlipped = !_questionPair.IsFlipped;
        }

        public double ConfidenceQuestion => ConfidenceCalculator.CalculateConfidence(_questionPair, _answerHistory, MaxHistoryUseForConfidence);
        public double ConfidenceAnswer => ConfidenceCalculator.CalculateConfidence(_flippedQuestionPair, _answerHistory, MaxHistoryUseForConfidence);
        public string Question { get; }
        public string Answer { get; }
    }
}
