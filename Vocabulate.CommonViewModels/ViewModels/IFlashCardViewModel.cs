﻿using System.Collections.Generic;
using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.UI;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.QuestionHistory;

namespace Vocabulate.CommonViewModels.ViewModels
{
    public interface IFlashCardViewModel : IMvxViewModel
    {
        void Setup(IQuestionCollection questionCollection, IAnswerHistory answerHistory);

        string Question { get; }
        string Transliteration { get; }
        string Definition { get; }

        string WellKnownString { get; }
        string SomewhatKnownString { get; }
        string NotKnownString { get; }

        MvxObservableCollection<WordDefinitionViewModel> WordDefinitions { get; }

        ICommand WellKnownCommand { get; }
        ICommand SomewhatKnownCommand { get; }
        ICommand NotKnownCommand { get; }
        ICommand ChangeDefinition { get; }

        MvxColor QuestionBackgroundColor { get; }
        MvxColor QuestionTextColor { get; }

        MvxColor TransliterationBackgroundColor { get; }
        MvxColor TransliterationTextColor { get; }

        MvxColor DefinitionBackgroundColor { get; }
        MvxColor DefinitionTextColor { get; }

        MvxColor WellKnownBackgroundColor { get; }
        MvxColor WellKnownTextColor { get; }
        MvxColor SomewhatKnownBackgroundColor { get; }
        MvxColor SomewhatKnownTextColor { get; }
        MvxColor NotKnownBackgroundColor { get; }
        MvxColor NotKnownTextColor { get; }
    }
}
