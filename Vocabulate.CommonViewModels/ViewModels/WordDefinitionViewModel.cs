﻿using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.UI;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.StyleHelper.ColorSchemes;

namespace Vocabulate.CommonViewModels.ViewModels
{
    public class WordDefinitionViewModel : MvxViewModel
    {
        public WordDefinitionViewModel(IQuestionPair questionPair)
        {
            Question = questionPair.Question;
            Transliteration = questionPair.Transliteration;
            Definition = questionPair.Answer;
            Unit = "Unit " + questionPair.Tier;
        }

        public string Question { get; }
        public string Transliteration { get; }
        public string Definition { get; }
        public string Unit { get; }

        public MvxColor BackgroundColor => ColorConstants.AlternativeNoGuessBackground;
        public MvxColor TextColor => ColorConstants.AlternativeNoGuessText;
    }
}
