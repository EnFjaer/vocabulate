﻿using System.Collections.Generic;
using MvvmCross.Core.ViewModels;
using Vocabulate.StyleHelper;

namespace Vocabulate.CommonViewModels.ViewModels
{
    public interface IQuizViewModel :IMvxViewModel
    {
        GuessState CurrentGuessState { get; }

        string Question { get; }

        List<IAlternativeViewModel> Alternatives { get;  }

        int WaitTime { get; }

        MvxCommand<IAlternativeViewModel> GuessCommand { get; }

        void Update();
    }
}