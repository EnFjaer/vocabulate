﻿namespace Vocabulate.CommonViewModels.ViewModels
{
    public interface IListQuestionPair
    {
        double ConfidenceQuestion { get; }
        double ConfidenceAnswer { get; }
        string Question { get; }
        string Answer { get; }

    }
}
