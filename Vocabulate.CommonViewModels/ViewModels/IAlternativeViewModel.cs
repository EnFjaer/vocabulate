﻿using Vocabulate.StyleHelper;

namespace Vocabulate.CommonViewModels.ViewModels
{
    public interface IAlternativeViewModel
    {
        bool IsCorrect { get; }

        string Name { get; }

        void SetGuessState(GuessState guessState);

        AlternativeState State { get; }

        void Guessed();
    }
}
