﻿using MvvmCross.Core.ViewModels;
using Vocabulate.StyleHelper;

namespace Vocabulate.CommonViewModels.ViewModels
{
    public class AlternativeViewModel : MvxViewModel, IAlternativeViewModel
    {
        private bool _guessed;

        private GuessState _guessState;

        public AlternativeViewModel(string alternative, bool isCorrect)
        {
            Name = alternative;
            IsCorrect = isCorrect;
            _guessState = GuessState.NoGuess;
        }

        public AlternativeState State
        {
            get
            {
                switch (_guessState)
                {
                    case GuessState.NoGuess:
                        return AlternativeState.NoGuess;
                    case GuessState.Correct:
                        return IsCorrect
                            ? AlternativeState.CorrectGuessCorrectAlternative
                            : AlternativeState.CorrectGuessIncorrectAlternative;
                    case GuessState.Incorrect:
                        if (_guessed)
                            return AlternativeState.IncorrectGuess;
                        return IsCorrect
                            ? AlternativeState.IncorrectGuessCorrectAlternative
                            : AlternativeState.IncorrectGuessIncorrectAlternative;
                    default:
                        return AlternativeState.NoGuess;
                }
            }
        }

        public string Name { get; }
        public bool IsCorrect { get; }

        public void SetGuessState(GuessState guessState)
        {
            _guessState = guessState;
            RaisePropertyChanged(() => State);
        }

        public void Guessed()
        {
            _guessed = true;
        }
    }
}
