﻿using System.Collections.Generic;
using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.UI;
using Vocabulate.QuizEngine;
using Vocabulate.QuizEngine.ChoosersAndGenerators;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.QuestionHistory;
using Vocabulate.StyleHelper.ColorSchemes;

namespace Vocabulate.CommonViewModels.ViewModels
{
    public class FlashCardViewModel : MvxViewModel, IFlashCardViewModel
    {
        private IQuizMaster _quizMaster;
        private IAnswerReceiver _answerReceiver;
        private IQuizQuestion _quizQuestion;
        private IQuestionCollection _questionCollection;
        private WordDefinitionViewModel _wordDefinition;
        private bool _showAnswer;

        protected WordDefinitionViewModel WordDefinition
        {

            get { return _wordDefinition; }

            set
            {
                _wordDefinition = value;
                RaiseAllPropertiesChanged();
            }
        }

        private enum Confidence { Unknown, SomewhatKnown, Known}

        public string Question => WordDefinition.Question;
        public string Transliteration => WordDefinition.Transliteration;
        public string Definition => WordDefinition.Definition;

        public string WellKnownString => "Yes";
        public string SomewhatKnownString => "Maybe";
        public string NotKnownString => "No";

        public bool ShowAnswer
        {
            get => _showAnswer;
            private set
            {
                _showAnswer = value;
                RaisePropertyChanged(() => ShowAnswer);
                RaisePropertyChanged(() => ShowFlashCard);
            }
        }

        public bool ShowFlashCard => !ShowAnswer;

        public MvxObservableCollection<WordDefinitionViewModel> WordDefinitions { get; private set; }

        public ICommand WellKnownCommand => new MvxCommand(() => Answer(Confidence.Known));
        public ICommand SomewhatKnownCommand => new MvxCommand(() => Answer(Confidence.SomewhatKnown));
        public ICommand NotKnownCommand => new MvxCommand(() => Answer(Confidence.Unknown));
        public ICommand ChangeDefinition => new MvxCommand<WordDefinitionViewModel>(definition => WordDefinition = definition);
        public ICommand ShowAnswerCommand => new MvxCommand(DoShowAnswer);

        public MvxColor QuestionBackgroundColor => ColorConstants.QuestionCorrectGuessBackground;
        public MvxColor QuestionTextColor => ColorConstants.QuestionCorrectGuessText;

        public MvxColor TransliterationBackgroundColor => ColorConstants.ZabaanLightColor;
        public MvxColor TransliterationTextColor => ColorConstants.QuestionCorrectGuessText;

        public MvxColor DefinitionBackgroundColor => ColorConstants.ZabaanColor;
        public MvxColor DefinitionTextColor => ColorConstants.QuestionCorrectGuessText;

        public MvxColor WellKnownBackgroundColor => ColorConstants.KnownWordColor;
        public MvxColor WellKnownTextColor => ColorConstants.AlternativeCorrectGuessCorrectAlternativeText;
        public MvxColor SomewhatKnownBackgroundColor => ColorConstants.SomewhatKnownWordColor;
        public MvxColor SomewhatKnownTextColor => ColorConstants.AlternativeNoGuessText;
        public MvxColor NotKnownBackgroundColor => ColorConstants.UnknownWordColor;
        public MvxColor NotKnownTextColor => ColorConstants.AlternativeIncorrectGuessText;

        public void Setup(IQuestionCollection questionCollection, IAnswerHistory answerHistory)
        {
            var questionChooser = new QuestionChooser(questionCollection, answerHistory, rollTrigger:100);
            _quizMaster = new QuizMaster(new AlternativeGenerator(questionChooser, questionCollection), questionChooser);
            _answerReceiver = questionChooser;
            _questionCollection = questionCollection;
            WordDefinitions = new MvxObservableCollection<WordDefinitionViewModel>();
            Update();
        }

        public void Update()
        {
            _quizQuestion = _quizMaster.GetQuestion();
            /* WordDefinitions.Clear();
            foreach (var questionPair in _questionCollection.QuestionPairs.FindAll(
                _ => _.Question == _quizQuestion.Question))
            {
                WordDefinitions.Add(new WordDefinitionViewModel(questionPair));
            }

            RaisePropertyChanged(() => WordDefinitions);
            */
            WordDefinition = new WordDefinitionViewModel(_quizQuestion);
            ShowAnswer = false;
        }

        private void Answer(Confidence confidence)
        {
            switch (confidence)
            {
                case Confidence.Known:
                    _answerReceiver?.RegisterAnswer(_quizQuestion, _quizQuestion.Answer);
                    break;
                case Confidence.Unknown:
                    _answerReceiver?.RegisterAnswer(_quizQuestion, string.Empty);
                    break;
                default:
                    break;
            }

            Update();
        }

        private void DoShowAnswer()
        {
            ShowAnswer = true;
        }
    }
}
