﻿namespace Vocabulate.StyleHelper
{
    public enum GuessState
    {
        NoGuess,
        Correct,
        Incorrect
    }
}
