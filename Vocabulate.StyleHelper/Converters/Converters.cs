﻿using System;
using System.Globalization;
using MvvmCross.Platform.Converters;
using MvvmCross.Platform.UI;
using MvvmCross.Plugins.Color;
using Vocabulate.StyleHelper.ColorSchemes;

namespace Vocabulate.StyleHelper.Converters
{
    public class CorrectToAlternativeBackgroundConverter : MvxColorValueConverter<AlternativeState>
    {
        protected override MvxColor Convert(AlternativeState value, object parameter, CultureInfo culture)
        {
            MvxColor backgroundColor;

            switch (value)
            {
                case AlternativeState.NoGuess:
                    backgroundColor = ColorConstants.AlternativeNoGuessBackground;
                    break;
                case AlternativeState.CorrectGuessCorrectAlternative:
                    backgroundColor = ColorConstants.AlternativeCorrectGuessCorrectAlternativeBackground;
                    break;
                case AlternativeState.CorrectGuessIncorrectAlternative:
                    backgroundColor = ColorConstants.AlternativeCorrectGuessIncorrectAlternativeBackground;
                    break;
                case AlternativeState.IncorrectGuessCorrectAlternative:
                    backgroundColor = ColorConstants.AlternativeIncorrectGuessCorrectAlternativeBackground;
                    break;
                case AlternativeState.IncorrectGuessIncorrectAlternative:
                    backgroundColor = ColorConstants.AlternativeIncorrectGuessIncorrectAlternativeBackground;
                    break;
                case AlternativeState.IncorrectGuess:
                    backgroundColor = ColorConstants.AlternativeIncorrectGuessBackground;
                    break;
                default:
                    backgroundColor = ColorConstants.AlternativeNoGuessBackground;
                    break;
            }

            return backgroundColor;
        }
    }

    public class CorrectToAlternativeTextConverter : MvxColorValueConverter<AlternativeState>
    {
        protected override MvxColor Convert(AlternativeState value, object parameter, CultureInfo culture)
        {
            MvxColor textColor;

            switch (value)
            {
                case AlternativeState.NoGuess:
                     textColor = ColorConstants.AlternativeNoGuessText;
                    break;
                case AlternativeState.CorrectGuessCorrectAlternative:
                     textColor = ColorConstants.AlternativeCorrectGuessCorrectAlternativeText;
                    break;
                case AlternativeState.CorrectGuessIncorrectAlternative:
                     textColor = ColorConstants.AlternativeCorrectGuessIncorrectAlternativeText;
                    break;
                case AlternativeState.IncorrectGuessCorrectAlternative:
                     textColor = ColorConstants.AlternativeIncorrectGuessCorrectAlternativeText;
                    break;
                case AlternativeState.IncorrectGuessIncorrectAlternative:
                     textColor = ColorConstants.AlternativeIncorrectGuessIncorrectAlternativeText;
                    break;
                case AlternativeState.IncorrectGuess:
                     textColor = ColorConstants.AlternativeIncorrectGuessText;
                    break;
                default:
                    textColor = ColorConstants.AlternativeNoGuessText;
                    break;
            }
            return textColor;
        }
    }

    public class GuessStateToBackgroundColorConverter : MvxColorValueConverter<GuessState>
    {
        protected override MvxColor Convert(GuessState value, object parameter, CultureInfo culture)

        {
            MvxColor backgroundColor;

            switch (value)
            {
                case GuessState.Correct:
                    backgroundColor = ColorConstants.QuestionNoGuessBackground;
                    break;
                case GuessState.Incorrect:
                    backgroundColor = ColorConstants.QuestionIncorrectGuessBackground;
                    break;
                case GuessState.NoGuess:
                    backgroundColor = ColorConstants.QuestionCorrectGuessBackground;
                    break;
                default:
                    backgroundColor = ColorConstants.QuestionNoGuessBackground;
                    break;
            }

            return backgroundColor;
        }
    }

    public class GuessStateToTextColorConverter : MvxColorValueConverter<GuessState>
    {
        protected override MvxColor Convert(GuessState value, object parameter, CultureInfo culture)
        {
            MvxColor textColor;

            switch (value)
            {
                case GuessState.Correct:
                    textColor = ColorConstants.QuestionNoGuessText;
                    break;
                case GuessState.Incorrect:
                    textColor = ColorConstants.QuestionIncorrectGuessText;
                    break;
                case GuessState.NoGuess:
                    textColor = ColorConstants.QuestionCorrectGuessText;
                    break;
                default:
                    textColor = ColorConstants.QuestionNoGuessText;
                    break;
            }

            return textColor;
        }
    }

    public class FalseToTrueConverter : MvxValueConverter<bool, bool>
    {
        protected override bool Convert(bool value, System.Type targetType, object parameter,
    System.Globalization.CultureInfo culture)
        {
            return !value;
        }
    }

    public class ConfidenceToBackgroundConverter : MvxColorValueConverter<double>
    {
        protected override MvxColor Convert(double value, object parameter, CultureInfo culture)
        {
            var minColor = ColorConstants.ConfidenceMinColor;
            var maxColor = ColorConstants.ConfidenceMaxColor;

            var confidenceColor = new MvxColor(
                (int)Math.Round(minColor.R * (1 - value) + maxColor.R * value),
                (int)Math.Round(minColor.G * (1 - value) + maxColor.G * value),
                (int)Math.Round(minColor.B * (1 - value) + maxColor.B * value)
                );

            return confidenceColor;
        }
    }
}
