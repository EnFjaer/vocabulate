﻿namespace Vocabulate.StyleHelper
{
    public enum AlternativeState
    {
        NoGuess,
        CorrectGuessIncorrectAlternative,
        CorrectGuessCorrectAlternative,
        IncorrectGuessCorrectAlternative,
        IncorrectGuessIncorrectAlternative,
        IncorrectGuess
    }
}
