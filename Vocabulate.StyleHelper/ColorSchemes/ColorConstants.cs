﻿using System;
using MvvmCross.Platform.UI;

namespace Vocabulate.StyleHelper.ColorSchemes
{
    public static class ColorConstants
    {
        private static IColorScheme _colorScheme;

        static ColorConstants()
        {
            _colorScheme = new BaseColorScheme();
        }

        public static void SetColorScheme(IColorScheme colorScheme)
        {
            _colorScheme = colorScheme;
        }

        public static MvxColor AlternativeNoGuessText => _colorScheme.AlternativeNoGuessText;
        public static MvxColor AlternativeNoGuessBackground => _colorScheme.AlternativeNoGuessBackground;

        public static MvxColor AlternativeCorrectGuessIncorrectAlternativeBackground
            => _colorScheme.AlternativeCorrectGuessIncorrectAlternativeBackground;

        public static MvxColor AlternativeCorrectGuessIncorrectAlternativeText
            => _colorScheme.AlternativeCorrectGuessIncorrectAlternativeText;

        public static MvxColor AlternativeCorrectGuessCorrectAlternativeBackground
            => _colorScheme.AlternativeCorrectGuessCorrectAlternativeBackground;

        public static MvxColor AlternativeCorrectGuessCorrectAlternativeText
            => _colorScheme.AlternativeCorrectGuessCorrectAlternativeText;

        public static MvxColor AlternativeIncorrectGuessCorrectAlternativeBackground
            => _colorScheme.AlternativeIncorrectGuessCorrectAlternativeBackground;

        public static MvxColor AlternativeIncorrectGuessCorrectAlternativeText
            => _colorScheme.AlternativeIncorrectGuessCorrectAlternativeText;

        public static MvxColor AlternativeIncorrectGuessIncorrectAlternativeBackground
            => _colorScheme.AlternativeIncorrectGuessIncorrectAlternativeBackground;

        public static MvxColor AlternativeIncorrectGuessIncorrectAlternativeText
            => _colorScheme.AlternativeIncorrectGuessIncorrectAlternativeText;

        public static MvxColor AlternativeIncorrectGuessBackground => _colorScheme.AlternativeIncorrectGuessBackground;
        public static MvxColor AlternativeIncorrectGuessText => _colorScheme.AlternativeIncorrectGuessText;

        public static MvxColor QuestionNoGuessBackground => _colorScheme.QuestionNoGuessBackground;
        public static MvxColor QuestionNoGuessText => _colorScheme.QuestionNoGuessText;
        public static MvxColor QuestionCorrectGuessBackground => _colorScheme.QuestionCorrectGuessBackground;
        public static MvxColor QuestionCorrectGuessText => _colorScheme.QuestionCorrectGuessText;
        public static MvxColor QuestionIncorrectGuessBackground => _colorScheme.QuestionIncorrectGuessBackground;
        public static MvxColor QuestionIncorrectGuessText => _colorScheme.QuestionIncorrectGuessText;

        public static MvxColor ZabaanColor => _colorScheme.ZabaanColor;
        public static MvxColor ZabaanDarkColor => _colorScheme.ZabaanDarkColor;
        public static MvxColor ZabaanLightColor => _colorScheme.ZabaanLightColor;

        public static MvxColor ConfidenceMinColor => _colorScheme.ConfidenceMinColor;
        public static MvxColor ConfidenceMaxColor => _colorScheme.ConfidenceMaxColor;

        public static MvxColor KnownWordColor => _colorScheme.KnownWordColor;
        public static MvxColor SomewhatKnownWordColor => _colorScheme.SomewhatKnownWordColor;
        public static MvxColor UnknownWordColor => _colorScheme.UnknownWordColor;
    }
}