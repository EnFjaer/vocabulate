﻿using MvvmCross.Platform.UI;

namespace Vocabulate.StyleHelper.ColorSchemes
{
    public class BaseColorScheme : IColorScheme
    {
        public MvxColor AlternativeNoGuessBackground => new MvxColor(72, 130, 145);
        public MvxColor AlternativeNoGuessText { get; } = new MvxColor(249, 240, 203);

        public MvxColor AlternativeCorrectGuessCorrectAlternativeBackground => new MvxColor(40, 180, 40);
        public MvxColor AlternativeCorrectGuessCorrectAlternativeText { get; } = new MvxColor(13, 11, 13);

        public MvxColor AlternativeCorrectGuessIncorrectAlternativeBackground => new MvxColor(70, 150, 70);
        public MvxColor AlternativeCorrectGuessIncorrectAlternativeText => AlternativeNoGuessText;
        public MvxColor AlternativeIncorrectGuessCorrectAlternativeBackground => new MvxColor(92, 150, 37);
        public MvxColor AlternativeIncorrectGuessCorrectAlternativeText => AlternativeCorrectGuessCorrectAlternativeText;

        public MvxColor AlternativeIncorrectGuessIncorrectAlternativeBackground => AlternativeNoGuessBackground;
        public MvxColor AlternativeIncorrectGuessIncorrectAlternativeText => AlternativeNoGuessText;
        public MvxColor AlternativeIncorrectGuessBackground => new MvxColor(180, 0, 0);
        public MvxColor AlternativeIncorrectGuessText => AlternativeNoGuessText;

        public MvxColor QuestionNoGuessBackground => new MvxColor(86, 78, 90);
        public MvxColor QuestionNoGuessText => AlternativeNoGuessText;
        public MvxColor QuestionCorrectGuessBackground => new MvxColor(86, 78, 90);
        public MvxColor QuestionCorrectGuessText => AlternativeNoGuessText;
        public MvxColor QuestionIncorrectGuessBackground => new MvxColor(86, 78, 90);
        public MvxColor QuestionIncorrectGuessText => AlternativeNoGuessText;
        public MvxColor ZabaanColor => new MvxColor(223, 96, 16);
        public MvxColor ZabaanDarkColor => new MvxColor(156, 67, 11);
        public MvxColor ZabaanLightColor => new MvxColor(232, 143, 87);
        public MvxColor ConfidenceMinColor => new MvxColor(200, 10, 10);
        public MvxColor ConfidenceMaxColor => new MvxColor(10, 200, 10);
        public MvxColor KnownWordColor => new MvxColor(20, 180, 20);
        public MvxColor SomewhatKnownWordColor => new MvxColor(180, 180, 180);
        public MvxColor UnknownWordColor => new MvxColor(180, 0, 0);
    }
}
