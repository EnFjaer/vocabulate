﻿using MvvmCross.Platform.UI;

namespace Vocabulate.StyleHelper.ColorSchemes
{
    public interface IColorScheme
    {
        MvxColor AlternativeNoGuessBackground { get; }
        MvxColor AlternativeNoGuessText { get; }
        MvxColor AlternativeCorrectGuessIncorrectAlternativeBackground { get; }
        MvxColor AlternativeCorrectGuessIncorrectAlternativeText { get; }
        MvxColor AlternativeCorrectGuessCorrectAlternativeBackground { get; }
        MvxColor AlternativeCorrectGuessCorrectAlternativeText { get; }
        MvxColor AlternativeIncorrectGuessCorrectAlternativeBackground { get; }
        MvxColor AlternativeIncorrectGuessCorrectAlternativeText { get; }
        MvxColor AlternativeIncorrectGuessIncorrectAlternativeBackground { get; }
        MvxColor AlternativeIncorrectGuessIncorrectAlternativeText { get; }
        MvxColor AlternativeIncorrectGuessBackground { get; }
        MvxColor AlternativeIncorrectGuessText { get; }
        MvxColor QuestionNoGuessBackground { get; }
        MvxColor QuestionNoGuessText { get; }
        MvxColor QuestionCorrectGuessBackground { get; }
        MvxColor QuestionCorrectGuessText { get; }
        MvxColor QuestionIncorrectGuessBackground { get; }
        MvxColor QuestionIncorrectGuessText { get; }
        MvxColor ZabaanColor { get; }
        MvxColor ZabaanDarkColor { get; }
        MvxColor ZabaanLightColor { get; }
        MvxColor ConfidenceMinColor { get; }
        MvxColor ConfidenceMaxColor { get; }
        MvxColor KnownWordColor { get; }
        MvxColor SomewhatKnownWordColor { get; }
        MvxColor UnknownWordColor { get; }
    }
}
