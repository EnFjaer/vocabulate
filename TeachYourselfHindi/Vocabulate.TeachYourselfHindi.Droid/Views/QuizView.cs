using Android.App;
using Android.OS;
using Android.Widget;
using MvvmCross.Binding.Droid.Views;
using Vocabulate.QuizEngine.ChoosersAndGenerators;
using Vocabulate.TeachYourselfHindi.Core;
using Vocabulate.TeachYourselfHindi.Core.ViewModels;

namespace Vocabulate.TeachYourselfHindi.Droid.Views
{
    [Activity(Label = "View for QuizView", Theme = "@android:style/Theme.NoTitleBar")]
    public class QuizView : Vocabulate.Common.Droid.Views.QuizView
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.QuizCross);

            Initialize(FindViewById<MvxGridView>(Resource.Id.quizGrid),
                FindViewById<LinearLayout>(Resource.Id.questionLayout), 
                FindViewById<TextView>(Resource.Id.collectionName), 
                FindViewById<TextView>(Resource.Id.questionView));
        }

        protected override void SetupViewmodel()
        {
            var quizViewModel = ViewModel as QuizViewModel;
            var questionChooser = new QuestionChooser(UnitHolder.Unit, UnitHolder.AnswerHistory);
            var alternativeGenerator = new AlternativeGenerator(questionChooser, UnitHolder.Unit, isNumbers:false);
            quizViewModel?.Setup(UnitHolder.Unit, alternativeGenerator, questionChooser, questionChooser, waitTime: 1000, hideAlternatives: true);
        }
    }
}