﻿using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;

namespace Vocabulate.TeachYourselfHindi.Droid.Views
{
    [Activity(Label = "Zabaan: Teach Yourself Hindi", Theme = "@android:style/Theme.NoTitleBar")]
    public class VocabListView : MvxActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.VocabListView);
        }
    }
}