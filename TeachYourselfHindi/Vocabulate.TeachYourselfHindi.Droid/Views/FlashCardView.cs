﻿using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;
using Vocabulate.CommonViewModels.ViewModels;
using Vocabulate.QuizEngine.ChoosersAndGenerators;
using Vocabulate.TeachYourselfHindi.Core;

namespace Vocabulate.TeachYourselfHindi.Droid.Views
{
    [Activity(Label = "Flash Card", Theme = "@android:style/Theme.NoTitleBar")]
    public class FlashCardView : MvxActivity
    {

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetupViewmodel();

            SetContentView(Resource.Layout.FlashCardView);
        }

        private void SetupViewmodel()
        {
            var flashCardViewModel = ViewModel as IFlashCardViewModel;

            flashCardViewModel?.Setup(UnitHolder.Unit, UnitHolder.AnswerHistory);

        }
    }
}