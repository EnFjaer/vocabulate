﻿using System.IO;
using Android.App;
using Android.OS;
using HockeyApp.Android;
using HockeyApp.Android.Metrics;
using MvvmCross.Droid.Views;
using Vocabulate.QuizEngine.Database;
using Vocabulate.TeachYourselfHindi.Core;
using Vocabulate.TeachYourselfHindi.Core.ViewModels;
using Vocabulate.TeachYourselfHindi.Droid.OsHelpers;
using Vocabulate.TeachYourselfHindi.Droid.Vocab;

namespace Vocabulate.TeachYourselfHindi.Droid.Views
{
    [Activity(Label = "Unit", Theme = "@android:style/Theme.NoTitleBar")]
    public class UnitPageView : MvxActivity
    {
        private const string AppIdentifier = "8176dc09906746b5bb07a89eee4d0d96";

        private const string AnswerDatabasePath = "./TeachYourselfHindiDatabase.db3";

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            InitHockeyApp();

            InitUnitHolder();

            SetupViewmodel();

            SetContentView(Resource.Layout.UnitPageView);
        }

        private void InitHockeyApp()
        {
            CrashManager.Register(this, AppIdentifier);
            MetricsManager.Register(Application, AppIdentifier);
        }

        private void SetupViewmodel()
        {
            var unitPageViewModel = ViewModel as UnitPageViewModel;
            unitPageViewModel?.Setup(new QuizActivityStarter<FlashCardView>(this));
        }

        private void InitUnitHolder()
        {
            if (!UnitHolder.IsInitialized)
            {
                var path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                path = Path.Combine(path, AnswerDatabasePath);
                var answerConnection = new AnswerDatabaseConnection(path);
                UnitHolder.Initialize(new AndroidUnitLoader(this), answerConnection, maxStored:1);
            }
        }
    }
}