using MvvmCross.Platform.Plugins;

namespace Vocabulate.TeachYourselfHindi.Droid.Bootstrap
{
    public class ColorPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Color.PluginLoader>
    {
    }
}