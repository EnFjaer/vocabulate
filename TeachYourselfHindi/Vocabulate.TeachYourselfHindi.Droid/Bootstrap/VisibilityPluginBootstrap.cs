using MvvmCross.Platform.Plugins;

namespace Vocabulate.TeachYourselfHindi.Droid.Bootstrap
{
    public class VisibilityPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Visibility.PluginLoader>
    {
    }
}