using MvvmCross.Droid.Views;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.TeachYourselfHindi.Core;
using Vocabulate.TeachYourselfHindi.Core.OsHelpers;

namespace Vocabulate.TeachYourselfHindi.Droid.OsHelpers
{
    public class QuizActivityStarter<T> : IQuizActivityStarter
    {
        private readonly MvxActivity _activity;

        public QuizActivityStarter(MvxActivity activity)
        {
            _activity = activity;
        }

        public void StartQuiz(IQuestionCollection unit)
        {
            _activity.StartActivity(typeof(T));
        }
    }
}