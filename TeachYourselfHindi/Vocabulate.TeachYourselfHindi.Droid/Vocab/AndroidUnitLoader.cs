using System.Collections.Generic;
using Android.Content;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.Extensions;
using Vocabulate.TeachYourselfHindi.Core.Vocab;
using Vocabulate.TeachYourselfHindi.Core.VocabImporters;

namespace Vocabulate.TeachYourselfHindi.Droid.Vocab
{
    public class AndroidUnitLoader : IUnitLoader
    {
        private readonly Context _context;

        public AndroidUnitLoader(Context context)
        {
            _context = context;
        }

        public List<IQuestionPair> Load(string filename, int unit)
        {
            var fileStream = _context.Assets.Open(filename);
            var vocabList = ImportFromCsv.GetVocabular(fileStream);
            var questionList = new List<IQuestionPair>();
            foreach(var vocab in vocabList)
                questionList.Add(vocab.ToWordFlashCard(unit));
            return questionList;
        }

        public List<IQuestionPair> Load(string filename)
        {
            return Load(filename, 0);
        }
    }
}