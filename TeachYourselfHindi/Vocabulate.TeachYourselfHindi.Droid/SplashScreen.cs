using Android.App;
using MvvmCross.Droid.Views;

namespace Vocabulate.TeachYourselfHindi.Droid
{
    [Activity(Label = "Zabaan: Teach Yourself Hindi", MainLauncher = true, Icon = "@drawable/icon")]
    public class SplashScreen : MvxSplashScreenActivity
    {
        public SplashScreen()
            : base(Resource.Layout.SplashScreen)
        {

        }
    }
}