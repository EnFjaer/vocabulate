﻿using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.IoC;
using Vocabulate.TeachYourselfHindi.Core.ViewModels;
namespace Vocabulate.TeachYourselfHindi.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            RegisterAppStart<UnitPageViewModel>();
        }
    }
}