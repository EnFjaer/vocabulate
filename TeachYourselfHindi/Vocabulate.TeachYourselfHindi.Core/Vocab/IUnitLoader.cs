﻿using System.Collections.Generic;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.TeachYourselfHindi.Core.Vocab
{
    public interface IUnitLoader
    {
        List<IQuestionPair> Load(string filename);
        List<IQuestionPair> Load(string filename, int unit);
    }
}
