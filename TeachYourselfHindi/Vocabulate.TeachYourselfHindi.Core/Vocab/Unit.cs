﻿using System.Collections.Generic;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.TeachYourselfHindi.Core.Vocab
{
    public class Unit : QuestionCollection
    {
        private readonly string _fileName;

        private readonly IUnitLoader _loader;

        private int _unitNr;

        public Unit(string unitName, string fileName, IUnitLoader unitLoader, int unitNr = 0) : base(unitName)
        {
            _fileName = fileName;
            _loader = unitLoader;
            _unitNr = unitNr;
        }

        protected override List<IQuestionPair> LoadQuestions()
        {
            return _loader.Load(_fileName, _unitNr);
        }
    }
}
