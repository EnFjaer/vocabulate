using System;
using System.Collections.Generic;
using System.Threading;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.QuestionHistory;
using Vocabulate.TeachYourselfHindi.Core.Vocab;

namespace Vocabulate.TeachYourselfHindi.Core
{
    public static class UnitHolder
    {
        private const int NrOfUnits = 18;
        private static object _lock = new Object();

        private static IQuestionCollection _unit;
        private static int _unitNr;

        private const int MaxNrOfAnswerStored = 5;

        private static IUnitLoader _unitLoader;

        public static IQuestionCollection Unit
        {
            get => _unit;

            set
            {
                var unitIndex = QuestionBook.QuestionCollections.IndexOf(value);
                if (unitIndex >= 0)
                {
                    _unitNr = unitIndex;
                }
                _unit = value;
            }
        }

        public static IQuestionCollection AllUnits => new CombinedCollection(QuestionBook.QuestionCollections);

        public static IQuestionCollection CurrentUnits => new CombinedCollection(QuestionBook.QuestionCollections.GetRange(0, UnitNr + 1));

        public static int UnitNr
        {
            get => _unitNr;
            set
            {
                _unitNr = value;
                Unit = QuestionBook.QuestionCollections[_unitNr];
            }
        }

        public static IAnswerHistory AnswerHistory { get; private set; }

        public static IQuestionBook QuestionBook { get; private set; }

        public static bool IsInitialized { get; private set; }


        public static void Initialize(IUnitLoader unitLoader, IAnswerDatabaseConnection answerDatabaseConnection = null, int maxStored = 5)
        {
            lock (_lock)
            {
                if (IsInitialized)
                    return;

                AnswerHistory = answerDatabaseConnection != null ? 
                    new AnswerHistory(new AnswerWriter(answerDatabaseConnection), new AnswerReader(answerDatabaseConnection), maxAnswersStored:maxStored) : 
                    new AnswerHistory(maxAnswersStored: maxStored);


                _unitLoader = unitLoader;
                SetupVocabBook("Teach yourself Hindi", "unit", NrOfUnits);

                IsInitialized = true;

                UnitNr = 0;
            }
        }

        public static void Uninitialize()
        {
            lock (_lock)
            {
                QuestionBook = null;
                IsInitialized = false;
            }
        }

        private static void SetupVocabBook(string bookName, string unitName, int nrOfUnits, int startUnit = 1)
        {
            var units = new List<IQuestionCollection>();

            for (var unitNr = startUnit; unitNr < startUnit + nrOfUnits; unitNr++)
            {
                var name = unitName + " " + unitNr;
                var filename = unitName + unitNr + ".csv";
                units.Add(new Unit(name, filename, _unitLoader, unitNr));
            }

            QuestionBook = new QuestionBook(bookName, units);
        }
    }
}