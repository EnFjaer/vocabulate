﻿using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.TeachYourselfHindi.Core.OsHelpers
{
    public interface IQuizActivityStarter
    {
        void StartQuiz(IQuestionCollection unit);
    }
}
