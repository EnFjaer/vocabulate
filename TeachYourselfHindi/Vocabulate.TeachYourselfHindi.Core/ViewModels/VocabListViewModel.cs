﻿using System.Collections.Generic;
using MvvmCross.Core.ViewModels;
using Vocabulate.CommonViewModels.ViewModels;

namespace Vocabulate.TeachYourselfHindi.Core.ViewModels
{
    public class VocabListViewModel : MvxViewModel
    {

        public List<IListQuestionPair> Vocab
        {
            get
            {
                UnitHolder.Unit.FlipQuestionAndAnswer = false;
                return CreateVocabList();
            }
        }

        public MvxCommand<IListQuestionPair> NoCommand => new MvxCommand<IListQuestionPair>(_ => { }, _ => true);

        public bool NotEnabled => false;

        private List<IListQuestionPair> CreateVocabList()
        {
            var vocabList = new List<IListQuestionPair>();
            foreach (var number in UnitHolder.Unit)
            {
                vocabList.Add(new ListQuestionPair(number, UnitHolder.AnswerHistory));
            }

            return vocabList;
        }
    }
}
