﻿using System.Collections.Generic;
using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.TeachYourselfHindi.Core.ViewModels
{
    public interface IUnitSelectionViewModel : IMvxViewModel
    {
        string BookName { get; }

        List<IQuestionCollection> QuestionCollections { get; }

        ICommand OpenUnitCommand { get; }
    }
}
