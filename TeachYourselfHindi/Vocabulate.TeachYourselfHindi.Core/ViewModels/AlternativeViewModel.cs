﻿
namespace Vocabulate.TeachYourselfHindi.Core.ViewModels
{
    public class AlternativeViewModel : Vocabulate.CommonViewModels.ViewModels.AlternativeViewModel
    {
        public AlternativeViewModel(string alternative, bool isCorrect) : base(alternative, isCorrect)
        {
        }
    }
}
