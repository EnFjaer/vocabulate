﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.UI;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.StyleHelper.ColorSchemes;
using Vocabulate.TeachYourselfHindi.Core.OsHelpers;

namespace Vocabulate.TeachYourselfHindi.Core.ViewModels
{
    public class UnitPageViewModel : MvxViewModel
    {
        private int _unitIndex;
        private int UnitIndex
        {
            get { return _unitIndex; }
            set
            {
                var maxIndex = _questionBook?.QuestionCollections.Count ?? 0;
                _unitIndex = value < maxIndex ? value : maxIndex - 1;
                _unitIndex = value >= 0 ? value : 0;
                UnitHolder.UnitNr = value;
                RaiseAllPropertiesChanged();
            }
        }

        private IQuestionBook _questionBook;

        private IQuizActivityStarter _quizActivityStarter;

        private IQuestionCollection CurrentUnit => _questionBook?.QuestionCollections[UnitIndex];

        public string NextString => "Next";
        public string PreviousString => "Prev";
        public string HindiToEnglishString => string.Empty;
        public string EnglishToHindiString => string.Empty;
        public string WordListString => "Unit words";
        public string UnitNameString => CurrentUnit?.CollectionName ?? string.Empty; 
        public string HindiToEnglishLevelString => LevelToString(GetHindiToEnglishLevel());
        public string EnglishToHindiLevelString => LevelToString(GetEnglishToHindiLevel());
        public string HindiToEnglishVocabSizeUnitString => GetVocabSizeUnitString(false);
        public string EnglishToHindiVocabSizeUnitString => GetVocabSizeUnitString(true);
        public string HindiToEnglishVocabSizeTotalString => GetVocabSizeUnitString(false,true);
        public string EnglishToHindiVocabSizeTotalString => GetVocabSizeUnitString(true,true);
        public string HindiToEnglishAllString => "With previous chapters";
        public string EnglishToHindiAllString => "With previous chapters";
        public bool HasNextUnit => UnitIndex < CurrentUnit.Count;
        public bool HasPreviousUnit => UnitIndex > 0;

        public MvxColor ButtonColor => ColorConstants.ZabaanLightColor;
        public MvxColor BackgroundColor => ColorConstants.ZabaanLightColor;

        public ICommand NextUnitCommand { get; private set; }
        public ICommand PreviousUnitCommand { get; private set; }
        public ICommand HindiToEnglishCommand { get; private set; }
        public ICommand EnglishToHindiCommand { get; private set; }
        public ICommand HindiToEnglishAllCommand { get; private set; }
        public ICommand EnglishToHindiAllCommand { get; private set; }
        public ICommand OpenVocabListCommand { get; private set; }

        public void Setup(IQuizActivityStarter quizActivityStarter)
        {
            _questionBook = UnitHolder.QuestionBook;

            _unitIndex = 0;

            _quizActivityStarter = quizActivityStarter;

            SetupCommands();
        }

        public override void Appeared()
        {
            RaiseAllPropertiesChanged();
        }

        private void SetupCommands()
        {
            NextUnitCommand = new MvxCommand(() => UnitIndex++, () => HasNextUnit);
            PreviousUnitCommand = new MvxCommand(() => UnitIndex--, () => HasPreviousUnit);
            HindiToEnglishCommand = new MvxCommand(() => StartQuiz(false));
            EnglishToHindiCommand = new MvxCommand(() => StartQuiz(true));
            HindiToEnglishAllCommand = new MvxCommand(() => StartCombinedQuiz(false));
            EnglishToHindiAllCommand = new MvxCommand(() => StartCombinedQuiz(true));
            OpenVocabListCommand = new MvxCommand(() => ShowViewModel<VocabListViewModel>());
        }

        private void StartQuiz(bool flip)
        {
            CurrentUnit.FlipQuestionAndAnswer = flip;
            UnitHolder.Unit = CurrentUnit;
            _quizActivityStarter.StartQuiz(CurrentUnit);
        }

        private void StartCombinedQuiz(bool flip)
        {
            UnitHolder.Unit = CurrentUnit;
            UnitHolder.Unit = UnitHolder.CurrentUnits;
            UnitHolder.CurrentUnits.FlipQuestionAndAnswer = flip;
            _quizActivityStarter.StartQuiz(UnitHolder.CurrentUnits);
        }

        private string LevelToString(double level)
        {
            return $"{level:0.00}";
        }

        private double GetUnitProfeciencyLevel(IList<IQuestionPair> questionPairs)
        {
            var nrAnsweredOnce = RelativeNumberOfAnswersAnsweredCorrectOnce(questionPairs);

            if (nrAnsweredOnce < 1)
                return nrAnsweredOnce;

            var nrAnsweredThreeTimes = ProgressToAtLeastThreeCorrectForEach(questionPairs);
            if (nrAnsweredThreeTimes < 1)
                return 1 + nrAnsweredThreeTimes;

            var nrAnsweredFiveLastCorrect = ProgressToFiveCorrectInARow(questionPairs);

            return 2 + nrAnsweredFiveLastCorrect;
        }

        private double GetEnglishToHindiLevel()
        {
            CurrentUnit.FlipQuestionAndAnswer = true;
            return GetUnitProfeciencyLevel(CurrentUnit);
        }

        private double GetHindiToEnglishLevel()
        {
            CurrentUnit.FlipQuestionAndAnswer = false;
            return GetUnitProfeciencyLevel(CurrentUnit);
        }

        private double RelativeNumberOfAnswersAnsweredCorrectOnce(IList<IQuestionPair> questionPairs)
        {
            int nrCorrectAtLeastOnce = 0;
            foreach (var questionPair in questionPairs)
            {
                if (UnitHolder.AnswerHistory.NumberOfCorrectAnswers(questionPair) > 0)
                    nrCorrectAtLeastOnce++;
            }

            return nrCorrectAtLeastOnce / (double) questionPairs.Count;
        }

        private double ProgressToAtLeastThreeCorrectForEach(IList<IQuestionPair> questionPairs)
        {
            int nrCorrect = 0;
            foreach (var questionPair in questionPairs)
            {
                var nrQuestionCorrect = UnitHolder.AnswerHistory.NumberOfCorrectAnswers(questionPair);
                nrCorrect = nrQuestionCorrect < 3 ? nrCorrect + nrQuestionCorrect : nrCorrect + 3;
            }

            return NumberOfMediumProficiency(questionPairs) / (double)(questionPairs.Count * 3);
        }

        private int NrOfLastGuessCorrect(IList<IQuestionPair> questionPairs)
        {
            int nrCorrect = 0;
            foreach (var questionPair in questionPairs)
            {
                var lastCorrect = UnitHolder.AnswerHistory.OutcomeOfLastAnswer(questionPair);
                if (lastCorrect)
                    nrCorrect++;
            }
            return nrCorrect;
        }

        private double ProgressToFiveCorrectInARow(IList<IQuestionPair> questionPairs)
        {

            return NumberOfHighProficiency(questionPairs) / (double)questionPairs.Count;
        }

        private int NumberOfHighProficiency(IList<IQuestionPair> questionPairs)
        {
            int nrCorrect = 0;
            foreach (var questionPair in questionPairs)
            {
                var lastOutcomes = UnitHolder.AnswerHistory.OutcomeOfLastAnswers(questionPair);

                if (lastOutcomes.Count >= 5 &&
                    lastOutcomes.GetRange(lastOutcomes.Count - 5, 5).All(_ => _))
                {
                    nrCorrect++;
                }
            }

            return nrCorrect;
        }

        private int NumberOfMediumProficiency(IList<IQuestionPair> questionPairs)
        {
            int nrCorrect = 0;
            foreach (var questionPair in questionPairs)
            {
                var nrQuestionCorrect = UnitHolder.AnswerHistory.NumberOfCorrectAnswers(questionPair);
                if (nrQuestionCorrect >= 3)
                    nrCorrect++;
            }

            return nrCorrect;
        }

        private string GetVocabSizeUnitString(bool flip = false, bool fullVocab = false)
        {
            var questionCollection = fullVocab ? UnitHolder.CurrentUnits : UnitHolder.Unit;

            questionCollection.FlipQuestionAndAnswer = flip;

            return $"{NrOfLastGuessCorrect(questionCollection)} " +
                   $"/ {questionCollection.Count}";
        }

    }
}
