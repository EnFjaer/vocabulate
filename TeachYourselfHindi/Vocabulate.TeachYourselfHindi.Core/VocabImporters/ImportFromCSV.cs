﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.TeachYourselfHindi.Core.VocabImporters
{
    public static class ImportFromCsv
    {

        public static List<FullDictionaryWord> GetVocabular(Stream file)
        {
            try
            {
                using (var sr = new StreamReader(file))
                {
                    var config = new CsvConfiguration();
                    config.Delimiter = "|";
                    var reader = new CsvReader(sr, config);

                    IEnumerable<FullDictionaryWord> records = reader.GetRecords<FullDictionaryWord>();

                    return records.ToList<FullDictionaryWord>();
                }
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
