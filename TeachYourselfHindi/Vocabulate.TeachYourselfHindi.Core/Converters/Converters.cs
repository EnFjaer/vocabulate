﻿using BaseConverter = Vocabulate.StyleHelper.Converters;

namespace Vocabulate.TeachYourselfHindi.Core.Converters
{
    public class CorrectToAlternativeBackgroundConverter : BaseConverter.CorrectToAlternativeBackgroundConverter
    {
    }

    public class CorrectToAlternativeTextConverter : BaseConverter.CorrectToAlternativeTextConverter
    {
    }

    public class GuessStateToBackgroundColorConverter : BaseConverter.GuessStateToBackgroundColorConverter
    {
    }

    public class GuessStateToTextColorConverter : BaseConverter.GuessStateToTextColorConverter
    {
    }

    public class FalseToTrueConverter : BaseConverter.FalseToTrueConverter
    {
    }

    public class ConfidenceToBackgroundConverter : BaseConverter.ConfidenceToBackgroundConverter
    {
    }
}
