﻿
using Android.Content;
using Android.Net;
using MvvmCross.Platform.Droid.Platform;
using MvvmCross.Plugins.WebBrowser;

namespace Vocabulate.Common.Droid
{
    public class AndroidWebBrowserTask : MvxAndroidTask, IMvxWebBrowserTask
    {
        public void ShowWebPage(string url)
        {
            var intent = new Intent(Intent.ActionView, Uri.Parse(url));
            StartActivity(intent);
        }
    }
}