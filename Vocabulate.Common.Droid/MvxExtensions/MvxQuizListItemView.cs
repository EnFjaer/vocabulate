using Android.Content;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding.Droid.Views;

namespace Vocabulate.Common.Droid.MvxExtensions
{
    public sealed class MvxQuizListItemView : MvxListItemView
    {
        public MvxQuizListItemView(Context context,
            IMvxLayoutInflaterHolder layoutInflaterHolder,
            object dataContext, ViewGroup parent,
            int templateId, int height) : base(context, layoutInflaterHolder, dataContext, parent, templateId)
        {
            var newLayoutParams = new LinearLayout.LayoutParams(
                             width: ViewGroup.LayoutParams.MatchParent,
                             height: height, 
                             weight: 1f);

            Content.LayoutParameters = newLayoutParams;
            Content.SetForegroundGravity(GravityFlags.Bottom);
        }
    }
}