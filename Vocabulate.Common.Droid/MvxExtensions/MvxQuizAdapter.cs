using Android.Content;
using Android.Views;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Binding.Droid.Views;

namespace Vocabulate.Common.Droid.MvxExtensions
{
    public class MvxQuizAdapter : MvxAdapter
    {
        public MvxQuizAdapter(Context context, IMvxAndroidBindingContext bindingContext) : base(context, bindingContext)
        {

        }

        public int ButtonHeight { get; set; }

        protected override IMvxListItemView CreateBindableView(object dataContext, ViewGroup parent, int templateId)
        {
            return new MvxQuizListItemView(Context, BindingContext.LayoutInflaterHolder, dataContext, parent, templateId, ButtonHeight);
        }
    }
}