using MvvmCross.Platform.Plugins;

namespace Vocabulate.Common.Droid.Bootstrap
{
    public class WebBrowserPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.WebBrowser.PluginLoader>
    {
    }
}