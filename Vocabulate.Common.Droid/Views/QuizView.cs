using System;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Binding.Droid.Views;
using MvvmCross.Droid.Views;
using Vocabulate.Common.Droid.MvxExtensions;
using Vocabulate.CommonViewModels.ViewModels;

namespace Vocabulate.Common.Droid.Views
{
    public abstract class QuizView : MvxActivity
    {
        private const double RelativeQuestionLayoutHeight = 0.3;
        private const double RelativeUnitNameHeight = 0.1;

        private int _nrOfAlternatives;

        private LinearLayout _questionLayout;
        private TextView _collectionNameTextView;
        private TextView _questionTextView;
        private MvxGridView _gridView;

        private int _questionLayoutHeight;
        private int _unitNameTextViewHeight;
        private int _questionTextViewHeight;
        private int _gridViewHeight;

        public int NrOfAlternatives {
            get { return _nrOfAlternatives; }
            set
            {
                _nrOfAlternatives = value;
                UpdateQuestionGridButtonHeight();
            }
        }

        protected void Initialize(MvxGridView gridViewId, LinearLayout questionLayoutId, TextView collectionNameTextViewId, TextView questionTextViewId)
        {
            SetupViewmodel();

            SetUiElements(gridViewId, questionLayoutId, collectionNameTextViewId, questionTextViewId);

            SetElementDimensions();

            SetupGridView();
            CreateBindingToNumberOfElements();
        }

        protected abstract void SetupViewmodel();

        private void SetUiElements(MvxGridView gridView, LinearLayout questionLayout, TextView collectionNameTextView,
            TextView questionTextView)
        {
            _gridView = gridView;
            _questionLayout = questionLayout;
            _collectionNameTextView = collectionNameTextView;
            _questionTextView = questionTextView;
        }


        private void SetElementDimensions()
        {
            CalculateElementHeights();
            SetHeights();
        }

        private void SetupGridView()
        {
            _gridView.Adapter = new MvxQuizAdapter(this, (MvxAndroidBindingContext)BindingContext);
            _gridView.SetGravity(GravityFlags.Bottom);
            _gridView.SetForegroundGravity(GravityFlags.Bottom);
        }

        private void CreateBindingToNumberOfElements()
        {
            var set = this.CreateBindingSet<QuizView, IQuizViewModel>();
            set.Bind(this).For(v => v.NrOfAlternatives).To(vm => vm.Alternatives.Count).TwoWay();
            set.Apply();
        }

        private void UpdateQuestionGridButtonHeight()
        {
            var nrOfCols = Math.Ceiling((double) NrOfAlternatives / 2);
            var colHeight = _gridViewHeight / nrOfCols;

            var adapter = (MvxQuizAdapter) _gridView.Adapter;
            adapter.ButtonHeight = Convert.ToInt32(colHeight);
        }

        private void CalculateElementHeights()
        {
            var height = GetRealHeight();

            _questionLayoutHeight = GetHeightGivenRatio(height, RelativeQuestionLayoutHeight);
            _gridViewHeight = height - _questionLayoutHeight;

            _unitNameTextViewHeight = GetHeightGivenRatio(_questionLayoutHeight, RelativeUnitNameHeight);
            _questionTextViewHeight = _questionLayoutHeight - _unitNameTextViewHeight;
        }

        private void SetHeights()
        {
            _questionLayout.LayoutParameters.Height = _questionLayoutHeight;
            _collectionNameTextView.LayoutParameters.Height = _unitNameTextViewHeight;
            _questionTextView.LayoutParameters.Height = _questionTextViewHeight;
            _gridView.LayoutParameters.Height = _gridViewHeight;
        }

        private int GetHeightGivenRatio(int height, double ratio)
        {
            var newHeight = height * ratio;
            return Convert.ToInt32(Math.Round(newHeight,0));
        }

        private int GetRealHeight()
        {
            int resourceId = Resources.GetIdentifier("status_bar_height", "dimen", "android");
            var totalHeight = Resources.DisplayMetrics.HeightPixels;

            if (resourceId > 0)
            {
                var statusBarHeight = Resources.GetDimensionPixelSize(resourceId);

                totalHeight = totalHeight - statusBarHeight;
            }

            return totalHeight;
        }
    }
}