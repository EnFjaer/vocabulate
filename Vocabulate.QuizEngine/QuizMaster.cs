﻿using Vocabulate.QuizEngine.ChoosersAndGenerators;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine
{
    public class QuizMaster : IQuizMaster
    {
        private readonly int _nrOfAlternatives;
        private readonly IAlternativeGenerator _alternativeGenerator;
        private readonly IQuestionChooser _questionChooser;

        public QuizMaster(
            IAlternativeGenerator alternativeGenerator,
            IQuestionChooser questionChooser,
            int nrOfAlternatives = 6)
        {
            _nrOfAlternatives = nrOfAlternatives;
            _alternativeGenerator = alternativeGenerator;
            _alternativeGenerator.NrOfAlternatives = _nrOfAlternatives;
            _questionChooser = questionChooser;
        }

        public IQuizQuestion GetQuestion()
        {
            var questionIndex = _questionChooser.GetQuestion();
            return GenerateQuizQuestion(questionIndex);
        }

        private QuizQuestion GenerateQuizQuestion(IQuestionPair questionPair)
        {
            var alternatives = _alternativeGenerator.GenerateAlternatives(questionPair);
            var correctAnswerIndex = _alternativeGenerator.FindCorrectAnswer(alternatives);
            return new QuizQuestion(questionPair, alternatives, correctAnswerIndex);
        }
    }
}
