﻿using System.Threading.Tasks;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.QuestionHistory
{
    public interface IAnswerWriter
    {
        Task<string> WriteAnswerAndRemoveOld(IQuizAnswer quizAnswer, int maxEqual);
    }
}
