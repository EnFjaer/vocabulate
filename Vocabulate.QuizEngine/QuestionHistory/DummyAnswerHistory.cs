﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.QuestionHistory
{
    public class DummyAnswerHistory : IAnswerHistory
    {
        public Task<bool> AddAnswer(IQuizAnswer quizAnswer)
        {
            return Task.FromResult(false);
        }

        public Task<bool> AddAnswers(IList<IQuizAnswer> quizAnswers)
        {
            return Task.FromResult(false);
        }

        public int NumberOfTimesAsked(IQuestionPair question)
        {
            return 0;
        }

        public int NumberOfCorrectAnswers(IQuestionPair question)
        {
            return 0;
        }

        public int NumberOfIncorrectAnswers(IQuestionPair question)
        {
            return 0;
        }

        public List<bool> OutcomeOfLastAnswers(IQuestionPair question)
        {
            return new List<bool>();
        }

        public bool OutcomeOfLastAnswer(IQuestionPair question)
        {
            return false;
        }

        public int NrQuestionsSinceLastAsked(IQuestionPair question)
        {
            return 0;
        }

        public DateTime LastTimeAsked(IQuestionPair question)
        {
            return DateTime.Now;
        }

        public int MaxAnswersStored => 0;
    }
}
