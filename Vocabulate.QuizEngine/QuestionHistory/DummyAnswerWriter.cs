﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.QuestionHistory
{
    public class DummyAnswerWriter : IAnswerWriter
    {
        public Task<string> WriteAnswer(IQuizAnswer quizAnswer)
        {
            return Task.FromResult("Dummy writer. No answer written");
        }

        public Task<string> WriteAnswers(List<IQuizAnswer> quizAnswers)
        {
            return Task.FromResult("Dummy writer. No answer written");
        }

        public Task<string> WriteAnswerAndRemoveOld(IQuizAnswer quizAnswer, int maxEqual)
        {
            return Task.FromResult("Dummy writer. No answer written");
        }
    }
}
