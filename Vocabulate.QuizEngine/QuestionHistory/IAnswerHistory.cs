﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.QuestionHistory
{
    public interface IAnswerHistory
    {
        Task<bool> AddAnswer(IQuizAnswer quizAnswer);
        Task<bool> AddAnswers(IList<IQuizAnswer> quizAnswers);

        int NumberOfTimesAsked(IQuestionPair question);
        int NumberOfCorrectAnswers(IQuestionPair question);
        int NumberOfIncorrectAnswers(IQuestionPair question);
        List<bool> OutcomeOfLastAnswers(IQuestionPair question);
        bool OutcomeOfLastAnswer(IQuestionPair question);
        int NrQuestionsSinceLastAsked(IQuestionPair question);
        DateTime LastTimeAsked(IQuestionPair question);

        int MaxAnswersStored { get; }
    }
}
