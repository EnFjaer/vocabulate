﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.QuestionHistory
{
    public class DummyAnswerReader : IAnswerReader
    {
        public int NrOfAnswers => 0;
        public IList<IQuizAnswer> GetAllQuizAnswers() => new List<IQuizAnswer>();
    }
}
