﻿using System;
using System.Collections.Generic;
using SQLite;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.Extensions;

namespace Vocabulate.QuizEngine.QuestionHistory
{
    public class QuizAnswerEntry : IQuizAnswer
    {
        public QuizAnswerEntry(IQuizAnswer answer)
        {
            AnsweredCorrectly = answer.AnsweredCorrectly;
            CorrectAnswerIndex = answer.CorrectAnswerIndex;
            Alternatives = answer.Alternatives;
            Question = answer.Question;
            Answer = answer.Answer;
            Tier = answer.Tier;
            IsFliped = answer.IsFlipped;
            GivenAnswer = answer.GivenAnswer;
            Transliteration = answer.Transliteration;
            TimeAnswered = answer.TimeAnswered;
        }

        public QuizAnswerEntry() : this(new NullQuizAnswer())
        {
            
        }

        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public string Question { get; set; }
        public string Answer { get; set; }
        public string Transliteration { get; set; }
        public bool IsFlipped { get; set; }

        [Ignore]
        public List<string> Alternatives
        {
            get => AlternativesBlobbed.DeserializeList<string>();
            set => AlternativesBlobbed = value.Serialize();
        }

        public string AlternativesBlobbed { get; set; }

        public int CorrectAnswerIndex { get; set; }

        public bool AnsweredCorrectly { get; set; }
        public string GivenAnswer { get; set; }
        public DateTime TimeAnswered { get; set; }

        public int Tier { get; set; }

        public bool IsFliped { get; set; }

        public override string ToString()
        {
            return
                $"[Question: ID={ID}, Question={Question}, Answer={Answer}, WasCorrect={AnsweredCorrectly}, TimeAnswered={TimeAnswered}]";
        }

        public T CopyTo<T>() where T : IQuizAnswer, new()
        {
            return new T()
            {
                Question = this.Question,
                Answer = this.Answer,
                AnsweredCorrectly = this.AnsweredCorrectly,
                CorrectAnswerIndex = this.CorrectAnswerIndex,
                Alternatives = this.Alternatives,
                TimeAnswered = this.TimeAnswered
            };
        }
    }
}