﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vocabulate.QuizEngine.QuestionHistory
{
    public interface IAnswerDatabaseConnection
    {
        Task<string> InsertUpdateData(QuizAnswerEntry data);
        Task<string> InsertUpdateListOfData(IList<QuizAnswerEntry> dataList);
        int GetTotalNumberOfEntries();
        void CreateDatabase();
        void DropDatabase();
        Task<IList<QuizAnswerEntry>> GetAllWhereKeyEquals(string key, string value);
        Task<IList<QuizAnswerEntry>> GetAllWhereKeysEquals(IList<string> keys, IList<string> values);
        IList<QuizAnswerEntry> GetAllEntries();
        Task<string> RemoveEntries(IList<QuizAnswerEntry> dataList);
    }
}
