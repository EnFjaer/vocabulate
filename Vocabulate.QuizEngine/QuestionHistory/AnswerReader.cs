﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.QuestionHistory
{
    public class AnswerReader : IAnswerReader
    {
        private IAnswerDatabaseConnection _connection;

        public AnswerReader(IAnswerDatabaseConnection connection)
        {
            _connection = connection;
        }

        public int NrOfAnswers => _connection.GetTotalNumberOfEntries();

        public IList<IQuizAnswer> GetAllQuizAnswers()
        {
            return  GetAllQuizAnswers<QuizAnswer>();
        }

        public IList<IQuizAnswer> GetAllQuizAnswers<T>() where T : IQuizAnswer, new()
        {
            var listOfAnswers = new List<IQuizAnswer>();

            var listOfAllEntries = _connection.GetAllEntries();

            foreach (var answer in listOfAllEntries)
            {
                listOfAnswers.Add(answer.CopyTo<T>());
            }
            return listOfAnswers;
        }
    }
}