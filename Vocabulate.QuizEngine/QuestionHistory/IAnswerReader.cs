﻿using System.Collections.Generic;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.QuestionHistory
{
    public interface IAnswerReader
    {
        int NrOfAnswers { get; }

        IList<IQuizAnswer> GetAllQuizAnswers();
    }
}