﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.QuestionHistory
{
    public class AnswerHistory : IAnswerHistory
    {
        private readonly Dictionary<Tuple<string, string>, List<IQuizAnswer>> _answerDictionary;

        private readonly Dictionary<IQuestionPair, int> _lastAskedAgo;

        private readonly IAnswerWriter _answerWriter;

        private readonly IAnswerReader _answerReader;

        public AnswerHistory(IAnswerWriter answerWriter = null, IAnswerReader answerReader = null, int maxAnswersStored = int.MaxValue)
        {
            MaxAnswersStored = maxAnswersStored;

            _answerDictionary = new Dictionary<Tuple<string, string>, List<IQuizAnswer>>();

            _lastAskedAgo = new Dictionary<IQuestionPair, int>();

            _answerWriter = answerWriter ?? new DummyAnswerWriter();

            _answerReader = answerReader ?? new DummyAnswerReader();

            AddAnswers(_answerReader.GetAllQuizAnswers(), false).Wait();
        }

        public async Task<bool> AddAnswer(IQuizAnswer quizAnswer)
        {
            await AddAnswerToDictionary(quizAnswer);
            ResetLastAskedAgoAndIncreaseAllOther(quizAnswer);
            return true;
        }

        public async Task<bool> AddAnswers(IList<IQuizAnswer> quizAnswers) 
        {
            return await AddAnswers(quizAnswers, true);
        }

        public int NrQuestionsSinceLastAsked(IQuestionPair question)
        {
            if (_lastAskedAgo.ContainsKey(question))
                return _lastAskedAgo[question];

            return int.MaxValue;
        }

        public DateTime LastTimeAsked(IQuestionPair question)
        {
            var answers = GetAnswersToQuestion(question);
            if (answers.Count == 0)
                return DateTime.MinValue;

            return answers.Select(_ => _.TimeAnswered).Max();
        }

        public int MaxAnswersStored { get; }

        public int NumberOfTimesAsked(IQuestionPair question)
        {
            return GetAnswersToQuestion(question).Count;
        }

        public int NumberOfCorrectAnswers(IQuestionPair question)
        {
            var answers = GetAnswersToQuestion(question);
            return answers.Count(_ => _.AnsweredCorrectly);
        }

        public int NumberOfIncorrectAnswers(IQuestionPair question)
        {
            var answers = GetAnswersToQuestion(question);
            return answers.Count(_ => !_.AnsweredCorrectly);
        }

        public List<bool> OutcomeOfLastAnswers(IQuestionPair question)
        {
            var answers = GetAnswersToQuestion(question);
            return answers.Select(_ => _.AnsweredCorrectly).ToList();
        }

        public bool OutcomeOfLastAnswer(IQuestionPair question)
        {
            var lastOutcomes = OutcomeOfLastAnswers(question);

            return lastOutcomes.Count > 0 ? lastOutcomes[lastOutcomes.Count - 1] : false;
        }

        private async Task<bool> AddAnswers(IList<IQuizAnswer> quizAnswers, bool writeToDatabase)
        {
            foreach (var quizAnswer in quizAnswers)
                await AddAnswerToDictionary(quizAnswer, writeToDatabase);

            return true;
        }

        private static Tuple<string, string> QuizQuestionAsTuple(IQuestionPair question)
        {
            return new Tuple<string, string>(question.Question, question.Answer);
        }

        private async Task<bool> AddAnswerToDictionary(IQuizAnswer answer, bool writeToDatabase = true)
        {
            var questionTuple = QuizQuestionAsTuple(answer);
            if (_answerDictionary.ContainsKey(questionTuple))
                _answerDictionary[questionTuple].Add(answer);
            else
            {
                _answerDictionary.Add(questionTuple,new List<IQuizAnswer>{answer});
            }
            if(writeToDatabase)
                await _answerWriter.WriteAnswerAndRemoveOld(answer, MaxAnswersStored);
            RemoveIfTooManyAnswers(answer);
            return true;
        }

        private List<IQuizAnswer> GetAnswersToQuestion(IQuestionPair question)
        {
            var questionTuple = QuizQuestionAsTuple(question);
            if (_answerDictionary.ContainsKey(questionTuple))
            {
                return _answerDictionary[questionTuple];
            }

            return new List<IQuizAnswer>();
        }

        private void RemoveIfTooManyAnswers(IQuestionPair question)
        {
            var answers = GetAnswersToQuestion(question);

            for (var i = 0; i < answers.Count - MaxAnswersStored; i++)
            {
                _answerDictionary[QuizQuestionAsTuple(question)].Remove(answers[i]);
            }
        }

        private void ResetLastAskedAgoAndIncreaseAllOther(IQuestionPair question)
        {
            var keys = _lastAskedAgo.Keys.ToList();
            foreach (var key in keys)
            {
                _lastAskedAgo[key]++;
            }

            if (_lastAskedAgo.ContainsKey(question))
                _lastAskedAgo[question] = 0;
            else
                _lastAskedAgo.Add(question, 0);
        }
    }
}
