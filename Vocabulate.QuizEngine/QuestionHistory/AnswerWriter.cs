﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.QuestionHistory
{
    public class AnswerWriter : IAnswerWriter
    {
        private IAnswerDatabaseConnection _connection;

        public AnswerWriter(IAnswerDatabaseConnection connection)
        {
            _connection = connection;
        }

        public async Task<string> WriteAnswer(IQuizAnswer quizAnswer)
        {
            var entry = new QuizAnswerEntry(quizAnswer);

            return await _connection.InsertUpdateData(entry);
        }

        public async Task<string> WriteAnswers(IList<IQuizAnswer> quizAnswers)
        {
            var entries = quizAnswers.Select(answer => new QuizAnswerEntry(answer)).ToList();

            return await _connection.InsertUpdateListOfData(entries);
        }

        public async Task<string> WriteAnswerAndRemoveOld(IQuizAnswer quizAnswer, int maxEqual)
        {
            var result = await WriteAnswer(quizAnswer);

            var duplicates = await _connection.GetAllWhereKeysEquals(new List<string>() {"Question", "Answer"},
                new List<string>() {quizAnswer.Question, quizAnswer.Answer});

            if (duplicates.Count > maxEqual)
            {
                var duplicatesToDelete = (duplicates as List<QuizAnswerEntry>).GetRange(0, duplicates.Count - maxEqual);
                await _connection.RemoveEntries(duplicatesToDelete);
            }

            return result;
        }
    }
}