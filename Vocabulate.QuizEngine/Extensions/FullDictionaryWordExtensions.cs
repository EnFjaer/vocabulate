﻿using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.Extensions
{
    public static class FullDictionaryWordExtensions
    {

        public static IQuestionPair ToWordFlashCard(this FullDictionaryWord word, int unit = 0)
        {
            string definition = word.PrimaryTranslation + "\n\n" + word.SecondaryTranslations;

            return new QuestionPair(question:word.Word, answer:definition, transliteration:word.Transliteration, tier:unit, isFlipped:false);
        }
    }
}
