﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Vocabulate.QuizEngine.Extensions
{
    public static class ListExtensions
    {
        private static Random rnd = new Random();

        private static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            
            while (n > 1)
            {
                int k = (rnd.Next(0, n) % n);
                n--;
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static string Serialize<T>(this IList<T> list)
        {
            using (var textWriter = new StringWriter())
            {
                XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                ns.Add("", "");
                using (XmlWriter writer =
                    XmlWriter.Create(textWriter, new XmlWriterSettings {OmitXmlDeclaration = true}))
                {
                    new XmlSerializer(typeof(List<T>)).Serialize(writer, list, ns);
                }
                return textWriter.ToString();
            }
        }

        public static List<T> DeserializeList<T>(this string serializedString)
        {
            var serializer = new XmlSerializer(typeof(List<T>));

            var stream = GenerateStreamFromString(serializedString);
            var listAsObject = serializer.Deserialize(stream);
            return listAsObject as List<T>;
        }
    }
}
