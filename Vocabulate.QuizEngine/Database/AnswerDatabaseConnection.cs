﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using SQLite;
using Vocabulate.QuizEngine.QuestionHistory;

namespace Vocabulate.QuizEngine.Database
{
    public class AnswerDatabaseConnection : IAnswerDatabaseConnection
    {
        private readonly string _path;

        public AnswerDatabaseConnection(string path)
        {
            _path = path;
            CreateDatabase();
        }

        public async Task<string> InsertUpdateData(QuizAnswerEntry data)
        {
            try
            {
                var db = new SQLiteAsyncConnection(_path);
                var insertOk = await db.InsertAsync(data);
                if(insertOk != 0)
                    await db.UpdateAsync(data);
                
                return "Single data file inserted or updated";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }

        public async Task<string> InsertUpdateListOfData(IList<QuizAnswerEntry> dataList)
        {
            try
            {
                var db = new SQLiteAsyncConnection(_path);
                var insertOk = await db.InsertAllAsync(dataList);
                if (insertOk != 0)
                    await db.UpdateAllAsync(dataList);
                return "List of data inserted or updated";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }

        public int GetTotalNumberOfEntries()
        {
            try
            {
                var db = new SQLiteConnection(_path);
                var count = db.ExecuteScalar<int>("SELECT Count(*) FROM QuizAnswerEntry");

                return count;
            }
            catch (SQLiteException ex)
            {
                Debug.WriteLine(ex.Message);
                return -1;
            }
        }

        public void CreateDatabase()
        {
            try
            {
                var connection = new SQLiteConnection(_path);
                connection.CreateTable<QuizAnswerEntry>();
                Debug.WriteLine(connection.ToString());
            }

            catch (SQLiteException ex)
            {
                Debug.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public void DropDatabase()
        {
            try
            {
                var connection = new SQLiteAsyncConnection(_path);
                var tmp = connection.DropTableAsync<QuizAnswerEntry>().Result;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
        }

        public async Task<IList<QuizAnswerEntry>> GetAllWhereKeyEquals(string key, string value)
        {
            var db = new SQLiteAsyncConnection(_path);
            return await db.QueryAsync<QuizAnswerEntry>($"Select * FROM QuizAnswerEntry WHERE {key} = '{value}'");
        }

        public async Task<IList<QuizAnswerEntry>> GetAllWhereKeysEquals(IList<string> keys, IList<string> values)
        {
            var db = new SQLiteAsyncConnection(_path);

            var query = "Select * FROM QuizAnswerEntry WHERE ";

            for (var i = 0; i < keys.Count; i++)
            {
                if (i > 0)
                    query += " AND ";
                query += $"{keys[i]} = '{values[i]}'";
            }

            return await db.QueryAsync<QuizAnswerEntry>(query);
        }

        public IList<QuizAnswerEntry> GetAllEntries()
        {
            var db = new SQLiteConnection(_path);
            return db.Query<QuizAnswerEntry>($"Select * FROM QuizAnswerEntry");
        }

        public async Task<string> RemoveEntries(IList<QuizAnswerEntry> dataList)
        {
            try
            {
                var db = new SQLiteAsyncConnection(_path);
                foreach (var data in dataList)
                {
                    await db.DeleteAsync(data);
                }

                return "Delete ok";
            }
            catch (SQLiteException ex)
            {
                return ex.Message;
            }
        }
    }
}