﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vocabulate.QuizEngine.Analytics;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.QuestionHistory;

namespace Vocabulate.QuizEngine.ChoosersAndGenerators
{
    public class FlashCardQuestionChooser : IQuestionChooser
    {
        private readonly IQuestionCollection _questionCollection;
        private readonly IAnswerHistory _answerHistory;

        public FlashCardQuestionChooser(IQuestionCollection questionCollection, IAnswerHistory answerHistory = null,
            IAnalyticsService analyticsService = null)
        {
            _questionCollection = questionCollection;
            _answerHistory = answerHistory;
        }

        public IQuestionPair GetQuestion()
        {
            throw new NotImplementedException();
        }

        public int CurrentTier { get; }
    }
}
