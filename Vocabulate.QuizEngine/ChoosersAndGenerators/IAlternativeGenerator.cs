﻿using System.Collections.Generic;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.ChoosersAndGenerators
{
    public interface IAlternativeGenerator
    {
        List<string> GenerateAlternatives(IQuestionPair question);

        IQuestionCollection QuestionCollection { set; }

        int NrOfAlternatives { set; }

        int FindCorrectAnswer(IList<string> alternatives);
    }
}
