﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.ChoosersAndGenerators.Helpers
{
    public class QuestionCollectionProbabilityHolder
    {
        private Dictionary<IQuestionPair, double> _questionCollectionToProbability;
        private IQuestionCollection _questionCollection;

        public QuestionCollectionProbabilityHolder(IQuestionCollection questionCollection)
        {
            _questionCollection = questionCollection;
            PopulateProbabilityDictionary();
        }

        public void ApplyProbabilityFactors(IProbabilityCalculator calculator)
        {
            var newProbabilities = calculator.CalculateProbability(_questionCollection);
            foreach (var question in _questionCollection)
            {
                _questionCollectionToProbability[question] *= newProbabilities[question];
            }
        }

        public IQuestionPair GetRandomQuestion(int useNMostProbible = 10)
        {
            if (_questionCollection.Count == 0)
                return default(IQuestionPair);

            if (useNMostProbible <= 0 || useNMostProbible > _questionCollection.Count)
                useNMostProbible = _questionCollection.Count;

            var candidatesAsList = SelectNMostProbibleQuestions(useNMostProbible);

            var candidatesAsDictionary = _questionCollectionToProbability.Where(_ => candidatesAsList.Contains(_.Key)).ToDictionary(_ => _.Key, _ => _.Value);

            return RandomElementChooser.ElementFromProbabilityDensity<IQuestionPair>(candidatesAsDictionary);
        }

        private void PopulateProbabilityDictionary()
        {
            _questionCollectionToProbability = new Dictionary<IQuestionPair, double>();
            foreach (var question in _questionCollection)
            {
                _questionCollectionToProbability.Add(question, 1);
            }

        }

        private List<IQuestionPair> SelectNMostProbibleQuestions(int n)
        {

            if (n >= _questionCollectionToProbability.Count)
                return _questionCollectionToProbability.Keys.ToList();
            
            var probabilities = _questionCollectionToProbability.Values.ToList();

            probabilities.Sort();

            var cutoff = probabilities[probabilities.Count - n];

            var mostProbibleQuestions = new List<IQuestionPair>();

            foreach(var question in _questionCollection)
            {
                if (_questionCollectionToProbability[question] >= cutoff)
                {
                    mostProbibleQuestions.Add(question);
                }
            }

            return mostProbibleQuestions;
        }
    }
}
