﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vocabulate.QuizEngine.ChoosersAndGenerators.Helpers
{
    public class ProbabilityDistributionCalculator
    {
        private static readonly Random Rnd = new Random();

        private readonly DistanceCalculator _distanceCalculator;
        private readonly Func<string, string, double> _stringDistanceMeasure;
        private readonly Func<string, string, double> _numberSimilarityMeasure;
        private readonly bool _useNumberSimilarityMeasure;

        public ProbabilityDistributionCalculator(
            Func<string, string, double> stringDistanceMeasure = null,
            Func<string,string,double> numberSimilarityMeasure = null,
            bool useNumberSimilarityMeasure = true)
        {
            _distanceCalculator = new DistanceCalculator();
            _stringDistanceMeasure = stringDistanceMeasure ??
                                    ((a, b) => _distanceCalculator.CalculateStringDistance(a, b));
            _numberSimilarityMeasure = numberSimilarityMeasure ??
                                     ((a, b) => _distanceCalculator.CalculateNumberSimilarity(a, b));
            _useNumberSimilarityMeasure = useNumberSimilarityMeasure;
        }


        public List<string> GetSimilarityWeightedAlternatives(List<string> currentAlternatives,
            List<string> potentialAlternatives, string correctAnswer, int nrOfAlternatives)
        {
            if (nrOfAlternatives >= currentAlternatives.Count + potentialAlternatives.Count)
            {
                currentAlternatives.AddRange(potentialAlternatives);

                return currentAlternatives.Distinct().ToList();
            }

            var probabilityDistribution =
                CalculateAlternativeProbabilityDistribution(potentialAlternatives, correctAnswer);
            while (potentialAlternatives.Count > 0 && currentAlternatives.Count < nrOfAlternatives)
            {
                var newAlternative =
                    GetSimilarityWeightedRandomAlternative(potentialAlternatives, probabilityDistribution);

                if (!currentAlternatives.Contains(newAlternative))
                    currentAlternatives.Add(newAlternative);
            }

            return currentAlternatives;
        }

        private List<double> CalculateAlternativeProbabilityDistribution(List<string> possibleAlternatives, string correctAnswer)
        {
            return CalculateSimilarities(possibleAlternatives, correctAnswer);
        }

        private List<double> CalculateSimilarities(
            List<string> possibleAlternatives, string correctAnswer)
        {


            var correctAnswerIsNumber = UseNumberSimilarity(correctAnswer);

            var similarityList = new List<double>();

            for (var i = 0; i < possibleAlternatives.Count; i++)
            {
                if (correctAnswerIsNumber)
                {
                    similarityList.Add(
                        _numberSimilarityMeasure(correctAnswer, possibleAlternatives[i]));
                }
                else
                {
                    similarityList.Add(
                        _stringDistanceMeasure(correctAnswer, possibleAlternatives[i]));
                }
            }

            if (!correctAnswerIsNumber)
                similarityList = _distanceCalculator.ConvertDistanceToSimilarityMeasure(similarityList);

            return similarityList;
        }

        private bool UseNumberSimilarity(string mightBeNumber)
        {
            if (!_useNumberSimilarityMeasure)
                return false;
            int correctInt;
            return int.TryParse(mightBeNumber, out correctInt);
        }

        private string GetSimilarityWeightedRandomAlternative(List<string> possibleAlternatives, List<double> probabilityDistribution)
        {
            int alternativeIndex = RandomElementChooser.IndexFromProbabilityDensity(probabilityDistribution);
            return possibleAlternatives[alternativeIndex];
        }
    }
}
