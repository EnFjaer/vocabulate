﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.QuestionHistory;

namespace Vocabulate.QuizEngine.ChoosersAndGenerators.Helpers
{
    public class QuestionPicker
    {
        private static readonly Random Rnd = new Random();
        private readonly IAnswerHistory _answerHistory;

        public QuestionPicker(IAnswerHistory answerHistory)
        {
            _answerHistory = answerHistory;
        }

        public bool AllQuestionUpToTierNHasAtLeastOneCorrect(int tier, IQuestionCollection questionCollection)
        {
            foreach (var questionPair in questionCollection)
            {
                if (questionPair.Tier <= tier)
                {
                    if (_answerHistory.NumberOfCorrectAnswers(questionPair) <= 0)
                        return false;
                }
            }

            return true;
        }

        public List<IQuestionPair> GetUpToTierNQuestions(int tier, IQuestionCollection questionCollection)
        {
            var questionPairs = new List<IQuestionPair>();
            foreach (var questionPair in questionCollection)
            {
                if (questionPair.Tier <= tier)
                    questionPairs.Add(questionPair);
            }

            return questionPairs;
        }

        public List<IQuestionPair> GetQuestionsLongestAgoAsked(List<IQuestionPair> questionPairs, double fractionInclude = 0.35)
        {
            return GetFractionOfQuestionsAskedLongestAgo(questionPairs, fractionInclude);
        }

        public IQuestionPair GetConfidenceWeightedRandomQuestion(List<IQuestionPair> possibleQuestions)
        {
            var probabilityDistribution = GetConfidenceWeightedProbabilityDistribution(possibleQuestions);

            var indexOfQuestion = GetRandomIndexFromProbabilityDistribution(probabilityDistribution);

            return possibleQuestions[indexOfQuestion];
        }

        public List<IQuestionPair> RemoveQuestionIdenticalToLastOne(List<IQuestionPair> questions, string question)
        {
            return questions.Where(_ => _.Question != question).ToList();
        }

        public List<IQuestionPair> GetQuestionsThatWasAnsweredIncorrectlyLastTime(
            List<IQuestionPair> possibleQuestions)
        {
            var difficultQuestions = new List<IQuestionPair>();

            foreach (var question in possibleQuestions)
            {
                if (!_answerHistory.OutcomeOfLastAnswer(question))
                {
                    difficultQuestions.Add(question);
                }
            }

            return difficultQuestions.Count > 0 ? difficultQuestions : possibleQuestions;
        }

        private List<double> GetConfidenceWeightedProbabilityDistribution(List<IQuestionPair> possibleQuestions)
        {
            var confidenceScores = new List<double>();
            for (var i = 0; i < possibleQuestions.Count; i++)
            {
                confidenceScores.Add(ConfidenceCalculator.CalculateConfidence(possibleQuestions[i], _answerHistory));
            }

            var probabilityFactors = confidenceScores.Select(ConfidenceToProbabilityFactor).ToList();
            var totalFactor = probabilityFactors.Sum();
            var weightedFactors = probabilityFactors.Select(_ => _ / totalFactor).ToList();
            var probabilityDistribution = new List<double>();

            double runningTotal = 0;
            for (var i = 0; i < weightedFactors.Count; i++)
            {
                probabilityDistribution.Add(runningTotal);
                runningTotal += weightedFactors[i];
            }

            return probabilityDistribution;
        }

        private double ConfidenceToProbabilityFactor(double confidence)
        {
            confidence = confidence < 0 ? 0 : confidence;
            confidence = confidence > 1 ? 1 : confidence;

            return (1 - confidence) * 4 + 1;
        }

        private int GetRandomIndexFromProbabilityDistribution(List<double> probabilityDistribution)
        {
            var randomIndex = Rnd.NextDouble();

            return probabilityDistribution.FindLastIndex(_ => _ < randomIndex);
        }

        private List<IQuestionPair> GetFractionOfQuestionsAskedLongestAgo(List<IQuestionPair> questionPairs,
            double fraction)
        {
            var timeAgoAsked = GetTimeAgoAskedList(questionPairs);

            timeAgoAsked.Sort((a,b) => b.Item1 - a.Item1);

            var includeAtLeastElementNr = (int) Math.Floor(fraction * questionPairs.Count);

            var leastAskedAgoValue = timeAgoAsked[includeAtLeastElementNr].Item1;

            return timeAgoAsked.Where(_ => _.Item1 >= leastAskedAgoValue).Select(_ => _.Item2).ToList();

        }

        private List<Tuple<int, IQuestionPair>> GetTimeAgoAskedList(List<IQuestionPair> questionPairs)
        {
            var timeAgoAskedList = new List<Tuple<int, IQuestionPair>>();

            foreach (var question in questionPairs)
            {
                int timeAskedAgo = _answerHistory.NrQuestionsSinceLastAsked(question);

                timeAskedAgo = ReducedTimeAgoWhenConfidenceIsHigh(question, timeAskedAgo);

                timeAgoAskedList.Add(new Tuple<int, IQuestionPair>(timeAskedAgo, question));
            }

            return timeAgoAskedList;
        }

        private int ReducedTimeAgoWhenConfidenceIsHigh(IQuestionPair question, int timeAskedAgo)
        {
            if (ConfidenceCalculator.CalculateConfidence(question, _answerHistory) > 0.99)
            {
                return timeAskedAgo / 3;
            }

            return timeAskedAgo;
        }
    }
}
