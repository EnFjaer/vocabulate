﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using F23.StringSimilarity;
using F23.StringSimilarity.Interfaces;

namespace Vocabulate.QuizEngine.ChoosersAndGenerators.Helpers
{
    public class DistanceCalculator
    {
        private static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        private IStringDistance _stringDistanceMeasurer;

        public DistanceCalculator(IStringDistance stringDistanceMeasurer = null)
        {
            _stringDistanceMeasurer = stringDistanceMeasurer ?? new Levenshtein();

        }

        public double CalculateStringDistance(string correct, string alternative)
        {
            return _stringDistanceMeasurer.Distance(correct, alternative);
        }

        public double CalculateNumberSimilarity(string correct, string alternative)
        {
            var reversedAlternative = Reverse(alternative);

            if (correct.Length == 1 && alternative.Length == 1)
            {
                return 6;
            }

            if (string.Compare(correct, reversedAlternative, StringComparison.Ordinal) == 0)
                return 6;

            for (var i = 0; i < correct.Length && i < alternative.Length; i++)
            {
                if (correct[i] == alternative[i])
                    return 6;
            }

            for (var i = 0; i < correct.Length && i < reversedAlternative.Length; i++)
            {
                if (correct[i] == reversedAlternative[i])
                    return 2;
            }

            return 1;
        }

        public List<double> ConvertDistanceToSimilarityMeasure(List<double> distances)
        {
            var maxDistance = distances.Max();
            return distances.Select(_ => Math.Pow(2 - _ / maxDistance, 4)).ToList();
        }
    }
}
