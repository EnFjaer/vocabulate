﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Vocabulate.QuizEngine.ChoosersAndGenerators.Helpers
{
    public static class RandomElementChooser
    {
        private static readonly Random Rnd = new Random();

        public static int IndexFromProbabilityDensity(List<double> probabilityList)
        {
            var summedProbabilityList = DensitiyProbabilityToSummedProbability(probabilityList);
            return IndexFromSummedProbability(summedProbabilityList);
        }

        public static T ElementFromProbabilityDensity<T>(Dictionary<T, double> candidates)
        {
            return ElementFromCustomProbabilityFunction(candidates, IndexFromProbabilityDensity);
        }

        private static List<double> DensitiyProbabilityToSummedProbability(List<double> similarities)
        {
            var totalFactor = similarities.Sum();
            var weightedFactors = similarities.Select(_ => _ / totalFactor).ToList();
            var probabilityDistribution = new List<double>();

            double runningTotal = 0;
            for (var i = 0; i < weightedFactors.Count; i++)
            {
                probabilityDistribution.Add(runningTotal);
                runningTotal += weightedFactors[i];
            }

            return probabilityDistribution;
        }

        private static int IndexFromSummedProbability(List<double> summedProbabilityList)
        {
            var randomIndex = Rnd.NextDouble();

            return summedProbabilityList.FindLastIndex(_ => _ < randomIndex);
        }

        private static T ElementFromCustomProbabilityFunction<T>(Dictionary<T, double> candidates,
            Func<List<double>, int> indexFromProbabilityFunction)
        {
            if (candidates.Count == 0)
                return default(T);

            var probabilities = new List<double>();
            var elements = new List<T>();
            foreach (var candidate in candidates)
            {
                probabilities.Add(candidate.Value);
                elements.Add(candidate.Key);
            }

            var index = indexFromProbabilityFunction(probabilities);
            return elements[index];
        }
    }
}
