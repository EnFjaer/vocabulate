﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vocabulate.QuizEngine.ChoosersAndGenerators.Helpers
{
    public class RollController
    {
        private int _currentRoll;

        private int _rollTrigger;

        public RollController(int rollTrigger)
        {
            _rollTrigger = rollTrigger;
        }

        public void AddResult(bool wasCorrect)
        {
            if (wasCorrect)
            {
                _currentRoll = _currentRoll > 0 ? _currentRoll + 1 : 1;
            }
            else
            {
                _currentRoll = _currentRoll < 0 ? _currentRoll - 1 : -1;
            }
        }

        public bool IsOnPositiveRoll => _currentRoll >= _rollTrigger;

        public bool IsOnNegativeRoll => _currentRoll <= -_rollTrigger;
    }
}
