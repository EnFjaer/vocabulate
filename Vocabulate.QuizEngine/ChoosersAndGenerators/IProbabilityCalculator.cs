﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.ChoosersAndGenerators
{
    public interface IProbabilityCalculator
    {
        Dictionary<IQuestionPair, double> CalculateProbability(IQuestionCollection questionCollection);
    }
}
