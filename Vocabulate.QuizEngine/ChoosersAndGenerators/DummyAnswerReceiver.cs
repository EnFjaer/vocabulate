﻿using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.ChoosersAndGenerators
{
    public class DummyAnswerReceiver : IAnswerReceiver
    {
        public void RegisterAnswer(IQuizQuestion question, string answer)
        {
        }
    }
}
