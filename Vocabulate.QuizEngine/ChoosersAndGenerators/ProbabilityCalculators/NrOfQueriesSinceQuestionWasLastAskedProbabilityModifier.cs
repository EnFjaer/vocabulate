﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.QuestionHistory;

namespace Vocabulate.QuizEngine.ChoosersAndGenerators.ProbabilityCalculators
{
    public class NrOfQueriesSinceQuestionWasLastAskedProbabilityModifier : IProbabilityCalculator
    {
        private IAnswerHistory _answerHistory;
        private double _valueAtSizeOfCollection;
        private double _maxValue;
        public NrOfQueriesSinceQuestionWasLastAskedProbabilityModifier(IAnswerHistory answerHistory, double valueAtSizeOfCollection = 2, double maxValue = 3)
        {
            _answerHistory = answerHistory;
            _maxValue = maxValue;
            _valueAtSizeOfCollection = valueAtSizeOfCollection;
        }

        public Dictionary<IQuestionPair, double> CalculateProbability(IQuestionCollection questionCollection)
        {
            var probabilityDictionary = new Dictionary<IQuestionPair, double>();
            var N = questionCollection.Count;

            foreach (var question in questionCollection)
            {
                if (_answerHistory.NrQuestionsSinceLastAsked(question) == int.MaxValue)
                    probabilityDictionary[question] = _maxValue;
                else
                {
                    var probabilityChange = 1 + (double)_answerHistory.NrQuestionsSinceLastAsked(question) / N *
                                            (_valueAtSizeOfCollection - 1);

                    probabilityChange = probabilityChange > _maxValue ? _maxValue : probabilityChange;

                    probabilityDictionary[question] = probabilityChange;
                }
            }

            return probabilityDictionary;
        }
    }
}
