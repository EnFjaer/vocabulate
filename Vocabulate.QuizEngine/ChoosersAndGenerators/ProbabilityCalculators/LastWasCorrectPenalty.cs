﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.QuestionHistory;

namespace Vocabulate.QuizEngine.ChoosersAndGenerators.ProbabilityCalculators
{
    public class LastWasCorrectPenalty : IProbabilityCalculator
    {
        private readonly IAnswerHistory _answerHistory;
        private readonly double _probabilityFactor;

        public LastWasCorrectPenalty(IAnswerHistory answerHistory, double factor = 0.5)
        {
            _answerHistory = answerHistory;
            _probabilityFactor = factor;
        }

        public Dictionary<IQuestionPair, double> CalculateProbability(IQuestionCollection questionCollection)
        {
            var probabilityDictionary = new Dictionary<IQuestionPair, double>();
            foreach (var question in questionCollection)
            {
                probabilityDictionary[question] = _answerHistory.OutcomeOfLastAnswer(question) ? _probabilityFactor : 1;
            }

            return probabilityDictionary;
        }
    }
}
