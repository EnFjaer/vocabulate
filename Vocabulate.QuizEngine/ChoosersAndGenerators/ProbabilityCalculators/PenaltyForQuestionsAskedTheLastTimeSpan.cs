﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.QuestionHistory;

namespace Vocabulate.QuizEngine.ChoosersAndGenerators.ProbabilityCalculators
{
    public class PenaltyForQuestionsAskedTheLastTimeSpan : IProbabilityCalculator
    {
        private TimeSpan _penaltyTime;
        private double _penalty;
        private IAnswerHistory _answerHistory;

        public PenaltyForQuestionsAskedTheLastTimeSpan(IAnswerHistory answerHistory, double penalty = 0.5, TimeSpan penaltyTime = default(TimeSpan))
        {
            if (penaltyTime == default(TimeSpan))
                penaltyTime = TimeSpan.FromMinutes(15);

            _penaltyTime = penaltyTime;
            _penalty = penalty;
            _answerHistory = answerHistory;
        }
        public Dictionary<IQuestionPair, double> CalculateProbability(IQuestionCollection questionCollection)
        {
            var probabilityDictionary = new Dictionary<IQuestionPair, double>();

            foreach (var question in questionCollection)
            {
                var isPenaltyTime = DateTime.Now - _answerHistory.LastTimeAsked(question) < _penaltyTime;
                probabilityDictionary[question] = isPenaltyTime ? _penalty : 1;
            }

            return probabilityDictionary;
        }
    }
}
