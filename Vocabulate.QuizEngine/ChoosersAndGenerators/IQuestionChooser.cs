﻿using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.ChoosersAndGenerators
{
    public interface IQuestionChooser
    {
        IQuestionPair GetQuestion();
        int CurrentTier { get; }
    }
}
