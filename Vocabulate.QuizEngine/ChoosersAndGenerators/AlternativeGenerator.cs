﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vocabulate.QuizEngine.ChoosersAndGenerators.Helpers;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.Extensions;

namespace Vocabulate.QuizEngine.ChoosersAndGenerators
{
    public class AlternativeGenerator : IAlternativeGenerator
    {
        private IQuestionPair _currentQuestion = new NullQuestionPair();
        private readonly IQuestionChooser _questionChooser;
        private readonly ProbabilityDistributionCalculator _probabilityDistributionCalculator;

        public AlternativeGenerator(IQuestionChooser questionChooser, IQuestionCollection questionCollection, int nrOfAlternatives = 8, bool isNumbers = true)
        {
            NrOfAlternatives = nrOfAlternatives;
            QuestionCollection = questionCollection ?? new NullQuestionCollection();
            _questionChooser = questionChooser ?? new NullQuestionChooser();
            _probabilityDistributionCalculator = new ProbabilityDistributionCalculator(useNumberSimilarityMeasure:isNumbers);
        }

        public List<string> GenerateAlternatives(IQuestionPair question)
        {
            _currentQuestion = question;
            var alternatives = CreateListOfAlternatives();
            alternatives.Shuffle();

            return alternatives;
        }

        public IQuestionCollection QuestionCollection { get; set; }

        public int NrOfAlternatives { get; set; }

        public int FindCorrectAnswer(IList<string> alternatives)
        {
            return alternatives.IndexOf(_currentQuestion.Answer);
        }

        private List<string> CreateListOfAlternatives()
        {
            if (NrOfAlternatives > QuestionCollection.Count)
                throw new IndexOutOfRangeException("NrOfAlternatives is larger than total number of questions");

            var alternatives = new List<string>();

            alternatives.Add(_currentQuestion.Answer);

            var tier = _questionChooser.CurrentTier;

            var validAlternatives = GetAlternativesThatAreNotCorrectAnswers();

            while (alternatives.Count < NrOfAlternatives)
            {
                var tierOkAlternatives = CreateListTierOkAlternatives(validAlternatives, tier++);

                alternatives = _probabilityDistributionCalculator.GetSimilarityWeightedAlternatives(alternatives, tierOkAlternatives, _currentQuestion.Answer, NrOfAlternatives);
            }

            return alternatives;
        }

        private List<string> CreateListTierOkAlternatives(List<IQuestionPair> validAlternatives, int tier)
        {
            return validAlternatives.FindAll(_ => _.Tier <= tier).Select(_ => _.Answer).ToList();
        }

        private List<IQuestionPair> GetAlternativesThatAreNotCorrectAnswers()
        {
            var invalidAnswers = QuestionCollection.ToList().FindAll(_ => _.Question == _currentQuestion.Question)
                .Select(_ => _.Answer);
            var validAlternatives = QuestionCollection.ToList();
            foreach (var invalidAnswer in invalidAnswers)
            {
                validAlternatives = validAlternatives.FindAll(_ => _.Answer != invalidAnswer);
            }
            return validAlternatives;

        }
    }
}
