﻿using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.ChoosersAndGenerators
{
    public interface IAnswerReceiver
    {
        void RegisterAnswer(IQuizQuestion question, string answer);
    }
}
