﻿using System.Collections.Generic;
using System.Linq;
using Vocabulate.QuizEngine.Analytics;
using Vocabulate.QuizEngine.ChoosersAndGenerators.Helpers;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.QuestionHistory;

namespace Vocabulate.QuizEngine.ChoosersAndGenerators
{
    public class QuestionChooser : IQuestionChooser, IAnswerReceiver
    {
        private IQuestionPair _lastQuestion = new NullQuestionPair();
        private readonly IAnswerHistory _answerHistory;
        private readonly AnswerTracker _answerTracker;
        private readonly RollController _rollController;
        private readonly QuestionPicker _questionPicker;
        private readonly IQuestionCollection _questionCollection;

        private bool _respectTier;

        public QuestionChooser(IQuestionCollection questionCollection, IAnswerHistory answerHistory = null, IAnalyticsService analyticsService = null, int rollTrigger = 3, bool respectTier = true)
        {
            _questionCollection = questionCollection;
            _answerHistory = answerHistory ?? new DummyAnswerHistory();
            _answerTracker = new AnswerTracker(analyticsService ?? new DummyAnalyticsService());
            CurrentTier = 1; 

            _rollController = new RollController(rollTrigger);
            _questionPicker = new QuestionPicker(_answerHistory);
            _respectTier = respectTier;
        }

        public IQuestionPair GetQuestion()
        {
            CalculateNextQuestion();
            return _lastQuestion;
        }


        public int CurrentTier { get; private set; }

        public void RegisterAnswer(IQuizQuestion question, string answer)
        {
            var wasCorrect = question.Answer == answer;

            var quizAnswer = new QuizAnswer(question, wasCorrect, answer);

            _rollController.AddResult(wasCorrect);

            _answerHistory.AddAnswer(quizAnswer);
            _answerTracker.TrackAnswer(quizAnswer);
        }

        private void CalculateNextQuestion()
        {
            var validQuestions = GetValidQuestions();

            validQuestions = _questionPicker.RemoveQuestionIdenticalToLastOne(validQuestions, _lastQuestion.Question);

            validQuestions = _rollController.IsOnPositiveRoll
                ? _questionPicker.GetQuestionsThatWasAnsweredIncorrectlyLastTime(validQuestions)
                : validQuestions;

            validQuestions = _questionPicker.GetQuestionsLongestAgoAsked(validQuestions);

            if (validQuestions.Count < 2)
                _lastQuestion = validQuestions[0];

            _lastQuestion = _questionPicker.GetConfidenceWeightedRandomQuestion(validQuestions);
        }

        private List<IQuestionPair> GetValidQuestions()
        {
            if (!_respectTier)
                return _questionPicker.GetUpToTierNQuestions(int.MaxValue, _questionCollection);

            var maxTier = _questionCollection.Max(_ => _.Tier);
            for (var i = maxTier - 1; i > 0; i--)
            {
                if (_questionPicker.AllQuestionUpToTierNHasAtLeastOneCorrect(i, _questionCollection))
                {
                    CurrentTier = i + 1;
                    return _questionPicker.GetUpToTierNQuestions(i + 1, _questionCollection);
                }
            }

            return _questionPicker.GetUpToTierNQuestions(1, _questionCollection);
        }
    }
}
