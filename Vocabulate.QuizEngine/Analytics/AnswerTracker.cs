﻿using System;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.Analytics
{
    public class AnswerTracker
    {
        private readonly IAnalyticsService _analyticsService;
        private DateTime _lastQuestionTime;
        private int _answerStreak;

        public AnswerTracker(IAnalyticsService analyticsService)
        {
            _analyticsService = analyticsService;
            _lastQuestionTime = DateTime.Now;
            _answerStreak = 0;
        }

        public void TrackAnswer(IQuizAnswer answer)
        {
            RegisterQuestion(answer);
            RegisterGuess(answer);
            RegisterAlternatives(answer);
            RegisterTier(answer);
            RegisterAnswerTime();
            RegisterAnswerStreak(answer);
        }

        private void RegisterQuestion(IQuizAnswer answer)
        {
            _analyticsService.TrackAppEvent(AnalyticsEventCategory.QuestionGuessed, answer.Question);
        }

        private void RegisterGuess(IQuizAnswer answer)
        {
            _analyticsService.TrackAppEvent(AnalyticsEventCategory.Guess,
                answer.IsFlipped.ToString());

            _analyticsService.TrackAppEvent(
                answer.AnsweredCorrectly ? AnalyticsEventCategory.CorrectGuess : AnalyticsEventCategory.IncorrectGuess,
                answer.Question);

            if(answer.AnsweredCorrectly)
                _analyticsService.TrackAppEvent(AnalyticsEventCategory.IncorrectGuessWithAnswer, answer.Question + " : " + answer.GivenAnswer);
        }

        private void RegisterAlternatives(IQuizAnswer answer)
        {
            foreach(var alternative in answer.Alternatives)
                _analyticsService.TrackAppEvent(AnalyticsEventCategory.AlternativePresented, alternative);
        }

        private void RegisterTier(IQuizAnswer answer)
        {
            _analyticsService.TrackAppEvent(AnalyticsEventCategory.TierPresented, answer.Tier.ToString());
        }

        private void RegisterAnswerTime()
        {
            var timeUsed = DateTime.Now - _lastQuestionTime;
            _analyticsService.TrackAppEvent(AnalyticsEventCategory.AnswerTime, timeUsed.Seconds.ToString());
            _lastQuestionTime = DateTime.Now;
        }

        private void RegisterAnswerStreak(IQuizAnswer answer)
        {
            if ((answer.AnsweredCorrectly && _answerStreak < 0) ||
               (!answer.AnsweredCorrectly && _answerStreak > 0))
            {
                _analyticsService.TrackAppEvent(AnalyticsEventCategory.AnswerStreak, _answerStreak.ToString());
                _answerStreak = 0;
            }

            if (answer.AnsweredCorrectly)
                _answerStreak++;
            else
            {
                _answerStreak--;
            }
        }
    }
}