﻿namespace Vocabulate.QuizEngine.Analytics
{
    public struct AnalyticsEventCategory
    {
        public static string CorrectGuess => "CorrectGuess";
        public static string IncorrectGuess => "IncorrectGuess";
        public static string IncorrectGuessWithAnswer => "IncorrectGuessWithAnswer";
        public static string Guess => "Guess";
        public static string QuestionGuessed => "QuestionGuessed";
        public static string AlternativePresented => "AlternativePresented";
        public static string TierPresented => "TierPresented";
        public static string AnswerTime => "AnswerTime";
        public static string AnswerStreak => "AnswerStreak";
    };
}
