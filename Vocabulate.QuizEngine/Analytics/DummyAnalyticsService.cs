﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vocabulate.QuizEngine.Analytics
{
    public class DummyAnalyticsService : IAnalyticsService
    {
        public void TrackAppPage(string pageNameToTrack)
        {
        }

        public void TrackAppEvent(string eventCategory, string eventToTrack)
        {
        }

        public void TrackAppException(string exceptionMessageToTrack, bool isFatalException)
        {
        }
    }
}
