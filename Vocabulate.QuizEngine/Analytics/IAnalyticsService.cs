﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vocabulate.QuizEngine.Analytics
{
    public interface IAnalyticsService
    {
        void TrackAppPage(string pageNameToTrack);
        void TrackAppEvent(string eventCategory, string eventToTrack);
        void TrackAppException(string exceptionMessageToTrack, bool isFatalException);
    }
}
