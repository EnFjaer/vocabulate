﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.QuestionHistory;

namespace Vocabulate.QuizEngine
{
    public static class ConfidenceCalculator
    {
        public static double CalculateConfidence(IQuestionPair questionPair, IAnswerHistory answerHistory, int maxHistoryUseForConfidence = 5)
        {
            var maxNumberStored = answerHistory.MaxAnswersStored;
            var correctHistory = answerHistory.OutcomeOfLastAnswers(questionPair);

            if (correctHistory.Count == 0)
                return 0;

            var useLastNAnswers = maxHistoryUseForConfidence < maxNumberStored
                ? maxHistoryUseForConfidence
                : maxNumberStored;


            var useHistory = correctHistory.Count <= useLastNAnswers
                ? correctHistory
                : correctHistory.GetRange(correctHistory.Count - useLastNAnswers, useLastNAnswers);

            var correctRate = (double)useHistory.Count(_ => _) / useHistory.Count;

            if (useHistory.Count == useLastNAnswers)
                return correctRate;

            return correctRate * Math.Pow((double)useHistory.Count / useLastNAnswers, 0.5);
        }
    }
}
