﻿using System;
using Vocabulate.QuizEngine.Analytics;
using Vocabulate.QuizEngine.ChoosersAndGenerators;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.Database;
using Vocabulate.QuizEngine.QuestionHistory;

namespace Vocabulate.QuizEngine
{
    public class FlashCardEngine<T> where T : IQuestionPair, new()
    {

        private IQuestionChooser _questionChooser;
        private IAnswerReceiver _answerReceiver;

        public FlashCardEngine(IQuestionCollection questionCollection, IAnswerDatabaseConnection answerDatabaseConnection = null, IAnalyticsService analystiService = null)
        {
            IAnswerReader answerReader;
            IAnswerWriter answerWriter;

            if (answerDatabaseConnection == null)
            {
                answerReader = new DummyAnswerReader();
                answerWriter = new DummyAnswerWriter();
            }
            else
            {
                answerReader = new AnswerReader(answerDatabaseConnection);
                answerWriter = new AnswerWriter(answerDatabaseConnection);
            }

            var answerHistory = new AnswerHistory(answerWriter, answerReader);

            _questionChooser = new QuestionChooser(questionCollection, answerHistory, analystiService);
            
        }

        public T GetFlashCard()
        {
            var flashCardQuestion = _questionChooser.GetQuestion();
            return flashCardQuestion is T ? ((T) flashCardQuestion) : new T();
        }

        private void RegisterAnswer(T question, int confidence)
        {
        }

        private bool IsOfCurrentTemplate(IQuestionPair questionPair)
        {
            return questionPair is T;
        }
    }
}
