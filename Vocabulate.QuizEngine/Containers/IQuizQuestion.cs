﻿using System.Collections.Generic;

namespace Vocabulate.QuizEngine.Containers
{
    public interface IQuizQuestion : IQuestionPair
    {
        List<string> Alternatives { get; set; }
        int CorrectAnswerIndex { get; set; }
    }
}
