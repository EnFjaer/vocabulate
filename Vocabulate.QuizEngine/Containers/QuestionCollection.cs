﻿using System.Collections.Generic;

namespace Vocabulate.QuizEngine.Containers
{
    public class QuestionCollection : BaseQuestionCollection
    {
        private bool _isFlipped;
        private List<IQuestionPair> _questionPairs;

        public QuestionCollection(string collectionName)
        {
            CollectionName = collectionName;
        }

        public QuestionCollection(string collectionName, List<IQuestionPair> questionPairs)
        {
            CollectionName = collectionName;
            _questionPairs = questionPairs;
            _isFlipped = false;
        }

        protected override IList<IQuestionPair> QuestionPairs
        {
            get
            {

                if (_questionPairs == null)
                {
                    _questionPairs = LoadQuestions();
                }

                return _questionPairs;
            }
        }

        public override string CollectionName { get; }

        public override bool FlipQuestionAndAnswer
        {
            get { return _isFlipped; }

            set
            {
                _isFlipped = value;
                DoFlipQuestionAndAnswer();
            }
        }

        protected virtual List<IQuestionPair> LoadQuestions()
        {
            return new List<IQuestionPair>();
        }

        private void DoFlipQuestionAndAnswer()
        {
            for (var i = 0; i < QuestionPairs.Count; i++)
            {
                QuestionPairs[i].IsFlipped = _isFlipped;
            }
        }
    }
}
