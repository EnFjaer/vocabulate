﻿using System;
using System.Collections.Generic;

namespace Vocabulate.QuizEngine.Containers
{
    public class QuizAnswer : QuizQuestion, IQuizAnswer
    {
        public QuizAnswer(IQuestionPair questionPair, List<string> alternatives, int correctAnswerIndex, bool answeredCorrectly, string givenAnswer) : base(questionPair, alternatives, correctAnswerIndex)
        {
            AnsweredCorrectly = answeredCorrectly;
            GivenAnswer = givenAnswer;
            TimeAnswered = DateTime.UtcNow;
        }

        public QuizAnswer(IQuizQuestion quizQuestion, bool answeredCorrectly, string givenAnswer) : this(quizQuestion, quizQuestion.Alternatives, quizQuestion.CorrectAnswerIndex, answeredCorrectly, givenAnswer)
        {
        }

        public QuizAnswer() : base(new NullQuestionPair(), new List<string>(), -1)
        {
        }

        public bool AnsweredCorrectly { get; set; }
        public string GivenAnswer { get; set; }
        public DateTime TimeAnswered { get; set; }
    }
}
