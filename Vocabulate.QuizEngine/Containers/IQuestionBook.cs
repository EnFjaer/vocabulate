﻿using System.Collections.Generic;

namespace Vocabulate.QuizEngine.Containers
{
    public interface IQuestionBook
    {
        string BookName { get; }

        List<IQuestionCollection> QuestionCollections { get; }
    }
}
