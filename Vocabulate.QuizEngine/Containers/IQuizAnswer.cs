﻿using System;

namespace Vocabulate.QuizEngine.Containers
{
    public interface IQuizAnswer : IQuizQuestion
    {
        bool AnsweredCorrectly { get; set; }
        string GivenAnswer { get; set; }
        DateTime TimeAnswered { get; set; }
    }
}
