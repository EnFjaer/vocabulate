﻿using System.Collections.Generic;

namespace Vocabulate.QuizEngine.Containers
{
    public class CombinedCollection : BaseQuestionCollection
    {
        private readonly List<IQuestionCollection> _questionCollections;

        public CombinedCollection(List<IQuestionCollection> questionCollections)
        {
            _questionCollections = questionCollections;
        }

        protected override IList<IQuestionPair> QuestionPairs
        {
            get
            {
                var questionPairs = new List<IQuestionPair>();
                foreach (var questionCollection in _questionCollections)
                {
                    questionPairs.AddRange(questionCollection);
                }

                return questionPairs;
            }
        }

        public override string CollectionName => _questionCollections[0].CollectionName + " - " +
                                        _questionCollections[_questionCollections.Count - 1].CollectionName;

        public override bool FlipQuestionAndAnswer
        {
            get => _questionCollections[0].FlipQuestionAndAnswer;

            set
            {
                foreach (var questionCollection in _questionCollections)
                {
                    questionCollection.FlipQuestionAndAnswer = value;
                }
            }
        }
    }
}
