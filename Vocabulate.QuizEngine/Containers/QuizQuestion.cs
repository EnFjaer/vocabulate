﻿using System.Collections.Generic;

namespace Vocabulate.QuizEngine.Containers
{
    public class QuizQuestion : QuestionPair, IQuizQuestion
    {
        public QuizQuestion(IQuestionPair questionPair, List<string> alternatives = null, int correctAnswerIndex = -1)
        {
            IsFlipped = questionPair.IsFlipped;

            Question = questionPair.Question;
            Answer = questionPair.Answer;
            Tier = questionPair.Tier;
            Alternatives = alternatives ?? new List<string>();
            CorrectAnswerIndex = correctAnswerIndex;
            Transliteration = questionPair.Transliteration;
        }

        public List<string> Alternatives { get; set; }
        public int CorrectAnswerIndex { get; set; }

    }
}
