﻿using System;
using System.Collections.Generic;

namespace Vocabulate.QuizEngine.Containers
{
    public interface IQuestionCollection : IList<IQuestionPair>
    {
        string CollectionName { get; }
        bool FlipQuestionAndAnswer { get; set; }
        void AddRange(IList<IQuestionPair> addList);
    }
}
