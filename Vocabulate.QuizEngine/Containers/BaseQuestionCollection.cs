﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Vocabulate.QuizEngine.Containers
{
    public abstract class BaseQuestionCollection : IQuestionCollection
    {
        public IEnumerator<IQuestionPair> GetEnumerator()
        {
            return QuestionPairs.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(IQuestionPair item)
        {
            QuestionPairs.Add(item);
        }

        public void Clear()
        {
            QuestionPairs.Clear();
        }

        public bool Contains(IQuestionPair item)
        {
            return QuestionPairs.Contains(item);
        }

        public void CopyTo(IQuestionPair[] array, int arrayIndex)
        {
            QuestionPairs.CopyTo(array, arrayIndex);
        }

        public bool Remove(IQuestionPair item)
        {
            return QuestionPairs.Remove(item);
        }

        public int Count
        {
            get => QuestionPairs.Count;
        }

        public bool IsReadOnly
        {
            get => QuestionPairs.IsReadOnly;
        }

        public int IndexOf(IQuestionPair item)
        {
            return QuestionPairs.IndexOf(item);
        }

        public void Insert(int index, IQuestionPair item)
        {
            QuestionPairs.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            QuestionPairs.RemoveAt(index);
        }

        public IQuestionPair this[int index]
        {
            get => QuestionPairs[index];
            set => QuestionPairs[index] = value;
        }

        protected abstract IList<IQuestionPair> QuestionPairs { get; }
        public abstract string CollectionName { get; }
        public abstract bool FlipQuestionAndAnswer { get; set; }
        public void AddRange(IList<IQuestionPair> addList)
        {
            foreach(var element in addList)
                Add(element);
        }
    }
}