﻿using System.Collections.Generic;

namespace Vocabulate.QuizEngine.Containers
{
    public class NullQuestionCollection : BaseQuestionCollection
    {
        protected override IList<IQuestionPair> QuestionPairs => new List<IQuestionPair>();
        public override string CollectionName => string.Empty;
        public override bool FlipQuestionAndAnswer { get; set; }
    }
}
