﻿using System;

namespace Vocabulate.QuizEngine.Containers
{
    public class FullDictionaryWord
    {
        public FullDictionaryWord(string word, string primaryTranslation, string secondaryTranslations,
            string transliteration)
        {
            Word = word;
            PrimaryTranslation = primaryTranslation;
            SecondaryTranslations = secondaryTranslations;
            Transliteration = transliteration;
        }

        public FullDictionaryWord()
        {
            Word = string.Empty;
            PrimaryTranslation = string.Empty;
            SecondaryTranslations = string.Empty;
            Transliteration = string.Empty;
        }

        public string Word { get; set; }
        public string PrimaryTranslation { get; set; }
        public string SecondaryTranslations { get; set; }
        public string Transliteration { get; set; }
    }
}
