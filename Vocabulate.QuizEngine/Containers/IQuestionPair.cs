﻿namespace Vocabulate.QuizEngine.Containers
{
    public interface IQuestionPair
    {
        string Question { get; set; }
        string Answer { get; set; }
        string Transliteration { get; set; }
        bool IsFlipped { get; set; }
        int Tier { get; set; }
    }
}
