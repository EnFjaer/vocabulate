﻿using System;

namespace Vocabulate.QuizEngine.Containers
{
    class NullQuestionPair : IQuestionPair
    {
        public string Question
        {
            get => string.Empty;
            set { }
        }

        public string Answer
        {
            get => string.Empty;
            set { }
        }

        public string Transliteration { get => String.Empty;
            set {}
        }

        public bool IsFlipped
        {
            get => false;
            set { }
        }

        public int Tier
        {
            get => -1;
            set { }
        }
    }
}
