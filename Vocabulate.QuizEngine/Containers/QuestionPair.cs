﻿using System;

namespace Vocabulate.QuizEngine.Containers
{
    public class QuestionPair : IQuestionPair
    {
        private string _question;
        private string _answer;

        public QuestionPair(string question, string answer, int tier = 1, bool isFlipped = false, string transliteration = "")
        {
            if (isFlipped)
            {
                _question = answer;
                _answer = question;
            }
            else
            {
                _question = question;
                _answer = answer;
            }

            IsFlipped = isFlipped;
            Tier = tier;
            Transliteration = transliteration;
        }

        public QuestionPair() : this(string.Empty, string.Empty)
        {
        }

        public QuestionPair(IQuestionPair questionPair) : this(questionPair.Question, questionPair.Answer, questionPair.Tier, questionPair.IsFlipped, questionPair.Transliteration)
        {
        }

        public string Question
        {
            get => IsFlipped ? _answer : _question;
            set
            {
                if (IsFlipped)
                {
                    _answer = value;
                }
                else
                {
                    _question = value;
                }
            }
        }

        public string Answer
        {
            get => IsFlipped ? _question : _answer;
            set
            {
                if (IsFlipped)
                {
                    _question = value;
                }
                else
                {
                    _answer = value;
                }
            }
        }

        public string Transliteration { get; set; }

        public bool IsFlipped { get; set; }
        public int Tier { get; set; }

        public override bool Equals(object obj)
        {
            var item = obj as QuestionPair;

            if (item == null)
            {
                return false;
            }

            return this.Question == item.Question &&
                   this.Answer == item.Answer &&
                   this.IsFlipped == item.IsFlipped &&
                   this.Tier == item.Tier;
        }

        public override int GetHashCode()
        {
            return Question.GetHashCode() + Answer.GetHashCode();
        }
    }
}
