﻿using System;
using System.Collections.Generic;

namespace Vocabulate.QuizEngine.Containers
{
    public class NullQuizAnswer : IQuizAnswer
    {
        public List<string> Alternatives
        {
            get { return new List<string>(); }
            set { }
        }

        public int CorrectAnswerIndex
        {
            get { return -1; }
            set { }
        }

        public bool AnsweredCorrectly
        {
            get { return false; }
            set { }
        }

        public string GivenAnswer
        {
            get => string.Empty;
            set { }
        }

        public DateTime TimeAnswered
        {
            get => DateTime.MinValue; 
            set { }
        }

        public string Question
        {
            get => string.Empty;
            set { }
        }

        public string Answer
        {
            get => string.Empty;
            set { }
        }

        public string Transliteration {
            get => string.Empty;
            set {}
        }

        public bool IsFlipped
        {
            get => false;
            set { }
        }

        public int Tier
        {
            get => -1;
            set { }
        }
    }
}
