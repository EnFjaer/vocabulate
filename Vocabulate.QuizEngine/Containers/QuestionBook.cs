﻿using System.Collections.Generic;

namespace Vocabulate.QuizEngine.Containers
{
    public class QuestionBook : IQuestionBook
    {
        public QuestionBook(string bookName, List<IQuestionCollection> questionCollections)
        {
            BookName = bookName;
            QuestionCollections = questionCollections;
        }
        public string BookName { get; }
        public List<IQuestionCollection> QuestionCollections { get; }
    }
}
