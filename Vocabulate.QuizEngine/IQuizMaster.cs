﻿using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine
{
    public interface IQuizMaster
    {
        IQuizQuestion GetQuestion();
    }
}
