﻿using System.Linq;
using Moq;
using NFluent;
using NUnit.Framework;

namespace Vocabulate.Droid.Tests.Database
{
    namespace AnswerReaderWriterTests
    {
        public class TestBase
        {
            protected AnswerReaderWriterFixture Fixture;

            [SetUp]
            public void RunBeforeAnyTest()
            {
                Fixture = new AnswerReaderWriterFixture();
            }
        }

        [TestFixture]
        public class WriteAnswer : TestBase
        {
            [Test]
            public void IfNoAnswerIsWrittenNrOfAnswersIsZero()
            {
                var reader = Fixture.CreateReader();

                var nrOfAnswers = reader.NrOfAnswers;
                Check.That(nrOfAnswers).IsEqualTo(0);
            }

            [Test]
            public async void AfterOneAnswerIsWrittenNrOfAnswersIsOne()
            {
                var reader = Fixture.CreateReader();
                var writer = Fixture.CreateWriter();

                var quizAnswer = Fixture.GetQuizAnswer("Question", "Answer");

                await writer.WriteAnswer(quizAnswer);
                var nrOfAnswers = reader.NrOfAnswers;

                Check.That(nrOfAnswers).IsEqualTo(1);
            }

            [Test]
            public async void AfterTwoIdenticalAnswersIsWrittenNrOfAnswersIsTwo()
            {
                var reader = Fixture.CreateReader();
                var writer = Fixture.CreateWriter();

                var quizAnswer = Fixture.GetQuizAnswer("Question", "Answer");
                await writer.WriteAnswer(quizAnswer);
                await writer.WriteAnswer(quizAnswer);
                var nrOfAnswers = reader.NrOfAnswers;

                Check.That(nrOfAnswers).IsEqualTo(2);
            }

            [Test]
            public async void AllElementsOfAnswerIsAccessedAtLeastOnce()
            {
                var writer = Fixture.CreateWriter();

                var quizAnswer = Fixture.GetRandomCompleteQuizAnswer();
                await writer.WriteAnswer(quizAnswer.Object);

                quizAnswer.VerifyGet(_ => _.Question, Times.AtLeastOnce);
                quizAnswer.VerifyGet(_ => _.Answer, Times.AtLeastOnce);
                quizAnswer.VerifyGet(_ => _.Alternatives, Times.AtLeastOnce);
                quizAnswer.VerifyGet(_ => _.AnsweredCorrectly, Times.AtLeastOnce);
                quizAnswer.VerifyGet(_ => _.CorrectAnswerIndex, Times.AtLeastOnce);
            }
        }

        [TestFixture]
        public class WriteAnswerAndRemoveOld : TestBase
        {
            [Test]
            public async void AddsLatestAnswerAndRemovesOldestIfTooManyEqual()
            {
                var reader = Fixture.CreateReader();
                var writer = Fixture.CreateWriter();

                var maxEqualQuestionSaved = 5;
                var numberToDelete = 3;
                for (var i = 0; i < maxEqualQuestionSaved + numberToDelete; i++)
                {
                    var quizAnswer = Fixture.GetQuizAnswer("Question", "Answer", i);
                    await writer.WriteAnswerAndRemoveOld(quizAnswer, maxEqualQuestionSaved);
                }

                var readAnswers = reader.GetAllQuizAnswers();
                Check.That(readAnswers.Count).IsEqualTo(maxEqualQuestionSaved);
                for (var i = 0; i < maxEqualQuestionSaved; i++)
                {
                    Check.That(readAnswers[i].CorrectAnswerIndex).IsEqualTo(i + numberToDelete);
                }
            }
        }

        public class GetAllQuizAnswers : TestBase
        {
            [Test]
            public async void ReturnsExactlyOneAnswerWhenOneIsSaved()
            {
                var reader = Fixture.CreateReader();
                var writer = Fixture.CreateWriter();

                var quizAnswer = Fixture.GetRandomCompleteQuizAnswer();
                await writer.WriteAnswer(quizAnswer.Object);
                var readAnswers = reader.GetAllQuizAnswers();

                Check.That(readAnswers.Count).IsEqualTo(1);
            }

            [Test]
            public async void ReturnsExactlyTheSameListWrittenWhenRead()
            {
                var nrOfAnswers = 10;

                var reader = Fixture.CreateReader();
                var writer = Fixture.CreateWriter();

                var quizAnswers = Fixture.GetListOfRandomAnswers(nrOfAnswers);
                await writer.WriteAnswers(quizAnswers);
                var readAnswers = reader.GetAllQuizAnswers();

                Check.That(readAnswers.Count).IsEqualTo(nrOfAnswers);

                for (var i = 0; i < nrOfAnswers; i++)
                {
                    Check.That(quizAnswers[i].Question).IsEqualTo(readAnswers[i].Question);
                    Check.That(quizAnswers[i].Answer).IsEqualTo(readAnswers[i].Answer);
                    Check.That(quizAnswers[i].Alternatives.SequenceEqual(readAnswers[i].Alternatives)).IsTrue();
                    Check.That(quizAnswers[i].AnsweredCorrectly).IsEqualTo(readAnswers[i].AnsweredCorrectly);
                    Check.That(quizAnswers[i].CorrectAnswerIndex).IsEqualTo(readAnswers[i].CorrectAnswerIndex);
                }
            }
        }
    }
}
