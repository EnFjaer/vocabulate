﻿using NFluent;
using NUnit.Framework;
using Vocabulate.StyleHelper;

namespace Vocabulate.CommonViewModels.tests.ViewModels
{
    namespace AlternativeViewModelTests
    {
        public class BaseClass
        {
            protected AlternativeViewModelFixture Fixture;

            [SetUp]
            protected void RunBeforeAnyTest()
            {
                Fixture = new AlternativeViewModelFixture();
            }
        }


        [TestFixture]
        public class State : BaseClass
        {
            [TestCase(true, true, GuessState.NoGuess, AlternativeState.NoGuess)]
            [TestCase(true, false, GuessState.NoGuess, AlternativeState.NoGuess)]
            [TestCase(false, true, GuessState.NoGuess, AlternativeState.NoGuess)]
            [TestCase(false, false, GuessState.NoGuess, AlternativeState.NoGuess)]
            [TestCase(false, true, GuessState.Incorrect, AlternativeState.IncorrectGuess)]
            [TestCase(false, false, GuessState.Incorrect, AlternativeState.IncorrectGuessIncorrectAlternative)]
            [TestCase(true, false, GuessState.Incorrect, AlternativeState.IncorrectGuessCorrectAlternative)]
            [TestCase(true, true, GuessState.Correct, AlternativeState.CorrectGuessCorrectAlternative)]
            [TestCase(false, false, GuessState.Correct, AlternativeState.CorrectGuessIncorrectAlternative)]
            [TestCase(true, false, GuessState.Correct, AlternativeState.CorrectGuessCorrectAlternative)]
            public void ReturnsExpectedState(bool isCorrect, bool isGuess, GuessState guessState, AlternativeState expectedState)
            {
                var sut = Fixture.WithCorrectAnswer(isCorrect).CreateSut();

                if (isGuess)
                {
                    sut.Guessed();
                }

                sut.SetGuessState(guessState);

                Check.That(sut.State).IsEqualTo(expectedState);
            }
        }
    }
}