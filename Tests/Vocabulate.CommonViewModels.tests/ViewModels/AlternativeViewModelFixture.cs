﻿using Vocabulate.CommonViewModels.ViewModels;
using AlternativeViewModel = Vocabulate.TeachYourselfHindi.Core.ViewModels.AlternativeViewModel;

namespace Vocabulate.CommonViewModels.tests.ViewModels
{
    public class AlternativeViewModelFixture
    {
        private readonly string _alternative;

        private bool _isCorrect;

        public AlternativeViewModelFixture()
        {
            _alternative = "Dummy";
            _isCorrect = false;
        }

        public AlternativeViewModelFixture WithCorrectAnswer(bool isCorrect = true)
        {
            _isCorrect = isCorrect;
            return this;
        }

        public IAlternativeViewModel CreateSut()
        {
            return new AlternativeViewModel(_alternative, _isCorrect);
        }
    }
}
