﻿using System.Collections.Generic;
using System.IO;
using Moq;
using Vocabulate.QuizEngine.ChoosersAndGenerators;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.TeachYourselfHindi.Core.ViewModels;
using Vocabulate.TeachYourselfHindi.Core.Vocab;
using Vocabulate.TeachYourselfHindi.Core.VocabImporters;

namespace Vocabulate.CommonViewModels.tests.ViewModels
{
    internal class QuizViewModelFixture
    {
        private string[] _unit1Questions = {"अलग", "आठ", "उर्दू", "ख़ुश", "गंदा", "चप्पल", "चिट्ठी", "छात्र"};

        private string[] _unit1Answers =
            {"separate", "eight", "Urdu (f.)", "happy", "dirty", "sandal (f.)", "note (f.)", "student (m.)"};


        private string _unitPath;
        private string _unitName;
        private int _nrOfAlternatives;

        private Mock<IUnitLoader> _unitLoaderMock;

        private IQuestionCollection _unit;

        public List<IQuestionPair> _quizWords;


        public QuizViewModelFixture()
        {
            _unitPath = @"dummyPath";
            _unitName = "Unit 1";

            _nrOfAlternatives = 3;

            _unitLoaderMock = new Mock<IUnitLoader>();

            AnswerReceiver = new Mock<IAnswerReceiver>();
        }

        public Mock<IAnswerReceiver> AnswerReceiver { get; }

        public QuizViewModel CreateSut()
        {
            LoadQuizData();

            _unitLoaderMock.Setup((_) => _.Load(It.IsAny<string>(), It.IsAny<int>())).Returns(_quizWords);

            _unit = new Unit(_unitName, _unitPath, _unitLoaderMock.Object);

            var sut = new QuizViewModel();
            sut.Setup(_unit, AnswerReceiver.Object, _nrOfAlternatives, 100);
            return sut;
        }

        public QuizViewModelFixture WithUnitNr(int unitNr)
        {
            switch (unitNr)
            {
                case 1:
                    _unitName = "Unit 1";
                    break;

                case 2:
                    _unitName = "Unit 2";
                    break;

                default:
                    _unitPath = string.Empty;
                    _unitName = "Empty unit";
                    break;
            }
            return this;
        }

        public QuizViewModelFixture WithNumberOfAlternatives(int nrOfAlternatives)
        {
            _nrOfAlternatives = nrOfAlternatives;

            return this;
        }

        public bool IsValidQuestion(string question)
        {
            return _quizWords.Exists(_ => _.Question.Equals(question));
        }

        private void LoadQuizData()
        {
            _quizWords = new List<IQuestionPair>();
            if (_unitPath == string.Empty)
            {
                _quizWords = new List<IQuestionPair>();
                return;
            }

            for (var i = 0; i < _unit1Questions.Length; i++)
                _quizWords.Add(new QuestionPair(_unit1Questions[i], _unit1Answers[i]));
        }
    }
}
