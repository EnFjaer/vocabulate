﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Vocabulate.QuizEngine.ChoosersAndGenerators;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.Tests
{
    public class QuizMasterFixture
    {
        private static readonly Random Random = new Random();
        private int _nrOfQuestions;
        private int _nrOfAlternatives;
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }

        private IQuestionCollection _quizPage;


        public QuizMasterFixture()
        {
            _quizPage = new QuestionCollection("collection");
            AlternativeGenerator = new Mock<IAlternativeGenerator>();
            QuestionChooser = new Mock<IQuestionChooser>();
            QuizPairs = new List<IQuestionPair>();
            _nrOfQuestions = 10;
            _nrOfAlternatives = 6;
        }

        public QuizMaster CreateSut()
        {
            if (QuizPairs.Count == 0)
                GenerateQuizPage(_nrOfQuestions);

            SetupMocks();
            return new QuizMaster(AlternativeGenerator.Object, QuestionChooser.Object, _nrOfAlternatives);
        }

        public List<IQuestionPair> QuizPairs;
        public Mock<IAlternativeGenerator> AlternativeGenerator;
        public Mock<IQuestionChooser> QuestionChooser;

        public QuizMasterFixture WithNrOfQuestions(int nr)
        {
            _nrOfQuestions = nr;
            GenerateQuizPage(_nrOfQuestions);

            return this;
        }

        public QuizMasterFixture WithNrOfAlternatives(int nrOfAlternatives)
        {
            _nrOfAlternatives = nrOfAlternatives;
            return this;
        }

        public QuizMasterFixture DuplicateQuestionsWithSimilarAnswers()
        {
            if (QuizPairs.Count == 0)
                GenerateQuizPage(_nrOfQuestions);

            for (var i = 1; i < _nrOfQuestions; i++)
            {
                var duplicateQuestion = new QuestionPair(QuizPairs[i].Question, QuizPairs[i - 1].Answer);
                QuizPairs.Add(duplicateQuestion);
            }
            return this;
        }

        private void SetupMocks()
        {
            _quizPage.ToList().AddRange(QuizPairs);
            QuestionChooser.Setup(_ => _.GetQuestion()).Returns(QuizPairs[0]);
            AlternativeGenerator.Setup(_ => _.GenerateAlternatives(It.IsAny<IQuestionPair>()))
                .Returns(GenerateRandomStringList(_nrOfAlternatives));
            AlternativeGenerator.Setup(_ => _.FindCorrectAnswer(It.IsAny<List<string>>())).Returns(0);
        }

        private List<string> GenerateRandomStringList(int length)
        {
            var list = new List<string>();
            for (var i = 0; i < length; i++)
            {
                list.Add(RandomString(10));
            }
            return list;
        }

        private void GenerateQuizPage(int length)
        {
            QuizPairs.Clear();
            for(var i = 0; i < length; i++)
                QuizPairs.Add(new QuestionPair(RandomString(10), RandomString(10)));
        }
    }
}
