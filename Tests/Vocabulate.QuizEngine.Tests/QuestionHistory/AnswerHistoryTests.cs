﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using NFluent;
using NUnit.Framework;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.Tests.QuestionHistory
{
    namespace AnswerHistoryTests
    {
        public class TestBase
        {
            protected AnswerHistoryFixture Fixture;

            [SetUp]
            public void RunBeforeAnyTest()
            {
                Fixture = new AnswerHistoryFixture();
            }
        }

        [TestFixture]
        public class NumberOfTimesAsked : TestBase
        {
            [Test]
            public void ReturnsZeroWhenNoAnswersHasBeenGiven()
            {
                var sut = Fixture.CreateSut();

                var question = Fixture.GetRandomQuestion();

                Check.That(sut.NumberOfTimesAsked(question)).IsEqualTo(0);
            }

            [Test]
            public async Task ReturnsOneIfOneAnswerHasBeenGiven()
            {
                var sut = Fixture.CreateSut();

                var answer = Fixture.GetRandomQuizAnswer();
                var question = (IQuizQuestion) answer;

                await sut.AddAnswer(answer);

                Check.That(sut.NumberOfTimesAsked(question)).IsEqualTo(1);
            }

            [Test]
            public async Task ReturnsOneIfOneAnswerToThatQuestionIsGiven()
            {
                var sut = Fixture.CreateSut();

                var answers = Fixture.GetRandomQuizAnswers(10);
                var question = (IQuizQuestion)answers[0];

                await sut.AddAnswers(answers);

                Check.That(sut.NumberOfTimesAsked(question)).IsEqualTo(1);
            }

            [TestCase(0)]
            [TestCase(3)]
            [TestCase(10)]
            public async Task ReturnsCorrectNumberWhenSeveralAnswersIsGiven(int nrOfAnswers)
            {
                var sut = Fixture.CreateSut();

                var answers = Fixture.GetRandomQuizAnswers(10);

                var question = Fixture.GetRandomQuestion();

                for (var i = 0; i < nrOfAnswers; i++)
                {
                    answers.Add(new QuizAnswer(question, true, string.Empty));
                }

                await sut.AddAnswers(answers);

                Check.That(sut.NumberOfTimesAsked(question)).IsEqualTo(nrOfAnswers);
            }

            [TestCase(3, 2)]
            [TestCase(10, 5)]
            [TestCase(100, 5)]

            public async Task NeverReturnsMoreThanMaxNrOfAnswers(int nrOfAnswers, int maxNrOfAnswers)
            {
                var sut = Fixture.WithMaxNrOfAnswers(maxNrOfAnswers).CreateSut();

                var answers = Fixture.GetRandomQuizAnswers(10);

                var question = Fixture.GetRandomQuestion();

                for (var i = 0; i < nrOfAnswers; i++)
                {
                    answers.Add(new QuizAnswer(question, true, string.Empty));
                }

                await sut.AddAnswers(answers);

                Check.That(sut.NumberOfTimesAsked(question)).IsLessThan(maxNrOfAnswers + 1);
            }
        }

        [TestFixture]
        public class OutcomeOfLastAnswers : TestBase
        {
            [Test]
            public void ReturnsEmptyListIfNoAnswersHasBeenGiven()
            {
                var sut = Fixture.CreateSut();

                var question = Fixture.GetRandomQuestion();

                Check.That(sut.OutcomeOfLastAnswers(question)).IsEmpty();
            }

            [Test]
            public async Task ReturnsListWithOneTrueElementIfOneCorrectAnswerIsGiven()
            {
                var sut = Fixture.CreateSut();

                var answer = Fixture.GetRandomQuizAnswer(true);
                var question = (IQuizQuestion)answer;

                await sut.AddAnswer(answer);

                Check.That(sut.OutcomeOfLastAnswers(question)).HasSize(1);
                Check.That(sut.OutcomeOfLastAnswers(question)[0]).IsTrue();
            }

            [Test]
            public async Task ReturnsListWithOneFalseElementIfOneCorrectAnswerIsGiven()
            {
                var sut = Fixture.CreateSut();

                var answer = Fixture.GetRandomQuizAnswer(false);
                var question = (IQuizQuestion)answer;

                await sut.AddAnswer(answer);

                Check.That(sut.OutcomeOfLastAnswers(question)).HasSize(1);
                Check.That(sut.OutcomeOfLastAnswers(question)[0]).IsFalse();
            }

            [TestCase(new []{true})]
            [TestCase(new []{false, true, true, false, true})]
            [TestCase(new []{false, false, true, false, true, false, true, true, false, true, true})]
            public async Task ReturnsCorrectListOfBools(bool[] correctArray)
            {
                var sut = Fixture.CreateSut();

                var answers = Fixture.GetRandomQuizAnswers(10);

                var question = Fixture.GetRandomQuestion();

                foreach (bool t in correctArray)
                {
                    answers.Add(new QuizAnswer(question, t, string.Empty));
                }

                await sut.AddAnswers(answers);

                Check.That(sut.OutcomeOfLastAnswers(question)).ContainsExactly(correctArray);
            }

            [TestCase(1)]
            [TestCase(5)]
            [TestCase(8)]
            public async Task OnlyReturnsTheLastNBoolsWhenMaxNrIsSet(int maxNrOfAnswers)
            {
                var correctArray = new List<bool>{false, false, true, false, true, false, true, true, false, true, true};
                var sut = Fixture.WithMaxNrOfAnswers(maxNrOfAnswers).CreateSut();

                var answers = Fixture.GetRandomQuizAnswers(10);

                var question = Fixture.GetRandomQuestion();

                foreach (bool t in correctArray)
                {
                    answers.Add(new QuizAnswer(question, t, string.Empty));
                }

                await sut.AddAnswers(answers);

                var lastPartOfArray = correctArray.GetRange(correctArray.Count - maxNrOfAnswers, maxNrOfAnswers);

                Check.That(sut.OutcomeOfLastAnswers(question)).ContainsExactly(lastPartOfArray);
            }
        }

        [TestFixture]
        public class NumberOfCorrectAnswers : TestBase
        {
            [TestCase(new bool[] { })]
            [TestCase(new[] {true})]
            [TestCase(new[] {false, true, true, false, true})]
            [TestCase(new[] {false, false, true, false, true, false, true, true, false, true, true})]
            public async Task ReturnsCorrectNrOfCorrectAnswers(bool[] correctArray)
            {
                var sut = Fixture.CreateSut();

                var answers = Fixture.GetRandomQuizAnswers(10);
                var question = Fixture.GetRandomQuestion();

                var nrOfCorrect = 0;

                for (var i = 0; i < correctArray.Length; i++)
                {
                    answers.Add(new QuizAnswer(question, correctArray[i], string.Empty));
                    if (correctArray[i])
                        nrOfCorrect++;
                }

                await sut.AddAnswers(answers);

                Check.That(sut.NumberOfCorrectAnswers(question)).IsEqualTo(nrOfCorrect);
            }
        }

        [TestFixture]
        public class NumberOfIncorrectAnswers : TestBase
        {
            [TestCase(new bool[] { })]
            [TestCase(new[] { true })]
            [TestCase(new[] { false, true, true, false, true })]
            [TestCase(new[] { false, false, true, false, true, false, true, true, false, true, true })]
            public async Task ReturnsCorrectNrOfIncorrectAnswers(bool[] correctArray)
            {
                var sut = Fixture.CreateSut();

                var answers = Fixture.GetRandomQuizAnswers(10);
                var question = Fixture.GetRandomQuestion();

                var nrOfIncorrect = 0;

                for (var i = 0; i < correctArray.Length; i++)
                {
                    answers.Add(new QuizAnswer(question, correctArray[i], string.Empty));
                    if (!correctArray[i])
                        nrOfIncorrect++;
                }

                await sut.AddAnswers(answers);

                Check.That(sut.NumberOfIncorrectAnswers(question)).IsEqualTo(nrOfIncorrect);
            }
        }

        [TestFixture]
        public class AddAnswer : TestBase
        {
            [Test]
            public async Task CallsAnswerWriter()
            {
                var sut = Fixture.CreateSut();
                var answer = Fixture.GetRandomQuizAnswer();

                await sut.AddAnswer(answer);

                Fixture.AnswerWriter.Verify(_ => _.WriteAnswerAndRemoveOld(answer, int.MaxValue));
            }

            [Test]
            public async Task CallsAnswerWriterWithMaxNrOfAnswers()
            {
                var sut = Fixture.WithMaxNrOfAnswers(5).CreateSut();
                var answer = Fixture.GetRandomQuizAnswer();

                await sut.AddAnswer(answer);

                Fixture.AnswerWriter.Verify(_ => _.WriteAnswerAndRemoveOld(answer, 5));
            }
        }

        [TestFixture]
        public class AddAnswers : TestBase
        {
            [Test]
            public async Task CallsAnswerWriterOnceForEveryAnswer()
            {
                var sut = Fixture.CreateSut();
                var answers = Fixture.GetRandomQuizAnswers(10);

                await sut.AddAnswers(answers);

                Fixture.AnswerWriter.Verify(_ => _.WriteAnswerAndRemoveOld(It.IsAny<IQuizAnswer>(), int.MaxValue),Times.Exactly(10));
            }
        }

        [TestFixture]
        public class AnswerReader : TestBase
        {
            [Test]
            public void ReadsAnswersFromReader()
            {
                var answers = Fixture.GetRandomQuizAnswers(10);
                var sut = Fixture.WithAnswersInDatabase(answers).CreateSut();

                Fixture.AnswerReader.Verify(_ => _.GetAllQuizAnswers(), Times.AtLeastOnce);
            }

            [Test]
            public void AnswersReadIsInHistory()
            {
                var answers = Fixture.GetRandomQuizAnswers(10);
                var sut = Fixture.WithAnswersInDatabase(answers).CreateSut();

                foreach (var answer in answers)
                    Check.That(sut.NumberOfCorrectAnswers(answer)).IsEqualTo(1);
            }
        }

        [TestFixture]
        public class TimeSinceLastAsked : TestBase
        {
            [Test]
            public void ReturnsMaxIntWhenNotAsked()
            {
                var sut = Fixture.CreateSut();
                var answer = Fixture.GetRandomQuizAnswer();

                var askedAgo = sut.NrQuestionsSinceLastAsked(answer);

                Check.That(askedAgo).IsEqualTo(int.MaxValue);
            }

            [Test]
            public async Task ReturnsZeroIfJustAsked()
            {
                var sut = Fixture.CreateSut();
                var answer = Fixture.GetRandomQuizAnswer();
                await sut.AddAnswer(answer);

                var askedAgo = sut.NrQuestionsSinceLastAsked(answer);

                Check.That(askedAgo).IsEqualTo(0);
            }

            [TestCase(0)]
            [TestCase(1)]
            [TestCase(5)]
            [TestCase(10)]
            public async Task ReturnsNrIfNrQuestionAskedAfter(int nrAsked)
            {
                var sut = Fixture.CreateSut();
                var answer = Fixture.GetRandomQuizAnswer();
                await sut.AddAnswer(answer);

                var extraAnswers = Fixture.GetRandomQuizAnswers(nrAsked);
                for(var i = 0; i < extraAnswers.Count; i++)
                {
                    await sut.AddAnswer(extraAnswers[i]);
                }

                var askedAgo = sut.NrQuestionsSinceLastAsked(answer);

                Check.That(askedAgo).IsEqualTo(nrAsked);
            }
        }

        [TestFixture]
        public class LastTimeAsked : TestBase
        {
            [Test]
            public async Task IfNotAskedReturnsMinValue()
            {
                var sut = Fixture.CreateSut();
                var answer = Fixture.GetRandomQuizAnswer();
                var answerTime = sut.LastTimeAsked(answer);

                Check.That(answerTime).Equals(DateTime.MinValue);
            }

            [Test]
            public async Task ReturnsDateThatIsEarlierThanNow()
            {
                var sut = Fixture.CreateSut();
                var answer = Fixture.GetRandomQuizAnswer();
                await sut.AddAnswer(answer);
                await Task.Delay(10);

                var answerTime = sut.LastTimeAsked(answer);
                var timeSpan = answerTime - DateTime.UtcNow;

                Check.That(timeSpan.TotalMilliseconds).IsLessThan(0);
            }

            [Test]
            public async Task ReturnsDateThatIsLaterThanBeforeAsking()
            {
                var sut = Fixture.CreateSut();
                var beforeAskTime = DateTime.UtcNow;                
                await Task.Delay(10);

                var answer = Fixture.GetRandomQuizAnswer();
                await sut.AddAnswer(answer);

                var answerTime = sut.LastTimeAsked(answer);
                var timeSpan = answerTime - beforeAskTime;

                Check.That(timeSpan.TotalMilliseconds).IsGreaterThan(0);
            }
        }
    }
}
