﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Moq;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.Database;
using Vocabulate.QuizEngine.QuestionHistory;

namespace Vocabulate.QuizEngine.Tests.QuestionHistory
{
    public class AnswerReaderWriterFixture
    {
        private static readonly Random Rnd = new Random();

        private IAnswerDatabaseConnection _connection;
        private readonly string _path;
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[Rnd.Next(s.Length)]).ToArray());
        }


        public AnswerReaderWriterFixture()
        {
            var path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            _path = Path.Combine(path, "testDatabase3.db3");
            _connection = new AnswerDatabaseConnection(_path);

            _connection.DropDatabase();
            _connection.CreateDatabase();

        }

        public AnswerReader CreateReader()
        {
            return new AnswerReader(_connection);
        }

        public AnswerWriter CreateWriter()
        {
            return new AnswerWriter(_connection);
        }

        public IQuizAnswer GetQuizAnswer(string question, string correctAnswer, int correctIndex = 0)
        {
            var quizAnswer = SetupQuizAnswer(question, correctAnswer, new List<string>(), correctIndex: correctIndex);
            return quizAnswer.Object;
        }

        public Mock<IQuizAnswer> GetRandomCompleteQuizAnswer(int nrOfAlternatives = 6)
        {
            var alternatives = GetRandomListOfStrings(nrOfAlternatives);
            var correctIndex = Rnd.Next(0, nrOfAlternatives);
            var answer = alternatives[correctIndex];
            var question = RandomString(10);
            return SetupQuizAnswer(question, answer, alternatives, true, correctIndex);
        }

        public List<IQuizAnswer> GetListOfRandomAnswers(int length)
        {
            var answers = new List<IQuizAnswer>();

            for (var i = 0; i < length; i++)
            {
                answers.Add(GetRandomCompleteQuizAnswer().Object);
            }

            return answers;
        }

        private Mock<IQuizAnswer> SetupQuizAnswer(
            string question, 
            string correctAnswer, 
            List<string> alternatives, 
            bool wasCorrect = true,
            int correctIndex = 0)
        {
            var quizAnswer = new Mock<IQuizAnswer>();
            quizAnswer.SetupGet(_ => _.Question).Returns(question);
            quizAnswer.SetupGet(_ => _.Answer).Returns(correctAnswer);
            quizAnswer.SetupGet(_ => _.Alternatives).Returns(alternatives);
            quizAnswer.SetupGet(_ => _.AnsweredCorrectly).Returns(wasCorrect);
            quizAnswer.SetupGet(_ => _.CorrectAnswerIndex).Returns(correctIndex);

            return quizAnswer;
        }

        private List<string> GetRandomListOfStrings(int length)
        {
            var outList = new List<string>();
            for (var i = 0; i < length; i++)
            {
                outList.Add(RandomString(10));
            }

            return outList;
        }
    }
}