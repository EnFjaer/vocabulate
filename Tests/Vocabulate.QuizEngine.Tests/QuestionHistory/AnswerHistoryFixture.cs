﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.QuestionHistory;

namespace Vocabulate.QuizEngine.Tests.QuestionHistory
{
    public class AnswerHistoryFixture
    {
        private static readonly Random Rnd = new Random();

        private IList<IQuizAnswer> _loadAnswers = new List<IQuizAnswer>();

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[Rnd.Next(s.Length)]).ToArray());
        }

        private int _maxNrOfAnswers = int.MaxValue;

        public Mock<IAnswerWriter> AnswerWriter = new Mock<IAnswerWriter>();
        public Mock<IAnswerReader> AnswerReader = new Mock<IAnswerReader>();

        public AnswerHistory CreateSut()
        {
            AnswerWriter.Setup(_ => _.WriteAnswerAndRemoveOld(It.IsAny<IQuizAnswer>(), It.IsAny<int>())).Returns(Task.FromResult("String from test"));
            AnswerReader.Setup(_ => _.GetAllQuizAnswers()).Returns(_loadAnswers);

            return new AnswerHistory(AnswerWriter.Object, AnswerReader.Object, _maxNrOfAnswers);
        }

        public IQuizQuestion GetRandomQuestion()
        {
            var answer = new Mock<IQuizQuestion>();
            answer.SetupGet(_ => _.Question).Returns(RandomString(10));
            answer.SetupGet(_ => _.Answer).Returns(RandomString(10));
            return answer.Object;
        }

        public IQuizAnswer GetRandomQuizAnswer(bool isCorrect = true)
        {
            var answer = new Mock<IQuizAnswer>();
            var now = DateTime.UtcNow;
            answer.SetupGet(_ => _.Question).Returns(RandomString(10));
            answer.SetupGet(_ => _.Answer).Returns(RandomString(10));
            answer.SetupGet(_ => _.AnsweredCorrectly).Returns(isCorrect);
            answer.SetupGet(_ => _.TimeAnswered).Returns(now);
            return answer.Object;
        }

        public List<IQuizAnswer> GetRandomQuizAnswers(int nrOfAnswers = 2)
        {
            var answerList = new List<IQuizAnswer>();
            for (var i = 0; i < nrOfAnswers; i++)
            {
                answerList.Add(GetRandomQuizAnswer());
            }

            return answerList;
        }

        public AnswerHistoryFixture WithMaxNrOfAnswers(int maxNr)
        {
            _maxNrOfAnswers = maxNr;
            return this;
        }

        public AnswerHistoryFixture WithAnswersInDatabase(IList<IQuizAnswer> answers)
        {
            _loadAnswers = answers;
            return this;
        }
    }
}
