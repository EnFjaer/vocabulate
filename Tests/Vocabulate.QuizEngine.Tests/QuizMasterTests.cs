﻿using Moq;
using NUnit.Framework;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.Tests
{
    namespace QuizMasterTests
    {
        internal class TestBase
        {
            protected QuizMasterFixture Fixture;

            [SetUp]
            public void RunBeforeAnyTest()
            {
                Fixture = new QuizMasterFixture();
            }
        }

        [TestFixture]
        internal class GetQuestion : TestBase
        {
            [Test]
            public void SetsNrOfAlternativesToAlternativeGenerator()
            {
                var sut = Fixture.WithNrOfAlternatives(10).CreateSut();

                Fixture.AlternativeGenerator.VerifySet(_ => _.NrOfAlternatives = 10, Times.Once);
            }
        }
    }
}