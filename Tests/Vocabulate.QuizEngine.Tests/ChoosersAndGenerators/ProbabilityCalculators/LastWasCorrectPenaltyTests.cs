﻿using System.Linq;
using NFluent;
using NUnit.Framework;

namespace Vocabulate.QuizEngine.Tests.ChoosersAndGenerators.ProbabilityCalculators
{
    namespace LastWasCorrectPenaltyTests
    {
        internal class TestBase
        {
            protected LastWasCorrectPenaltyFixture Fixture;

            [SetUp]
            public void RunBeforeAnyTest()
            {
                Fixture = new LastWasCorrectPenaltyFixture();
            }
        }

        [TestFixture]
        internal class CalculateProbability : TestBase
        {
            [Test]
            public void ReturnsOnesIfNoQuestionsAreAsked()
            {
                var sut = Fixture.CreateSut();

                var nrQuestions = 4;
                var questions = Fixture.GetQuestionCollection(nrQuestions);

                var probabilities = sut.CalculateProbability(questions);

                Check.That(probabilities.Values.ToList().All(_ => _ == 1)).IsTrue();
            }

            [Test]
            public void ReturnsModifiedValueForQuestionsWichWasAnsweredCorrectlyLast()
            {
                var nrQuestions = 4;
                var questions = Fixture.GetQuestionCollection(nrQuestions);

                Fixture.SetLastAskedForQuestion(questions[0], true);

                var sut = Fixture.CreateSut();

                var probabilities = sut.CalculateProbability(questions);
                Check.That(probabilities[questions[0]]).IsEqualTo(0.5);
                for (var i = 1; i < nrQuestions; i++)
                    Check.That(probabilities[questions[i]]).IsEqualTo(1);
            }
        }
    }
}
