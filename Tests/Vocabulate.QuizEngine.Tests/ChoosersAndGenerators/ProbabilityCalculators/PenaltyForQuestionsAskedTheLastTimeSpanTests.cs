﻿using System;
using System.Linq;
using NFluent;
using NUnit.Framework;

namespace Vocabulate.QuizEngine.Tests.ChoosersAndGenerators.ProbabilityCalculators
{
    namespace PenaltyForQuestionsAskedTheLastTimeSpanTests
    {
        internal class TestBase
        {
            protected PenaltyForQuestionsAskedTheLastTimeSpanFixture Fixture;

            [SetUp]
            public void RunBeforeAnyTest()
            {
                Fixture = new PenaltyForQuestionsAskedTheLastTimeSpanFixture();
            }
        }

        [TestFixture]
        internal class CalculateProbability : TestBase
        {
            [Test]
            public void ReturnsOnesWhenNoQuestionsHaveBeenAsked()
            {
                var sut = Fixture.CreateSut();
                var questions = Fixture.GetQuestionCollection(10);
                var probabilities = sut.CalculateProbability(questions);

                foreach (var question in questions)
                {
                    Check.That(probabilities[question]).IsEqualTo(1);
                }
            }

            [Test]
            public void ReturnsSetPenaltyIfQuestionAskedWithinLastNMinutes()
            {
                var questions = Fixture.GetQuestionCollection(10);
                Fixture.SetLastTimeAsked(questions[4], DateTime.Now);
                var sut = Fixture.WithPenaltyValue(0.5).CreateSut();

                var probabilities = sut.CalculateProbability(questions);

                Check.That(probabilities[questions[4]]).IsEqualTo(0.5);
                for (var i = 0; i < questions.Count; i++)
                    if (i != 4)
                        Check.That(probabilities[questions[i]]).IsEqualTo(1);
            }

            [Test]
            public void ReturnsOneIfQuestionAskedLongerAgoThanNMinutes()
            {
                var questions = Fixture.GetQuestionCollection(10);
                Fixture.SetLastTimeAsked(questions[5], DateTime.Now - TimeSpan.FromMinutes(20));
                var sut = Fixture.WithPenaltyValue(0.5).WithPenaltyTime(TimeSpan.FromMinutes(10)).CreateSut();

                var probabilities = sut.CalculateProbability(questions);

                Check.That(probabilities[questions[5]]).IsEqualTo(1);
                for (var i = 0; i < questions.Count; i++)
                    if (i != 5)
                        Check.That(probabilities[questions[i]]).IsEqualTo(1);
            }
        }
    }
}