﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vocabulate.QuizEngine.ChoosersAndGenerators.ProbabilityCalculators;

namespace Vocabulate.QuizEngine.Tests.ChoosersAndGenerators.ProbabilityCalculators
{
    public class PenaltyForQuestionsAskedTheLastTimeSpanFixture : BaseProbabilityCalculatorFixture
    {
        private double _penaltyValue = 0.5;
        private TimeSpan _penaltyTime;

        public PenaltyForQuestionsAskedTheLastTimeSpanFixture()
        {
            _penaltyTime = TimeSpan.FromMinutes(3);
        }

        public PenaltyForQuestionsAskedTheLastTimeSpan CreateSut()
        {
            return new PenaltyForQuestionsAskedTheLastTimeSpan(AnswerHistoryMock.Object, _penaltyValue, _penaltyTime);
        }

        public PenaltyForQuestionsAskedTheLastTimeSpanFixture WithPenaltyValue(double value)
        {
            _penaltyValue = value;
            return this;
        }

        public PenaltyForQuestionsAskedTheLastTimeSpanFixture WithPenaltyTime(TimeSpan value)
        {
            _penaltyTime = value;
            return this;
        }
    }
}
