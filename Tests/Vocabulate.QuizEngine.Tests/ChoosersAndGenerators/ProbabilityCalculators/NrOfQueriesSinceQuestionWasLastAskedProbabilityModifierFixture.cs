﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vocabulate.QuizEngine.ChoosersAndGenerators.ProbabilityCalculators;

namespace Vocabulate.QuizEngine.Tests.ChoosersAndGenerators.ProbabilityCalculators
{
    public class NrOfQueriesSinceQuestionWasLastAskedProbabilityModifierFixture : BaseProbabilityCalculatorFixture
    {
        private double _valueAtCollectionSize = 2;
        private double _maxValue = 3;

        public NrOfQueriesSinceQuestionWasLastAskedProbabilityModifier CreateSut()
        {
            return new NrOfQueriesSinceQuestionWasLastAskedProbabilityModifier(AnswerHistoryMock.Object, _valueAtCollectionSize, _maxValue);
        }

        public NrOfQueriesSinceQuestionWasLastAskedProbabilityModifierFixture WithValueAtCollectionSize(double value)
        {
            _valueAtCollectionSize = value;
            return this;
        }

        public NrOfQueriesSinceQuestionWasLastAskedProbabilityModifierFixture WithMaxValue(double value)
        {
            _maxValue = value;
            return this;
        }
    }
}
