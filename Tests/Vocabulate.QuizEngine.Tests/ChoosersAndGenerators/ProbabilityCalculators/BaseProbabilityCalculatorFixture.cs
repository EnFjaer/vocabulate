﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.QuestionHistory;

namespace Vocabulate.QuizEngine.Tests.ChoosersAndGenerators.ProbabilityCalculators
{
    public class BaseProbabilityCalculatorFixture
    {
        protected Mock<IAnswerHistory> AnswerHistoryMock;

        public BaseProbabilityCalculatorFixture()
        {
            SetupDefaultAnswerHistoryBehavior();
        }

        public IQuestionCollection GetQuestionCollection(int nr)
        {
            var questionCollection = new QuestionCollection("Test collection");
            for (var i = 0; i < nr; i++)
            {
                questionCollection.Add(new QuestionPair("Question " + i, "Answer " + i, tier: i));
            }

            return questionCollection;
        }

        public void SetLastAskedForQuestion(IQuestionPair question, bool wasCorrect)
        {
            AnswerHistoryMock.Setup(_ => _.OutcomeOfLastAnswer(question)).Returns(wasCorrect);
        }

        public void SetNrQuestionsSinceLastAsked(IQuestionPair question, int nrAskedAgo)
        {
            AnswerHistoryMock.Setup(_ => _.NrQuestionsSinceLastAsked(question)).Returns(nrAskedAgo);
        }

        public void SetLastTimeAsked(IQuestionPair question, DateTime askedTime)
        {
            AnswerHistoryMock.Setup(_ => _.LastTimeAsked(question)).Returns(askedTime);
        }

        private void SetupDefaultAnswerHistoryBehavior()
        {
            AnswerHistoryMock = new Mock<IAnswerHistory>();
            AnswerHistoryMock.Setup(_ => _.OutcomeOfLastAnswer(It.IsAny<IQuestionPair>())).Returns(false);
            AnswerHistoryMock.Setup(_ => _.NrQuestionsSinceLastAsked(It.IsAny<IQuestionPair>())).Returns(int.MaxValue);
            AnswerHistoryMock.Setup(_ => _.LastTimeAsked(It.IsAny<IQuestionPair>())).Returns(DateTime.MinValue);
        }
    }
}
