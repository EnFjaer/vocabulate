﻿using Moq;
using Vocabulate.QuizEngine.ChoosersAndGenerators.ProbabilityCalculators;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.QuestionHistory;

namespace Vocabulate.QuizEngine.Tests.ChoosersAndGenerators.ProbabilityCalculators
{
    public class LastWasCorrectPenaltyFixture : BaseProbabilityCalculatorFixture
    {
        public LastWasCorrectPenalty CreateSut()
        {
            return new LastWasCorrectPenalty(AnswerHistoryMock.Object);
        }
    }
}

