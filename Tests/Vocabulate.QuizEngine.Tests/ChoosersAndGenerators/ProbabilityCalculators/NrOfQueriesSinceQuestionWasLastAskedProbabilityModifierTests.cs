﻿using System.Linq;
using NFluent;
using NUnit.Framework;

namespace Vocabulate.QuizEngine.Tests.ChoosersAndGenerators.ProbabilityCalculators
{
    namespace NrOfQueriesSinceQuestionWasLastAskedProbabilityModifierTests
    {
        internal class TestBase
        {
            protected NrOfQueriesSinceQuestionWasLastAskedProbabilityModifierFixture Fixture;

            [SetUp]
            public void RunBeforeAnyTest()
            {
                Fixture = new NrOfQueriesSinceQuestionWasLastAskedProbabilityModifierFixture();
            }
        }

        internal class CalculateProbability : TestBase
        {
            [Test]
            public void ReturnsMaxValueIfNoQuestionsAreAsked()
            {
                var sut = Fixture.WithMaxValue(2).CreateSut();

                var nrQuestions = 4;
                var questions = Fixture.GetQuestionCollection(nrQuestions);

                var probabilities = sut.CalculateProbability(questions);
                foreach(var probability in probabilities.Values.ToList())
                    Check.That(probability).IsEqualTo(2);
            }

            [Test]
            public void ReturnsLinearlyOrderedProbabilitiesBasedOnAskedAgo()
            {

                var nrQuestions = 4;
                var questions = Fixture.GetQuestionCollection(nrQuestions);
                Fixture.SetNrQuestionsSinceLastAsked(questions[1], 7);
                var sut = Fixture.WithMaxValue(10).WithValueAtCollectionSize(3).CreateSut();
                var expectedValue = 7.0 / 4.0 * (3.0 - 1.0) + 1.0;

                var probabilities = sut.CalculateProbability(questions);

                Check.That(probabilities[questions[1]]).IsEqualTo(expectedValue);

            }
        }
    }
}
