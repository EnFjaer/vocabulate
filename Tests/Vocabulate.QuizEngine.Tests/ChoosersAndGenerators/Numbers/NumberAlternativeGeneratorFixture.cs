﻿using System.Collections.Generic;
using System.Linq;
using Moq;
using Vocabulate.QuizEngine.ChoosersAndGenerators;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.Tests.ChoosersAndGenerators.Numbers
{
    public class NumberAlternativeGeneratorFixture
    {
        private int _nrOfAlternatives;

        private Mock<IQuestionChooser> _questionChooser;

        private int _currentTier;

        private bool _fliped;

        public NumberAlternativeGeneratorFixture()
        {
            _fliped = false;
            _currentTier = 3;
            _nrOfAlternatives = 8;
            QuestionCollection = NumberTestHelper.CreateNumbers();
            _questionChooser = new Mock<IQuestionChooser>();
        }

        public IQuestionCollection QuestionCollection { get; set; }

        public AlternativeGenerator CreateSut()
        {
            _questionChooser.SetupGet(_ => _.CurrentTier).Returns(_currentTier);
            QuestionCollection.FlipQuestionAndAnswer = _fliped;
            return new AlternativeGenerator(_questionChooser.Object, QuestionCollection, _nrOfAlternatives);
        }

        public NumberAlternativeGeneratorFixture WithNrOfAlternatives(int nrOfAlternatives)
        {
            _nrOfAlternatives = nrOfAlternatives;
            return this;
        }

        public NumberAlternativeGeneratorFixture WithQuestionsFromTier(int tier)
        {
            _currentTier = tier;
            return this;
        }

        public NumberAlternativeGeneratorFixture WithFlipedQuestions()
        {
            _fliped = true;
            return this;
        }

        public NumberAlternativeGeneratorFixture WithQuestionPairs(List<IQuestionPair> addedPairs)
        {
            QuestionCollection.Clear();
            QuestionCollection.AddRange(addedPairs);
            return this;
        }
    }
}
