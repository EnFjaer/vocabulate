﻿using System;
using System.Collections.Generic;
using System.Linq;
using NFluent;
using NUnit.Framework;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.Tests.ChoosersAndGenerators.Numbers
{
    namespace NumberAlternativeGeneratorTests
    {

        internal class TestBase
        {
            protected readonly Random Rnd = new Random();

            protected NumberAlternativeGeneratorFixture Fixture;

            [SetUp]
            public void RunBeforeAnyTest()
            {
                Fixture = new NumberAlternativeGeneratorFixture();
            }
        }

        [TestFixture]
        internal class NrOfAlternatives : TestBase
        {
            [TestCase(1)]
            [TestCase(2)]
            [TestCase(6)]
            [TestCase(10)]
            public void ReturnsCorrectNumberOfAlternatives(int nrOfAlternatives)
            {
                var sut = Fixture.CreateSut();

                sut.NrOfAlternatives = nrOfAlternatives;

                Check.That(sut.GenerateAlternatives(Fixture.QuestionCollection[0]).Count).IsEqualTo(nrOfAlternatives);
            }
        }

        [TestFixture]
        internal class GenerateAlternatives : TestBase
        {
            [Test]
            public void ReturnsAlternativesIncludingCorrectAnswer()
            {
                var sut = Fixture.CreateSut();

                var questionId = Rnd.Next(0, Fixture.QuestionCollection.Count);

                var question = Fixture.QuestionCollection[questionId];
                var alternatives = sut.GenerateAlternatives(question);

                Check.That(alternatives).Contains(question.Answer);
            }

            [Test]
            public void ReturnsUniqueAlternatives()
            {
                var sut = Fixture.CreateSut();

                var alternatives = sut.GenerateAlternatives(Fixture.QuestionCollection[0]);

                Check.That(alternatives.Count).IsEqualTo(alternatives.Distinct().Count());
            }

            [Test]
            public void ReturnsOnlyAlternativesFromTheCollection()
            {
                var sut = Fixture.CreateSut();

                var alternatives = sut.GenerateAlternatives(Fixture.QuestionCollection[0]);

                foreach (var alternative in alternatives)
                {
                    Check.That(Fixture.QuestionCollection.Select(_ => _.Answer)).Contains(alternative);
                }
            }

            [TestCase(2)]
            [TestCase(6)]
            [TestCase(12)]
            public void ReturnsCorrectAlternativeAtDifferentPositions(int nrOfAlternatives)
            {
                var reps = nrOfAlternatives * 10;

                var correctAlternativePosition = new int[nrOfAlternatives];

                var sut = Fixture.WithNrOfAlternatives(nrOfAlternatives).CreateSut();

                for(var rep = 0; rep < reps; rep++)
                {
                    var questionId = Rnd.Next(0, Fixture.QuestionCollection.Count);

                    var alternatives = sut.GenerateAlternatives(Fixture.QuestionCollection[questionId]);

                    correctAlternativePosition[sut.FindCorrectAnswer(alternatives)]++;
                }

                foreach (var alternativePosition in correctAlternativePosition)
                {
                    Check.That(alternativePosition).IsGreaterThan(0);
                }
            }

            [TestCase(1)]
            [TestCase(2)]
            [TestCase(3)]
            public void ReturnsOnlyAlternativesFromCurrentTierOrLower(int tier)
            {
                var sut = Fixture.WithQuestionsFromTier(tier).CreateSut();

                var questionIndex = Fixture.QuestionCollection.ToList().FindIndex(_ => _.Tier == tier);

                var alternatives = sut.GenerateAlternatives(Fixture.QuestionCollection[questionIndex]);

                foreach (var alternative in alternatives)
                {
                    var question = Fixture.QuestionCollection.ToList().Find(_ => _.Answer == alternative);
                    Check.That(question.Tier).IsLessThan(tier + 1);
                }
            }

            [TestCase(81, 18, 24, 4)]
            [TestCase(81, 82, 37, 4)]
            [TestCase(81, 18, 48, 2)]
            [TestCase(4, 40, 36, 4)]
            [TestCase(4, 7, 91, 4)]
            [TestCase(77, 70, 23, 4)]
            [TestCase(70, 7, 34, 4)]
            public void ReturnsSimilarNumberAlternativeMoreOftenThanNotSimilar(int correctNumber, int similarNumber,
                int notSimilarNumber, int factor)
            {
                var sut = Fixture.WithFlipedQuestions().WithNrOfAlternatives(8).CreateSut();
                var reps = 1000;

                var question = Fixture.QuestionCollection.First(_ => _.Answer == correctNumber.ToString());

                var similarAlternative = Fixture.QuestionCollection
                    .First(_ => _.Answer == similarNumber.ToString()).Answer;
                var notSimilarAlternative = Fixture.QuestionCollection
                    .First(_ => _.Answer == notSimilarNumber.ToString()).Answer;

                var similarCount = 0;
                var notSimilarCount = 0;

                for (var rep = 0; rep < reps; rep++)
                {
                    var alternatives = sut.GenerateAlternatives(question);

                    if (alternatives.Contains(similarAlternative))
                        similarCount++;
                    if (alternatives.Contains(notSimilarAlternative))
                        notSimilarCount++;
                }

                Check.That(similarCount).IsGreaterThan(factor * notSimilarCount);
            }

            [TestCase("Test A", "Test B", "Not similar", 3, 15)]
            [TestCase("एक", "दो", "चौंतीस", 3, 15)]
            public void ReturnsSimilarStringAlternativeMoreOftenThanNotSimilarButNotToomuch(string correct,
                string similar, string notSimilar, int minFactor, int maxFactor)
            {
                var correctQuestion = new QuestionPair(correct, correct);
                var similarQuestion = new QuestionPair(similar, similar);
                var notSimilarQuestion = new QuestionPair(notSimilar, notSimilar);
                var addList = new List<IQuestionPair> { correctQuestion, similarQuestion, notSimilarQuestion };

                for (var i = 0; i < 100; i++)
                {
                    addList.Add(new QuestionPair(notSimilar + i, notSimilar + i));
                }

                var sut = Fixture.WithQuestionPairs(addList).WithNrOfAlternatives(8).CreateSut();
                var reps = 1000;

                var similarCount = 0;
                var notSimilarCount = 0;

                for (var rep = 0; rep < reps; rep++)
                {
                    var alternatives = sut.GenerateAlternatives(correctQuestion);

                    if (alternatives.Contains(similar))
                        similarCount++;
                    if (alternatives.Contains(notSimilar))
                        notSimilarCount++;
                }

                Check.That(similarCount).IsGreaterThan(minFactor * notSimilarCount);
                Check.That(similarCount).IsLessThan(maxFactor * notSimilarCount);
            }
        }

        [TestFixture]
        internal class FindCorrectAnswer : TestBase
        {
            [Test]
            public void FindsCorrectAnswer()
            {
                const int reps = 10;

                var sut = Fixture.CreateSut();

                for (var rep = 1; rep < reps; rep++)
                {
                    var questionId = Rnd.Next(0, Fixture.QuestionCollection.Count);

                    var questionPair = Fixture.QuestionCollection[questionId];
                    var alternatives = sut.GenerateAlternatives(questionPair);

                    var correctId = sut.FindCorrectAnswer(alternatives);

                    Check.That(alternatives[correctId]).IsEqualTo(questionPair.Answer);
                }
            }
        }
    }
}
