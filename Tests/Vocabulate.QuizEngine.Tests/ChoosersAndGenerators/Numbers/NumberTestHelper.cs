﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.Tests.ChoosersAndGenerators.Numbers
{
    public static class NumberTestHelper
    {
        public static IQuestionCollection CreateNumbers()
        {
            var questionPairs = new List<IQuestionPair>();

            for (var i = 1; i <= 100; i++)
            {
                questionPairs.Add(new QuestionPair(i.ToString(), "alphanumeric: " + i, CalculateTier(i)));
            }

            return new QuestionCollection("Test collection", questionPairs);
        }

        private static int CalculateTier(int i)
        {
            if (i < 11)
                return 1;
            if (i < 21)
                return 2;
            if (i % 10 == 0)
                return 2;
            return 3;
        }
    }
}
