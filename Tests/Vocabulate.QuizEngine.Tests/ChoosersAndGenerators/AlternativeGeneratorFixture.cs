﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using Vocabulate.QuizEngine.ChoosersAndGenerators;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.Tests.ChoosersAndGenerators
{
    public class AlternativeGeneratorFixture
    {
        private static readonly Random Random = new Random();
        private int _nrOfQuestions;
        private int _nrOfAlternatives;
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }

        private IQuestionCollection _quizPage;

        public AlternativeGeneratorFixture()
        {
            _quizPage = new QuestionCollection("Collection");
            QuizPairs = new List<IQuestionPair>();
            _nrOfQuestions = 10;
            _nrOfAlternatives = 6;
        }

        public AlternativeGenerator CreateSut()
        {
            if (QuizPairs.Count == 0)
                GenerateQuizPage(_nrOfQuestions);

            SetupMocks();
            var sut = new AlternativeGenerator(null, null);
            sut.QuestionCollection = _quizPage;
            sut.NrOfAlternatives = _nrOfAlternatives;
            return sut;
        }

        public List<IQuestionPair> QuizPairs;

        public AlternativeGeneratorFixture WithNrOfQuestions(int nr)
        {
            _nrOfQuestions = nr;
            GenerateQuizPage(_nrOfQuestions);

            return this;
        }

        public AlternativeGeneratorFixture WithNrOfAlternatives(int nrOfAlternatives)
        {
            _nrOfAlternatives = nrOfAlternatives;
            return this;
        }

        public AlternativeGeneratorFixture DuplicateQuestionsWithSimilarAnswers()
        {
            if (QuizPairs.Count == 0)
                GenerateQuizPage(_nrOfQuestions);

            for (var i = 1; i < _nrOfQuestions; i++)
            {
                var duplicateQuestion = new QuestionPair(QuizPairs[i].Question, QuizPairs[i - 1].Answer);
                QuizPairs.Add(duplicateQuestion);
            }
            return this;
        }

        private void SetupMocks()
        {
            _quizPage.AddRange(QuizPairs);
        }

        private void GenerateQuizPage(int nr)
        {
            QuizPairs.Clear();
            for (var i = 0; i < nr; i++)
                QuizPairs.Add(new QuestionPair(RandomString(10), RandomString(10)));
        }
    }
}
