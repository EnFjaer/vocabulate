﻿using System;
using System.Collections.Generic;
using NFluent;
using NUnit.Framework;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.Tests.ChoosersAndGenerators
{
    namespace QuestionChooserTests
    {
        internal class TestBase
        {
            protected QuestionChooserFixture Fixture;
            protected static readonly Random Rnd = new Random();


            [SetUp]
            public void RunBeforeAnyTest()
            {
                Fixture = new QuestionChooserFixture();
            }
        }

        [TestFixture]
        internal class GetQuestion : TestBase
        {
            [Test]
            public void NeverReturnsTheSameQuestionTwiceInARow()
            {
                var nrRepetitions = 100;
                var sut = Fixture.CreateSut();

                var previousQuestion = sut.GetQuestion();

                for (var rep = 0; rep < nrRepetitions; rep++)
                {
                    var nextQuestion = sut.GetQuestion();

                    Check.That(previousQuestion.Question).IsNotEqualTo(nextQuestion.Question);

                    var questionWithAlts = Fixture.GetQuestionWithIncorrectAnswer(nextQuestion, string.Empty);

                    sut.RegisterAnswer(questionWithAlts, string.Empty);

                    previousQuestion = nextQuestion;
                }
            }

            [Test]
            public void DoesNotReturnQuestionOver10BeforeAtLeastOneCorrectAnswer()
            {
                var nrRepetitions = 100;
                var sut = Fixture.CreateSut();

                for (var rep = 0; rep < nrRepetitions; rep++)
                {
                    var nextQuestion = sut.GetQuestion();
                    var questionNr = int.Parse(nextQuestion.Question);
                    var questionWithAlts = Fixture.GetQuestionWithIncorrectAnswer(nextQuestion, string.Empty);

                    sut.RegisterAnswer(questionWithAlts, string.Empty);

                    Check.That(questionNr).IsLessThan(11);
                }
            }

            [Test]
            public void ReturnsAllQuestionsFromTier1BeforeShowingAnyOtherQuestion()
            {
                var nrRepetitions = 100;
                var sut = Fixture.CreateSut();

                int[] guessed = new int[11];

                for (var rep = 0; rep < nrRepetitions; rep++)
                {
                    var nextQuestion = sut.GetQuestion();
                    var questionNr = int.Parse(nextQuestion.Question);
                    var questionWithAlts = Fixture.GetQuestionWithCorrectAnswer(nextQuestion, string.Empty);

                    sut.RegisterAnswer(questionWithAlts, string.Empty);

                    if (questionNr < 11)
                    {
                        guessed[questionNr]++;
                    }
                    else
                    {
                        for (var i = 0; i < 11; i++)
                        {
                            Check.That(guessed[i]).IsGreaterThan(0);
                        }
                    }
                }
            }

            [TestCase(1)]
            [TestCase(2)]
            [TestCase(3)]
            public void DoesNotRepeatQuestionUntilAllInTierIsAsked(int tier)
            {
                var sut = Fixture.CreateSut();

                var questionDictionary = new Dictionary<IQuestionPair, bool>();

                var tierQuestions = Fixture.GetAllQuestionPairsFromTier(tier);

                while (questionDictionary.Count < tierQuestions.Count)
                {
                    var nextQuestion = sut.GetQuestion();
                    var questionWithAlts = Fixture.GetQuestionWithCorrectAnswer(nextQuestion, nextQuestion.Answer);

                    sut.RegisterAnswer(questionWithAlts, nextQuestion.Answer);

                    if (nextQuestion.Tier == tier)
                    {
                        Check.That(questionDictionary.ContainsKey(nextQuestion)).IsFalse();
                        questionDictionary[nextQuestion] = true;
                    }
                }
            }

            [Test]
            public void QuestionsWithLowConfidenceIsChosenMoreOften()
            {
                var sut = Fixture.CreateSut();
                var questionPairs = Fixture.QuestionPairs;
                for (var i = 0; i < 5; i++)
                {
                    var question = questionPairs[i];
                    Fixture.RegisterNCorrectAnswers(question, 5);
                    var question2 = questionPairs[i + 5];
                    Fixture.RegisterNIncorrectAnswers(question2, 5);
                }

                for (var i = 0; i < 90; i++)
                {
                    var question = questionPairs[i];
                    Fixture.RegisterNCorrectAnswers(question, 1);
                }

                var nrRepetitions = 500;

                var questionsGiven = new int[10];

                for (var rep = 0; rep < nrRepetitions; rep++)
                {
                    var question = sut.GetQuestion();
                    var questionIndex = Fixture.QuestionPairs.IndexOf(question);
                    if (questionIndex < 10)
                        questionsGiven[questionIndex]++;
                }

                for (var i = 0; i < 5; i++)
                {
                    for (var j = 5; j < 10; j++)
                    {
                        Check.That(questionsGiven[i]).IsLessThan(questionsGiven[j]);
                    }
                }
            }

            [Test]
            public void HighConfidenceOverMediumConfidenceOverLowConfidence()
            {
                var sut = Fixture.CreateSut();
                var questionPairs = Fixture.QuestionPairs;

                Fixture.RegisterNCorrectAnswers(questionPairs[0], 5);
                Fixture.RegisterNCorrectAnswers(questionPairs[1], 3);
                Fixture.RegisterNIncorrectAnswers(questionPairs[1], 3);
                Fixture.RegisterNIncorrectAnswers(questionPairs[2], 5);

                for (var i = 3; i < 10; i++)
                {
                    var question = questionPairs[i];
                    Fixture.RegisterNIncorrectAnswers(question, 1);
                }

                var nrRepetitions = 1000;

                var questionsGiven = new int[10];

                for (var rep = 0; rep < nrRepetitions; rep++)
                {
                    var questionIndex = Fixture.QuestionPairs.IndexOf(sut.GetQuestion());
                    questionsGiven[questionIndex]++;
                }

                Check.That(questionsGiven[0]).IsLessThan(questionsGiven[1]);
                Check.That(questionsGiven[1]).IsLessThan(questionsGiven[2]);
            }

            [Test]
            public void AfterThreeCorrectAnswersOrMoreInARowNextQuestionWasNotAnsweredCorrectlyLastTimeAsked()
            {
                var sut = Fixture.CreateSut();
                var questionPairs = Fixture.QuestionPairs;

                for (var i = 0; i < 20; i++)
                {
                    var question = questionPairs[i];
                    Fixture.RegisterNCorrectAnswers(question, 1);
                }

                for (var i = 20; i < 100; i++)
                {
                    var question = questionPairs[i];
                    if (i % 2 == 0)
                        Fixture.RegisterNCorrectAnswers(question, 1);
                    else
                        Fixture.RegisterNIncorrectAnswers(question, 1);
                }

                var reps = 100;
                for (var rep = 0; rep < reps; rep++)
                {
                    for (var i = 0; i < 50; i++)
                    {
                        var question = questionPairs[Rnd.Next(0, 100)];
                        Fixture.RegisterNIncorrectAnswers(question, 1);
                    }

                    var nrCorrect = Rnd.Next(3, 10);
                    for (var i = 0; i < nrCorrect; i++)
                    {
                        var question = sut.GetQuestion();

                        var answer = Fixture.GetQuestionWithCorrectAnswer(question, question.Answer);

                        sut.RegisterAnswer(answer, question.Answer);
                    }

                    var nextQuestion = sut.GetQuestion();

                    Check.That(Fixture.LastAnswerWasCorrect(nextQuestion)).IsFalse();
                }
            }

            [Test]
            public void ReturnsQuestionFromTheThirdLongestAgoAskedWhenNotOnRoll()
            {
                var reps = 100;

                for (var rep = 0; rep <reps; rep++)
                {
                    var sut = Fixture.CreateSut();
                    var questionPairs = Fixture.QuestionPairs;

                    for (var i = 0; i < questionPairs.Count; i++)
                    {
                        var question = questionPairs[i];
                        Fixture.RegisterNCorrectAnswers(question, 1);
                    }

                    // Break run
                    Fixture.RegisterNIncorrectAnswers(questionPairs[questionPairs.Count - 1],1);

                    var newQuestion = sut.GetQuestion();

                    Check.That(questionPairs.GetRange(0, questionPairs.Count / 3 * 2 + 1)).Contains(newQuestion);
                }
            }
        }
    }
}