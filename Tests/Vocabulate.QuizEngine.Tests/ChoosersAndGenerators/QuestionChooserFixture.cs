﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vocabulate.QuizEngine.ChoosersAndGenerators;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.QuestionHistory;
using Vocabulate.QuizEngine.Tests.ChoosersAndGenerators.Numbers;

namespace Vocabulate.QuizEngine.Tests.ChoosersAndGenerators
{
    public class QuestionChooserFixture
    {
        private readonly Random _random = new Random();

        public List<IQuestionPair> QuestionPairs { get; private set; }

        private QuestionChooser _thisSut;

        private IAnswerHistory _answerHistory;

        public QuestionChooser CreateSut()
        {
            var numbers = NumberTestHelper.CreateNumbers();
            QuestionPairs = numbers.ToList();
            _answerHistory = new AnswerHistory();
            _thisSut = new QuestionChooser(numbers, _answerHistory);
            return _thisSut;
        }

        public IQuizQuestion GetQuestionWithCorrectAnswer(IQuestionPair questionPair, string answer)
        {
            var nrOfAlternatives = 6;
            var correctAlternative = _random.Next(0, nrOfAlternatives);
            var alternatives = GetAlternatives(nrOfAlternatives);
            alternatives[correctAlternative] = answer;
            return new QuizQuestion(questionPair, alternatives, correctAlternative);
        }

        public IQuizQuestion GetQuestionWithIncorrectAnswer(IQuestionPair questionPair, string answer)
        {
            var nrOfAlternatives = 6;
            var correctAlternative = _random.Next(0, nrOfAlternatives - 1);
            var alternatives = GetAlternatives(nrOfAlternatives);
            alternatives[5] = answer;
            return new QuizQuestion(questionPair, alternatives, correctAlternative);
        }

        public IList<IQuestionPair> GetAllQuestionPairsFromTier(int tier)
        {
            return QuestionPairs.FindAll(_ => _.Tier == tier);
        }

        public void RegisterNCorrectAnswers(IQuestionPair question, int nrCorrect)
        {
            for (var i = 0; i < nrCorrect; i++)
            {
                var correctQuestion = GetQuestionWithCorrectAnswer(question, question.Answer);
                _thisSut.RegisterAnswer(correctQuestion, question.Answer);
            }
        }

        public void RegisterNIncorrectAnswers(IQuestionPair question, int nrIncorrect)
        {
            for (var i = 0; i < nrIncorrect; i++)
            {
                var incorrectQuestion = GetQuestionWithCorrectAnswer(question, question.Answer);
                _thisSut.RegisterAnswer(incorrectQuestion, incorrectQuestion.Alternatives[0]);
            }
        }

        public bool LastAnswerWasCorrect(IQuestionPair question)
        {
            return _answerHistory.OutcomeOfLastAnswer(question);
        }

        private List<string> GetAlternatives(int nrOfAlternatives)
        {
            var alternatives = new List<string>();
            for (var i = 0; i < nrOfAlternatives; i++)
            {
                alternatives.Add(QuestionPairs[i].Answer);
            }
            return alternatives;
        }
    }
}
