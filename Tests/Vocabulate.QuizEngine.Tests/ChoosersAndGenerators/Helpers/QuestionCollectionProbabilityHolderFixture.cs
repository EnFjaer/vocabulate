﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Vocabulate.QuizEngine.ChoosersAndGenerators;
using Vocabulate.QuizEngine.ChoosersAndGenerators.Helpers;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.Tests.ChoosersAndGenerators.Helpers
{
    public class QuestionCollectionProbabilityHolderFixture
    {
        public QuestionCollectionProbabilityHolderFixture()
        {
            ProbabilityCalculator = new Mock<IProbabilityCalculator>();
            ProbabilityDictionary = new Dictionary<IQuestionPair, double>();
            QuestionCollection = new QuestionCollection("Test collection");
        }

        public IQuestionCollection QuestionCollection;

        public Dictionary<IQuestionPair, double> ProbabilityDictionary { get; private set; }

        public Mock<IProbabilityCalculator> ProbabilityCalculator { get; private set; }

        public QuestionCollectionProbabilityHolder CreateSut()
        {
            return new QuestionCollectionProbabilityHolder(QuestionCollection);
        }

        public QuestionCollectionProbabilityHolderFixture WithNrOfQuestions(int nr)
        {
            QuestionCollection.Clear();
            for (var i = 0; i < nr; i++)
            {
                QuestionCollection.Add(new QuestionPair("Question " + i, "Answer " + i, tier: i));
            }

            return this;
        }

        public IProbabilityCalculator GetCalculator(List<double> factors)
        {
            for (var i = 0; i < QuestionCollection.Count; i++)
            {
                if (i < factors.Count)
                {
                    ProbabilityDictionary[QuestionCollection[i]] = factors[i];
                }
                else
                {
                    ProbabilityDictionary[QuestionCollection[i]] = 1;
                }
            }
            ProbabilityCalculator.Setup(_ => _.CalculateProbability(It.IsAny<IQuestionCollection>()))
                .Returns(ProbabilityDictionary);

            return ProbabilityCalculator.Object;
        }

        public List<double> NormalizeProbabilities(List<double> factors)
        {
            var sum = factors.Sum();
            return factors.Select(_ => _ / sum).ToList();
        }
    }
}
