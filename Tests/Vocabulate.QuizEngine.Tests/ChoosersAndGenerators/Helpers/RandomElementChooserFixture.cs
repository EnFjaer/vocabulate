﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.Tests.ChoosersAndGenerators.Helpers
{
    public class RandomElementChooserFixture
    {
        public RandomElementChooserFixture()
        {
            QuestionPairs = new List<IQuestionPair>();
            QuestionDictionary = new Dictionary<IQuestionPair, double>();
            for (var i = 0; i < 10; i++)
            {
                QuestionPairs.Add(new QuestionPair("Question "+i, "Answer "+i));
                QuestionDictionary.Add(QuestionPairs[i], 0);
            }

        }

        public List<IQuestionPair> QuestionPairs;
        public Dictionary<IQuestionPair, double> QuestionDictionary;
    }
}
