﻿using System;
using System.Collections.Generic;
using System.Linq;
using NFluent;
using NUnit.Framework;
using Vocabulate.QuizEngine.ChoosersAndGenerators.Helpers;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.Tests.ChoosersAndGenerators.Helpers
{
    namespace RandomElementChooserTests
    {
        internal class TestBase
        {
            protected RandomElementChooserFixture Fixture;

            [SetUp]
            public void RunBeforeAnyTest()
            {
                Fixture = new RandomElementChooserFixture();
            }
        }

        [TestFixture]
        internal class IndexFromProbabilityDensity : TestBase
        {
            [Test]
            public void ReturnsMinus1IfListIsEmpty()
            {
                var testList = new List<double>();

                Check.That(RandomElementChooser.IndexFromProbabilityDensity(testList)).IsEqualTo(-1);
            }

            [Test]
            public void Returns0IfListIsLength1()
            {
                var testList = new List<double> {1};

                Check.That(RandomElementChooser.IndexFromProbabilityDensity(testList)).IsEqualTo(0);
            }

            [TestCase(1)]
            [TestCase(3)]
            [TestCase(5)]
            public void ReturnsExpectedIndexWhenAllProbabilityLaysAtOnePlace(int index)
            {
                var testList = new List<double>(new double[10]) {[index] = 5};

                Check.That(RandomElementChooser.IndexFromProbabilityDensity(testList)).IsEqualTo(index);
            }

            [Test]
            public void ReturnsADistributionOfAnswersTheFollowsTheWantedProbability()
            {
                var testList = new List<double> {0, 0.1, 0.2, 0.3, 0.4};
                var resultList = new List<double>(new double[testList.Count]);

                var reps = 1000;
                for (var i = 0; i < reps; i++)
                {
                    var index = RandomElementChooser.IndexFromProbabilityDensity(testList);
                    resultList[index]++;
                }

                for (var i = 0; i < testList.Count; i++)
                {
                    var std = Math.Sqrt(reps * testList[i] * (1 - testList[i]));
                    var minVal = reps * testList[i] - 3 * std - 1;
                    var maxVal = reps * testList[i] + 3 * std + 1;
                    Check.That(resultList[i]).IsGreaterThan(minVal);
                    Check.That(resultList[i]).IsLessThan(maxVal);
                }
            }
        }

        [TestFixture]
        internal class ElementFromProbabilityDensity : TestBase
        {
            [Test]
            public void ReturnsDefaultValueIfDictionaryIsEmpty()
            {
                var testDictionary = new Dictionary<IQuestionPair, double>();

                Check.That(RandomElementChooser.ElementFromProbabilityDensity(testDictionary)).Equals(default(IQuestionPair));
            }

            [Test]
            public void ReturnsTheOnlyElementIfTheDictionaryHasOnlyOneElement()
            {
                var testDictionary = new Dictionary<IQuestionPair, double>();

                var question = Fixture.QuestionPairs[0];
                testDictionary.Add(question,1);


                Check.That(RandomElementChooser.ElementFromProbabilityDensity(testDictionary)).Equals(question);
            }

            [TestCase(1)]
            [TestCase(3)]
            [TestCase(5)]
            public void ReturnsExpectedIndexWhenAllProbabilityLaysAtOnePlace(int index)
            {
                var testQuestion = Fixture.QuestionPairs[index];
                var testDictionary = Fixture.QuestionDictionary;
                testDictionary[testQuestion] = 1;

                Check.That(RandomElementChooser.ElementFromProbabilityDensity(testDictionary)).IsEqualTo(testQuestion);
            }
        }
    }
}
