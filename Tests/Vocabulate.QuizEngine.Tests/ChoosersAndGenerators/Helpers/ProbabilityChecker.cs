﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NFluent;
using Vocabulate.QuizEngine.ChoosersAndGenerators.Helpers;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.Tests.ChoosersAndGenerators.Helpers
{
    public static class ProbabilityChecker
    {
        public static void CheckProbabilities(QuestionCollectionProbabilityHolder sut, IQuestionCollection questionCollection, List<double> wantedProbabilities, int cutOff = 10)
        {
            var N = 1000;
            var testList = new List<double>(new double[4]);


            for (var rep = 0; rep < N; rep++)
            {
                var thisQuestion = sut.GetRandomQuestion(cutOff);

                var index = questionCollection.IndexOf(thisQuestion);
                testList[index]++;
            }

            for (var i = 0; i < testList.Count; i++)
            {
                var p = wantedProbabilities[i];
                var std = Math.Sqrt(N * p * (1 - p));
                var minVal = N * p - 2 * std - 1;
                var maxVal = N * p + 2 * std + 1;

                Check.That(testList[i]).IsLessThan(maxVal);
                Check.That(testList[i]).IsGreaterThan(minVal);
            }
        }
    }
}
