﻿using System;
using System.Collections.Generic;
using System.Linq;
using NFluent;
using NUnit.Framework;
using Vocabulate.QuizEngine.ChoosersAndGenerators.Helpers;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.Tests.ChoosersAndGenerators.Helpers
{
    namespace QuestionCollectionProbabilityHolderTests
    {
        internal class TestBase
        {
            protected QuestionCollectionProbabilityHolderFixture Fixture;

            [SetUp]
            public void RunBeforeAnyTest()
            {
                Fixture = new QuestionCollectionProbabilityHolderFixture();
            }


        }

        [TestFixture]
        internal class GetRandomQuestion : TestBase
        {
            [Test]
            public void ReturnsDefaultValueIfQuestionCollectionIsEmpty()
            {
                var sut = Fixture.CreateSut();

                Check.That(sut.GetRandomQuestion()).IsEqualTo(default(IQuestionPair));
            }

            [Test]
            public void ReturnsTheOnlyValueIfOnlyOneElementInQuestionCollection()
            {
                var sut = Fixture.WithNrOfQuestions(1).CreateSut();

                Check.That(sut.GetRandomQuestion()).IsEqualTo(Fixture.QuestionCollection[0]);
            }

            [Test]
            public void ReturnsQuestionsWithTheSameProbabilityIfNoProbabilityIsSet()
            {
                var sut = Fixture.WithNrOfQuestions(4).CreateSut();

                ProbabilityChecker.CheckProbabilities(sut, Fixture.QuestionCollection, new List<double>{0.25, 0.25, 0.25, 0.25});
            }
        }

        [TestFixture]
        internal class ApplyProbabilityFactors : TestBase
        {
            [Test]
            public void AppliesCorrectProbabilitiesWhenAddingOnlyOne()
            {
                var sut = Fixture.WithNrOfQuestions(4).CreateSut();
                var probabilities = new List<double> {1, 2, 3, 4};
                var probabilityCalculator = Fixture.GetCalculator(probabilities);
                sut.ApplyProbabilityFactors(probabilityCalculator);

                ProbabilityChecker.CheckProbabilities(sut, Fixture.QuestionCollection, Fixture.NormalizeProbabilities(probabilities));
            }

            [Test]
            public void AppliesCorrectProbabilitiesWhenAddinTwo()
            {
                var sut = Fixture.WithNrOfQuestions(4).CreateSut();
                var probabilities = new List<double> { 1, 2, 3, 4 };
                var squaredProbabilities = new List<double> {1, 4, 9, 16};
                var probabilityCalculator = Fixture.GetCalculator(probabilities);
                sut.ApplyProbabilityFactors(probabilityCalculator);
                sut.ApplyProbabilityFactors(probabilityCalculator);

                ProbabilityChecker.CheckProbabilities(sut, Fixture.QuestionCollection, Fixture.NormalizeProbabilities(squaredProbabilities));
            }

            [Test]
            public void DoesNotReturnLeastProbibleIfOutsideCutoff()
            {
                var sut = Fixture.WithNrOfQuestions(4).CreateSut();
                var probabilities = new List<double> { 1, 2, 3, 4 };
                var expectedProbabilities = new List<double> { 0, 2, 3, 4 };

                var probabilityCalculator = Fixture.GetCalculator(probabilities);
                sut.ApplyProbabilityFactors(probabilityCalculator);

                ProbabilityChecker.CheckProbabilities(sut, Fixture.QuestionCollection, Fixture.NormalizeProbabilities(expectedProbabilities), 3);

            }

            [Test]
            public void ReturnsLeastProbibleIfOutsideCutoffIfSameValueAsCutOffElement()
            {
                var sut = Fixture.WithNrOfQuestions(4).CreateSut();
                var probabilities = new List<double> { 2, 2, 3, 4 };
                var expectedProbabilities = new List<double> { 2, 2, 3, 4 };

                var probabilityCalculator = Fixture.GetCalculator(probabilities);
                sut.ApplyProbabilityFactors(probabilityCalculator);

                ProbabilityChecker.CheckProbabilities(sut, Fixture.QuestionCollection, Fixture.NormalizeProbabilities(expectedProbabilities), 3);
            }
        }
    }
}
