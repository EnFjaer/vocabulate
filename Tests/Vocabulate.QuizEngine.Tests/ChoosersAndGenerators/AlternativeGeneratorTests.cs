﻿using System.Linq;
using NFluent;
using NUnit.Framework;

namespace Vocabulate.QuizEngine.Tests.ChoosersAndGenerators
{
    namespace AlternativeGeneratorTests
    {
        internal class TestBase
        {
            protected AlternativeGeneratorFixture Fixture;

            [SetUp]
            public void RunBeforeAnyTest()
            {
                Fixture = new AlternativeGeneratorFixture();
            }
        }

        [TestFixture]
        internal class GenerateAlternatives : TestBase
        {
            [TestCase(2)]
            [TestCase(5)]
            [TestCase(8)]
            [TestCase(15)]
            public void ReturnsCorrectNumberOfAlternatives(int nrOfAlternatives)
            {
                var sut = Fixture.WithNrOfQuestions(50).WithNrOfAlternatives(nrOfAlternatives).CreateSut();

                Check.That(sut.GenerateAlternatives(sut.QuestionCollection[0]).Count).Equals(nrOfAlternatives);
            }

            [Test]
            public void AlwaysReturnsCorrectAnswer()
            {
                const int testDepth = 10;

                var sut = Fixture.WithNrOfQuestions(testDepth).CreateSut();

                for (var i = 0; i < testDepth; i++)
                {

                    var alternatives = sut.GenerateAlternatives(sut.QuestionCollection[i]);

                    var correctAnswer = Fixture.QuizPairs[i].Answer;

                    Check.That(alternatives).Contains(correctAnswer);
                }
            }

            [Test]
            public void ReturnsUniqueAlternatives()
            {
                const int testDepth = 100;
                const int nrOfAlternatives = 6;
                var sut = Fixture.WithNrOfAlternatives(nrOfAlternatives).WithNrOfQuestions(10)
                    .DuplicateQuestionsWithSimilarAnswers().CreateSut();
                for (var i = 0; i < testDepth; i++)
                {
                    var alternatives = sut.GenerateAlternatives(sut.QuestionCollection[i % 10]);
                    Check.That(alternatives.Distinct().Count()).IsEqualTo(alternatives.Count);
                }
            }

            [Test]
            public void ReturnsOnlyAlternativesFromQuizList()
            {
                const int testDepth = 10;
                const int nrOfAlternatives = 6;
                var sut = Fixture.WithNrOfAlternatives(nrOfAlternatives).WithNrOfQuestions(testDepth).CreateSut();
                for (var i = 0; i < testDepth; i++)
                {
                    var alternatives = sut.GenerateAlternatives(sut.QuestionCollection[i]);
                    foreach (var alternative in alternatives)
                    {
                        Check.That(Fixture.QuizPairs.Select(_ => _.Answer)).Contains(alternative);
                    }
                }
            }
        }

        [TestFixture]
        internal class FindCorrectAnswer : TestBase
        {
            [Test]
            public void ReturnsCorrectAnswerIndex()
            {
                var sut = Fixture.CreateSut();

                var correctAnswer = Fixture.QuizPairs[3].Answer;
                var alternatives = sut.GenerateAlternatives(Fixture.QuizPairs[3]);
                var correctIndex = sut.FindCorrectAnswer(alternatives);

                Check.That(alternatives[correctIndex]).Equals(correctAnswer);
            }

            [Test]
            public void ReturnsCorrectAnswerOnDifferentIndecies()
            {
                const int reps = 100;
                const int nrOfAlternatives = 6;
                var sut = Fixture.WithNrOfAlternatives(nrOfAlternatives).WithNrOfQuestions(reps).CreateSut();

                int[] correctAlternativeCounter = new int[nrOfAlternatives];

                for (var rep = 0; rep < reps; rep++)
                {
                    var alternatives = sut.GenerateAlternatives(sut.QuestionCollection[rep]);
                    var correctIndex = sut.FindCorrectAnswer(alternatives);
                    correctAlternativeCounter[correctIndex]++;
                }

                for (var i = 0; i < nrOfAlternatives; i++)
                {
                    Check.That(correctAlternativeCounter[i]).IsGreaterThan(0);
                }
            }

            [Test]
            public void DoesNotReturnMoreThanOneCorrectAlternative()
            {
                const int testDepth = 100;
                const int nrOfAlternatives = 6;
                var sut = Fixture.WithNrOfAlternatives(nrOfAlternatives).DuplicateQuestionsWithSimilarAnswers()
                    .CreateSut();
                for (var depth = 0; depth < testDepth; depth++)
                {
                    var questionPair = sut.QuestionCollection[depth % 10];
                    var alternatives = sut.GenerateAlternatives(questionPair);
                    for (var i = 0; i < nrOfAlternatives; i++)
                    {
                        if (i == sut.FindCorrectAnswer(alternatives))
                            continue;
                        var alternative = alternatives[i];
                        Check.That(Fixture.QuizPairs
                            .Select(_ => _).Count(_ => _.Answer == alternative && _.Question == questionPair.Question)).IsEqualTo(0);
                    }
                }
            }
        }
    }
}