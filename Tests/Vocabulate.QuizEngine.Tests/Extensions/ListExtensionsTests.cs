﻿using System.Collections.Generic;
using NFluent;
using NUnit.Framework;
using Vocabulate.QuizEngine.Extensions;

namespace Vocabulate.QuizEngine.Tests.Extensions
{
    namespace ListExtensions
    {
        [TestFixture]
        public class Serialize
        {
            [Test]
            public void ReturnsDeserializedEmptyList()
            {
                var testList = new List<string>();
                var serialized = testList.Serialize();
                var deserialized = serialized.DeserializeList<string>();

                Check.That(deserialized.Count).IsEqualTo(0);
            }

            [Test]
            public void ReturnsListWithTheSameElements()
            {
                var testList = new List<string>();
                testList.Add("String 1");
                testList.Add("String 2");
                var serialized = testList.Serialize();
                var deserialized = serialized.DeserializeList<string>();

                Check.That(deserialized.Count).IsEqualTo(2);
                Check.That(deserialized[0]).IsEqualTo(testList[0]);
                Check.That(deserialized[1]).IsEqualTo(testList[1]);
            }
        }
    }
}
