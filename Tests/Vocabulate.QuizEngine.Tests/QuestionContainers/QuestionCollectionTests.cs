﻿using NFluent;
using NUnit.Framework;

namespace Vocabulate.QuizEngine.Tests.QuestionContainers
{
    namespace QuestionCollectionTests
    {
        internal class TestBase
        {
            protected QuestionCollectionFixture Fixture;

            [SetUp]
            public void RunBeforeAnyTest()
            {
                Fixture = new QuestionCollectionFixture();
            }
        }

        [TestFixture]
        internal class CollectionName : TestBase
        {
            [Test]
            public void ReturnsNameSetInConstructor()
            {
                var sut = Fixture.WithCollectionName("Test name").CreateSut();

                Check.That(sut.CollectionName).IsEqualTo("Test name");
            }
        }

        [TestFixture]
        internal class QuestionPairs : TestBase
        {
            [Test]
            public void ReturnsEmptyListIfNoPairsAreSet()
            {
                var sut = Fixture.CreateSut();

                Check.That(sut).IsEmpty();
            }

            [TestCase(1)]
            [TestCase(10)]
            [TestCase(100)]
            [TestCase(1000)]
            public void ReturnsListWithCorrectNumberOfElements(int nrElements)
            {
                var sut = Fixture.WithQuestionPairs(nrElements).CreateSut();

                Check.That(sut.Count).IsEqualTo(nrElements);
            }
        }

        [TestFixture]
        internal class FlipQuestionAndAnswer : TestBase
        {
            [Test]
            public void ReturnsOriginalListWhenFalse()
            {
                var sut = Fixture.WithQuestionPairs().CreateSut();

                sut.FlipQuestionAndAnswer = false;
                for(var i = 0; i < sut.Count; i++)
                    Check.That(sut[i]).IsEqualTo(Fixture.QuestionPairs[i]);
            }

            [Test]
            public void ReturnsFlippedListWhenTrue()
            {
                var sut = Fixture.WithQuestionPairs().CreateSut();

                sut.FlipQuestionAndAnswer = true;

                for (var i = 0; i < sut.Count; i++)
                {
                    Check.That(sut[i].IsFlipped).IsTrue();
                }
            }
        }
    }
}
