﻿using NFluent;
using NUnit.Framework;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.Tests.QuestionContainers
{
    namespace QuestionPairTests
    {
        [TestFixture]
        internal class Flip
        {
            [Test]
            public void IsFalseInitially()
            {
                var sut = new QuestionPair("Question", "Answer");

                Check.That(sut.IsFlipped).IsFalse();
            }

            [Test]
            public void WhenFalseQuestionAndAnswerIsNotFlipped()
            {
                var sut = new QuestionPair("Question", "Answer");

                Check.That(sut.Question).IsEqualTo("Question");
                Check.That(sut.Answer).IsEqualTo("Answer");
            }

            [Test]
            public void WhenTrueQuestionAndAnswerIsFlipped()
            {
                var sut = new QuestionPair("Question", "Answer");
                sut.IsFlipped = true;

                Check.That(sut.Question).IsEqualTo("Answer");
                Check.That(sut.Answer).IsEqualTo("Question");
            }
        }
    }
}
