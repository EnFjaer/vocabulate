﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vocabulate.QuizEngine.Containers;

namespace Vocabulate.QuizEngine.Tests.QuestionContainers
{
    public class QuestionCollectionFixture
    {
        private static readonly Random Random = new Random();

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }

        private string _name = string.Empty;

        public List<IQuestionPair> QuestionPairs = new List<IQuestionPair>();

        public QuestionCollection CreateSut()
        {
            return new QuestionCollection(_name, QuestionPairs);
        }

        public QuestionCollectionFixture WithCollectionName(string name)
        {
            _name = name;
            return this;
        }

        public QuestionCollectionFixture WithQuestionPairs(int length = 10)
        {
            GenerateQuestionPairs(length);
            return this;
        }

        private void GenerateQuestionPairs(int length)
        {
            QuestionPairs.Clear();
            for (var i = 0; i < length; i++)
                QuestionPairs.Add(new QuestionPair(RandomString(10), RandomString(10)));
        }

    }
}
