﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Moq;
using Vocabulate.HindiNumbers.Core.NumberImporter;

namespace Vocabulate.HindiNumbers.Core.Tests
{
    public class KeeperOfNumbersFixture
    {
        private static readonly Random Rnd = new Random();
        private static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[Rnd.Next(s.Length)]).ToArray());
        }

        public KeeperOfNumbersFixture()
        {
            KeeperOfNumbers.Uninitialize();
        }

        public List<HindiNumber> GetListOfNumbers(int nrOfNumbers)
        {
            var numbers = new List<HindiNumber>();
            for(var i = 0; i < nrOfNumbers; i++)
                numbers.Add(CreateRandomHindiNumber());

            return numbers;
        }

        private HindiNumber CreateRandomHindiNumber()
        {
            return new HindiNumber(RandomString(5),RandomString(5),RandomString(5),"1");
        }
    }
}
