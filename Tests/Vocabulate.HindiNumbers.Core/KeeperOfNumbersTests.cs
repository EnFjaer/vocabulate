﻿using Castle.Components.DictionaryAdapter;
using NFluent;
using NUnit.Framework;
using Vocabulate.HindiNumbers.Core.NumberImporter;

namespace Vocabulate.HindiNumbers.Core.Tests
{
    namespace KeeperOfNumbersTests
    {
        public class TestBase
        {
            protected KeeperOfNumbersFixture Fixture;

            [SetUp]
            public void RunBeforeAnyTest()
            {
                Fixture = new KeeperOfNumbersFixture();
            }
        }

        public class Initialize : TestBase
        {
            [Test]
            public void IsInitializedIsFalseBeforeInitialization()
            {
                Check.That(KeeperOfNumbers.IsInitialized).IsFalse();
            }

            [Test]
            public void IsInitializedIsFalseIfNumberListIsEmpty()
            {
                KeeperOfNumbers.Initialize(new EditableList<HindiNumber>());
                Check.That(KeeperOfNumbers.IsInitialized).IsFalse();
            }

            [Test]
            public void IsInitializedIsFalseIfNumberListIsNull()
            {
                KeeperOfNumbers.Initialize(null);
                Check.That(KeeperOfNumbers.IsInitialized).IsFalse();
            }

            [Test]
            public void IsInitializedIsTrueIfNumberListIsPopulated()
            {
                KeeperOfNumbers.Initialize(Fixture.GetListOfNumbers(5));
                Check.That(KeeperOfNumbers.IsInitialized).IsTrue();
            }

            [TestCase(1)]
            [TestCase(5)]
            [TestCase(100)]
            public void NumberOfQuestionsIsTheSameSizeAsListOfNumbers(int length)
            {
                KeeperOfNumbers.Initialize(Fixture.GetListOfNumbers(length));
                Check.That(KeeperOfNumbers.NumberQuestions.Count).IsEqualTo(length);
            }

            [Test]
            public void NumberOfQuestionContainsTheSameAsListOfNumbers()
            {
                var listOfNumbers = Fixture.GetListOfNumbers(5);
                KeeperOfNumbers.Initialize(listOfNumbers);
                var questions = KeeperOfNumbers.NumberQuestions;

                for (var i = 0; i < listOfNumbers.Count; i++)
                {
                    Check.That(questions[i].Question).IsEqualTo(listOfNumbers[i].Alphanumeric);
                    Check.That(questions[i].Answer).IsEqualTo(listOfNumbers[i].Hindi);
                }
            }
        }
    }
}
