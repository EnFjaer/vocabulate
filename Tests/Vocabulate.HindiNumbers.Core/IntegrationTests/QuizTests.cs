﻿using System.Threading.Tasks;
using NFluent;
using NUnit.Framework;

namespace Vocabulate.HindiNumbers.Core.Tests.IntegrationTests
{
    namespace QuizTests
    {
        public class TestBase
        {
            protected QuizFixture Fixture;

            [SetUp]
            public void RunBeforeAnyTest()
            {
                Fixture = new QuizFixture();
            }
        }

        public class Guess : TestBase
        {
            [Test]
            public async Task AfterHundredCorrectGuessesEveryNumberHasOneCorrect()
            {
                var sut = Fixture.CreateSut();

                for (var i = 0; i < 100; i++)
                {
                    var question = sut.Question;
                    var alternatives = sut.Alternatives;
                    var correctAnswer = Fixture.GetCorrectAnswer(question, alternatives);

                    await sut.Guess(correctAnswer);
                }

                foreach (var question in KeeperOfNumbers.NumberQuestions)
                {
                    Check.That(Fixture.AnswerHistory.NumberOfCorrectAnswers(question)).IsEqualTo(1);
                }
            }

            [Test]
            public async Task AfterHundredIncorrectGuessesOnlyTheFirstTenNumbersHasAnswers()
            {
                var sut = Fixture.CreateSut();

                for (var i = 0; i < 100; i++)
                {
                    var question = sut.Question;
                    var alternatives = sut.Alternatives;
                    var incorrectAnswer = Fixture.GetIncorrectAnswer(question, alternatives);

                    await sut.Guess(incorrectAnswer);
                }

                for(var i = 0; i < KeeperOfNumbers.NumberQuestions.Count; i++)
                {
                    var question = KeeperOfNumbers.NumberQuestions[i];
                    if (i < 10)
                    {
                        Check.That(Fixture.AnswerHistory.NumberOfCorrectAnswers(question)).IsEqualTo(0);
                        Check.That(Fixture.AnswerHistory.NumberOfTimesAsked(question)).IsGreaterThan(0);
                    }
                    else
                    {
                        Check.That(Fixture.AnswerHistory.NumberOfTimesAsked(question)).IsEqualTo(0);
                    }
                }
            }

        }

    }
}
