﻿using System.Collections.Generic;
using System.Linq;
using Vocabulate.CommonViewModels.ViewModels;
using Vocabulate.HindiNumbers.Core.NumberImporter;
using Vocabulate.HindiNumbers.Core.ViewModels;
using Vocabulate.QuizEngine.ChoosersAndGenerators;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.QuestionHistory;

namespace Vocabulate.HindiNumbers.Core.Tests.IntegrationTests
{
    public class QuizFixture
    {
        public IAnswerHistory AnswerHistory;

        public NumberQuizViewModel CreateSut()
        {
            SetupKeeperOfNumbers();

            var questionCollection = CreateQuestionCollection();
            AnswerHistory = new AnswerHistory();
            var questionChooser = new QuestionChooser(questionCollection, AnswerHistory);
            var alternativeGenerator = new AlternativeGenerator(questionChooser, questionCollection);

            var sut = new NumberQuizViewModel();
            sut.Setup(questionCollection, alternativeGenerator, questionChooser, questionChooser, waitTime: 0);

            return sut;
        }

        public IAlternativeViewModel GetCorrectAnswer(string question, List<IAlternativeViewModel> alternatives)
        {
            var allQuestions = KeeperOfNumbers.NumberQuestions;
            var correctAnswer = allQuestions.FirstOrDefault(_ => _.Question == question).Answer;
            return alternatives.FirstOrDefault(_ => _.Name == correctAnswer);
        }

        public IAlternativeViewModel GetIncorrectAnswer(string question, List<IAlternativeViewModel> alternatives)
        {
            var allQuestions = KeeperOfNumbers.NumberQuestions;
            var correctAnswer = allQuestions.FirstOrDefault(_ => _.Question != question && alternatives.Any(a => a.Name == _.Answer)).Answer;
            return alternatives.FirstOrDefault(_ => _.Name == correctAnswer);
        }
        private void SetupKeeperOfNumbers()
        {
            KeeperOfNumbers.Initialize(CreateListOfNumbers());
        }

        private List<HindiNumber> CreateListOfNumbers()
        {
            var numbers = new List<HindiNumber>();

            for (var i = 1; i <= 100; i++)
            {
                numbers.Add(new HindiNumber(i.ToString(), i + " hindi", "Transliteration", CalculateTier(i).ToString()));
            }

            return numbers;
        }

        private int CalculateTier(int nr)
        {
            if (nr <= 10)
                return 1;
            if (nr < 20)
                return 2;
            if (nr % 10 == 0)
                return 2;
            return 3;
        }

        private IQuestionCollection CreateQuestionCollection()
        {
            return new QuestionCollection("TestCollection", KeeperOfNumbers.NumberQuestions.ToList());
        }
    }
}
