﻿using System.Collections.Generic;
using System.IO;
using Moq;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.TeachYourselfHindi.Core.ViewModels;
using Vocabulate.TeachYourselfHindi.Core.Vocab;
using Vocabulate.TeachYourselfHindi.Core.VocabImporters;

namespace Vocabulate.TeachYourselfHindi.Core.Tests.ViewModels
{
    internal class QuizViewModelFixture
    {
        private string _unitPath;
        private string _unitName;
        private int _nrOfAlternatives;

        private Mock<IUnitLoader> _unitLoaderMock;

        private IQuestionCollection _unit;

        public List<IQuestionPair> _quizWords;


        public QuizViewModelFixture()
        {
            _unitPath = @"C:\Workspace\Vocabulate\Tests\Vocabulate.tests\TestData\unit1.csv";
            _unitName = "Unit 1";

            _nrOfAlternatives = 3;

            _unitLoaderMock = new Mock<IUnitLoader>();
        }


        public QuizViewModel CreateSut()
        {
            LoadQuizData();

            _unitLoaderMock.Setup((_) => _.Load(It.IsAny<string>(), It.IsAny<int>())).Returns(_quizWords);

            _unit = new Unit(_unitName, _unitPath, _unitLoaderMock.Object);

            var sut = new QuizViewModel();
            sut.Setup(_unit, _nrOfAlternatives, 100);
            return sut;
        }

        public QuizViewModelFixture WithUnitNr(int unitNr)
        {
            switch (unitNr)
            {
                case 1:
                    _unitPath = @"C:\Workspace\Vocabulate\Tests\Vocabulate.tests\TestData\unit1.csv";
                    _unitName = "Unit 1";
                    break;

                case 2:
                    _unitPath = @"C:\Workspace\Vocabulate\Tests\Vocabulate.tests\TestData\unit2.csv";
                    _unitName = "Unit 2";
                    break;

                default:
                    _unitPath = string.Empty;
                    _unitName = "Empty unit";
                    break;
            }
            return this;
        }

        public QuizViewModelFixture WithNumberOfAlternatives(int nrOfAlternatives)
        {
            _nrOfAlternatives = nrOfAlternatives;

            return this;
        }

        public bool IsValidQuestion(string question)
        {
            return _quizWords.Exists(_ => _.Question.Equals(question));
        }

        private void LoadQuizData()
        {
            using (var csvFile = File.Open(_unitPath, FileMode.Open, FileAccess.Read))
            {
                _quizWords = new List<IQuestionPair>();
                var vocabList = ImportFromCsv.GetVocabular(csvFile);
                foreach (var vocab in vocabList)
                    _quizWords.Add(new QuestionPair(vocab.Word, vocab.PrimaryTranslation));
            }
        }
    }
}
