﻿using System.Linq;
using NFluent;
using NUnit.Framework;
using Vocabulate.StyleHelper;

namespace Vocabulate.TeachYourselfHindi.Core.Tests.ViewModels
{
    namespace QuizViewModelTests
    { 
        internal class BaseClass
        {
            protected QuizViewModelFixture Fixture;

            [SetUp]
            public void RunBeforeAnyTest()
            {
                Fixture = new QuizViewModelFixture();
            }
        }

        [TestFixture]
        internal class NrOfQuestions : BaseClass
        {

            [Test]
            public void ThrowsExceptionWhenFileNotFound()
            {
                Assert.Throws(typeof(System.ArgumentException),() => Fixture.WithUnitNr(0).CreateSut());
            }
        }

        [TestFixture]
        internal class Question : BaseClass
        {
            [Test]
            public void ReturnsAQuestionPresentInQuizFile()
            {
                var sut = Fixture.CreateSut();

                Check.That(Fixture.IsValidQuestion(sut.Question)).IsTrue();
            }

            [Test]
            public void ReturnsNewQuestionAfterUpdate()
            {
                var sut = Fixture.CreateSut();

                var oldQuestion = sut.Question;

                sut.Update();

                var newQuestion = sut.Question;

                Check.That(Fixture.IsValidQuestion(newQuestion)).IsTrue();
                Check.That(newQuestion).IsNotEqualTo(oldQuestion);
            }
        }

        [TestFixture]
        internal class Alternatives : BaseClass
        {
            [TestCase(2)]
            [TestCase(4)]
            [TestCase(8)]
            [TestCase(12)]
            public void ReturnsCorrectNumberOfAlternatives(int nrAlt)
            {
                var sut = Fixture.WithUnitNr(2).WithNumberOfAlternatives(nrAlt).CreateSut();

                Check.That(sut.Alternatives.Count).IsEqualTo(nrAlt);
            }
        }

        [TestFixture]
        internal class GuessCommand : BaseClass
        {
            [Test]
            public void ReturnsACommandThatCanBeExecuted()
            {
                var sut = Fixture.CreateSut();

                var guessCommand = sut.GuessCommand;
                Check.That(guessCommand.CanExecute(sut.Alternatives[0])).IsTrue();
            }

            [Test]
            public void ReturnsACommandThatExecutesGuessWhichChangesGuessState()
            {
                var sut = Fixture.WithUnitNr(1).WithNumberOfAlternatives(3).CreateSut();

                Check.That(sut.CurrentGuessState).IsEqualTo(GuessState.NoGuess);

                var guessCommand = sut.GuessCommand;
                guessCommand.Execute(sut.Alternatives[0]);

                Check.That(sut.CurrentGuessState).IsNotEqualTo(GuessState.NoGuess);
            }
        }

        [TestFixture]
        internal class Guess : BaseClass
        {
            [Test]
            public void ChangesGuessState()
            {
                var sut = Fixture.WithUnitNr(1).WithNumberOfAlternatives(3).CreateSut();

                Check.That(sut.CurrentGuessState).IsEqualTo(GuessState.NoGuess);

                var guess = sut.Guess(sut.Alternatives[0]);

                Check.That(sut.CurrentGuessState).IsNotEqualTo(GuessState.NoGuess);
            }

            [Test]
            public void SetsGuessStateToCorrectIfCorrectAnswerIsGiven()
            {
                var sut = Fixture.WithUnitNr(1).WithNumberOfAlternatives(3).CreateSut();

                var correctGuess = sut.Alternatives.FirstOrDefault(_ => _.IsCorrect);

                var guess = sut.Guess(correctGuess);

                Check.That(sut.CurrentGuessState).IsEqualTo(GuessState.Correct);
            }

            [Test]
            public void SetsGuessStateToIncorrectIfIncorrectAnswerIsGiven()
            {
                var sut = Fixture.WithUnitNr(1).WithNumberOfAlternatives(3).CreateSut();

                var correctGuess = sut.Alternatives.FirstOrDefault(_ => !_.IsCorrect);

                var guess = sut.Guess(correctGuess);

                Check.That(sut.CurrentGuessState).IsEqualTo(GuessState.Incorrect);
            }

            [Test]
            public void UpdatesGuessStateToNoGuessInstantaniouslyIfWaitTimeIsSetToZero()
            {
                var sut = Fixture.WithUnitNr(1).WithNumberOfAlternatives(3).CreateSut();
                sut.WaitTime = 0;

                var correctGuess = sut.Alternatives.FirstOrDefault(_ => !_.IsCorrect);

                var guess = sut.Guess(correctGuess);

                Check.That(sut.CurrentGuessState).IsEqualTo(GuessState.NoGuess);
            }

        }
    }
}
