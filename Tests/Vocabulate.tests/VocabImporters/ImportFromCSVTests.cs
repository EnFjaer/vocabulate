﻿using System.IO;
using NFluent;
using NUnit.Framework;
using Vocabulate.TeachYourselfHindi.Core.VocabImporters;

namespace Vocabulate.TeachYourselfHindi.Core.Tests.VocabImporters
{
    namespace ImportFromCSVTests
    {
        internal class BaseClass
        {
            protected readonly string TestFile = @"C:\Workspace\Vocabulate\Tests\Vocabulate.tests\TestData\unit1.csv";
            protected Stream CsvFile;


            [SetUp]
            public void RunBeforeAnyTest()            
            {
                CsvFile = File.Open(TestFile, FileMode.Open, FileAccess.Read);
            }

            [TearDown]
            public void RunAfterAnyTest()
            {
                CsvFile.Close();
            }
        }

        [TestFixture]
        internal class GetVocabular : BaseClass
        {
            [Test]
            public void ReturnsANonEmptyList()
            {
                var asList = ImportFromCsv.GetVocabular(CsvFile);

                Check.That(asList.Count).IsGreaterThan(0);
            }

            [Test]
            public void ReturnsAListWithCorrectNrOfElements()
            {
                var asList = ImportFromCsv.GetVocabular(CsvFile);

                Check.That(asList.Count).IsEqualTo(8);
            }

            [TestCase(0, "अलग")]
            [TestCase(1, "आठ")]
            [TestCase(2, "उर्दू")]
            public void ReturnsAListWithCorrectWord(int index, string expectedWord)
            {
                var asList = ImportFromCsv.GetVocabular(CsvFile);

                Check.That(asList[index].Word).IsEqualTo(expectedWord);

            }
        }

    }
}
