﻿namespace Vocabulate.HindiNumbers.Core.NumberImporter
{
    public struct HindiNumber
    {
        public HindiNumber(string alphanumeric, string hindi, string transliteration, string tier)
        {
            Alphanumeric = alphanumeric;
            Hindi = hindi;
            Transliteration = transliteration;
            Tier = tier;
        }
        public string Alphanumeric { get; set; }
        public string Hindi { get; set; }
        public string Transliteration { get; set; }
        public string Tier { get; set; }
    }
}
