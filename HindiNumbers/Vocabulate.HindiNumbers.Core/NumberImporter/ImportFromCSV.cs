﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;

namespace Vocabulate.HindiNumbers.Core.NumberImporter
{
    public static class ImportFromCsv
    {
        public static List<HindiNumber> GetNumbers(Stream file)
        {

            try
            {
                using (var sr = new StreamReader(file))
                {
                    var config = new CsvConfiguration();
                    config.Delimiter = "|";
                    var reader = new CsvReader(sr, config);

                    //CSVReader will now read the whole file into an enumerable
                    IEnumerable<HindiNumber> records = reader.GetRecords<HindiNumber>();

                    return records.ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
