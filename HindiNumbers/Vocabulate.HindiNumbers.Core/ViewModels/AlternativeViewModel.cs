﻿
namespace Vocabulate.HindiNumbers.Core.ViewModels
{
    public class AlternativeViewModel : Vocabulate.CommonViewModels.ViewModels.AlternativeViewModel
    {
        public AlternativeViewModel(string alternative, bool isCorrect) : base(alternative, isCorrect)
        {
        }
    }
}
