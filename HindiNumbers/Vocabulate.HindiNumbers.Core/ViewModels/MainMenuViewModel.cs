﻿using System;
using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Platform.UI;
using MvvmCross.Plugins.WebBrowser;
using Vocabulate.StyleHelper.ColorSchemes;

namespace Vocabulate.HindiNumbers.Core.ViewModels
{
    public class MainMenuViewModel : MvxViewModel
    {
        private readonly IMvxWebBrowserTask _webBrowser;
        private const string ZabaanOnline = "http://www.zabaan.com";


        public MainMenuViewModel()
        {
            _webBrowser = Mvx.Resolve<IMvxWebBrowserTask>(); ;
        }

        public string Hindi2AlphanumericName => "सात -> 7";
        public string Alphanumeric2HindiName => "7 -> सात";
        public string ListOfNumbersName => "List of numbers";
        public string SettingsName => "";
        public string AboutZabaanName => "About Zabaan";
        public MvxColor ButtonColor => ColorConstants.ZabaanLightColor;
        public MvxColor ZabaanButtonColor => ColorConstants.ZabaanColor;

        public ICommand ListOfNumbersCommand => new MvxCommand(() => ShowViewModel<ListOfNumbersViewModel>());
        public ICommand HindiToAlphanumericCommand => new MvxCommand(() =>
        {
            KeeperOfNumbers.NumberQuestions.FlipQuestionAndAnswer = true;
            ShowViewModel<NumberQuizViewModel>();
        });

        public ICommand AlphanumericToHindiCommand => new MvxCommand(() =>
        {
            KeeperOfNumbers.NumberQuestions.FlipQuestionAndAnswer = false;
            ShowViewModel<NumberQuizViewModel>();
        });

        public ICommand OpenZabaanOnWeb => new MvxCommand(() => _webBrowser.ShowWebPage(ZabaanOnline));
    }
}