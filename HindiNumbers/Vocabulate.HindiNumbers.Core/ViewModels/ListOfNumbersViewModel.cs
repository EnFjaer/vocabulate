﻿using System.Collections.Generic;
using MvvmCross.Core.ViewModels;
using Vocabulate.CommonViewModels.ViewModels;
using Vocabulate.StyleHelper;

namespace Vocabulate.HindiNumbers.Core.ViewModels
{
    public class ListOfNumbersViewModel : MvxViewModel
    {

        public List<IListQuestionPair> Numbers
        {
            get
            {
                KeeperOfNumbers.NumberQuestions.FlipQuestionAndAnswer = false;
                return CreateListOfNumbers();
            }
        }

        public MvxCommand<IListQuestionPair> NoCommand => new MvxCommand<IListQuestionPair>(_ => { }, _ => true);

        public bool NotEnabled => false;

        private List<IListQuestionPair> CreateListOfNumbers()
        {
            var listOfNumbers = new List<IListQuestionPair>();
            foreach (var number in KeeperOfNumbers.NumberQuestions)
            {
                listOfNumbers.Add(new ListQuestionPair(number, KeeperOfNumbers.AnswerHistory));
            }

            return listOfNumbers;
        }
    }
}
