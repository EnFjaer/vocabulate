﻿using System;
using System.Collections.Generic;
using Vocabulate.HindiNumbers.Core.NumberImporter;
using Vocabulate.QuizEngine.Containers;
using Vocabulate.QuizEngine.QuestionHistory;

namespace Vocabulate.HindiNumbers.Core
{
    public static class KeeperOfNumbers
    {
        private static object _lock = new Object();

        private const int MaxNrOfAnswerStored = 5;

        private static List<HindiNumber> _numbers;

        public static IQuestionCollection NumberQuestions { get; private set; }

        public static IAnswerHistory AnswerHistory { get; private set; }

        public static bool IsInitialized { get; private set; }

        public static void Initialize(List<HindiNumber> numbers, IAnswerDatabaseConnection answerDatabaseConnection = null)
        {
            lock (_lock)
            {
                if (IsInitialized)
                    return;

                AnswerHistory = answerDatabaseConnection != null ? new AnswerHistory(new AnswerWriter(answerDatabaseConnection), new AnswerReader(answerDatabaseConnection), MaxNrOfAnswerStored) : new AnswerHistory();

                _numbers = numbers;

                if (_numbers != null && _numbers.Count > 0)
                {
                    ConvertNumbersToQuestions();
                    IsInitialized = true;
                }
            }
        }

        public static void Uninitialize()
        {
            lock (_lock)
            {
                _numbers = null;
                NumberQuestions = null;
                IsInitialized = false;
            }
        }

        private static void ConvertNumbersToQuestions()
        {
            List<IQuestionPair> numberPairs = new List<IQuestionPair>();

            foreach (var number in _numbers)
            {
                numberPairs.Add(new QuestionPair(number.Alphanumeric, number.Hindi, int.Parse(number.Tier)));
            }

            NumberQuestions = new QuestionCollection("Hindi Numbers", numberPairs);
        }
    }
}
