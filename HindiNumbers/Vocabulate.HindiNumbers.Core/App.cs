﻿using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.IoC;
using Vocabulate.HindiNumbers.Core.ViewModels;

namespace Vocabulate.HindiNumbers.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            RegisterAppStart<MainMenuViewModel>();
        }
    }
}