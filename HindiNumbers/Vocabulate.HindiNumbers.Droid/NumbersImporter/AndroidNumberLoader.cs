using System.Collections.Generic;
using Android.Content;
using Vocabulate.HindiNumbers.Core.NumberImporter;

namespace Vocabulate.HindiNumbers.Droid.NumbersImporter
{
    public class AndroidNumberLoader
    {
        private readonly Context _context;

        public AndroidNumberLoader(Context context)
        {
            _context = context;
        }

        public List<HindiNumber> Load(string filename)
        {
            var fileStream = _context.Assets.Open(filename);
            return ImportFromCsv.GetNumbers(fileStream);

        }
    }
}