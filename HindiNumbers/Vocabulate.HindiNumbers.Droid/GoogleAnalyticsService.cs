﻿using Android.Content;
using Android.Gms.Analytics;
using Vocabulate.QuizEngine.Analytics;

namespace Vocabulate.HindiNumbers.Droid
{
    public class GoogleAnalyticsService : IAnalyticsService
    {
        public string TrackingId = "UA-100018721-1";

        private static GoogleAnalytics AnalysisInstance;
        private static Tracker AnalysisTracker;
        private static GoogleAnalyticsService ThisRef;


        private GoogleAnalyticsService()
        {
        }

        public static GoogleAnalyticsService GetInstance()
        {
            if (ThisRef == null)
                ThisRef = new GoogleAnalyticsService();
            return ThisRef;
        }

        public void Initialize(Context context)
        {
            AnalysisInstance = GoogleAnalytics.GetInstance(context.ApplicationContext);
            AnalysisInstance.SetLocalDispatchPeriod(10);

            AnalysisTracker = AnalysisInstance.NewTracker(TrackingId);
            AnalysisTracker.EnableExceptionReporting(true);
            AnalysisTracker.EnableAdvertisingIdCollection(true);
            AnalysisTracker.EnableAutoActivityTracking(true);
        }

        public void TrackAppPage(string pageNameToTrack)
        {
            AnalysisTracker.SetScreenName(pageNameToTrack);
            AnalysisTracker.Send(new HitBuilders.ScreenViewBuilder().Build());
        }

        public void TrackAppEvent(string eventCategory, string eventToTrack)
        {
            HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder();
            builder.SetCategory(eventCategory);
            builder.SetAction(eventToTrack);
            builder.SetLabel("AppEvent");

            AnalysisTracker.Send(builder.Build());
        }

        public void TrackAppException(string exceptionMessageToTrack, bool isFatalException)
        {
            HitBuilders.ExceptionBuilder builder = new HitBuilders.ExceptionBuilder();
            builder.SetDescription(exceptionMessageToTrack);
            builder.SetFatal(isFatalException);

            AnalysisTracker.Send(builder.Build());
        }
    }

}