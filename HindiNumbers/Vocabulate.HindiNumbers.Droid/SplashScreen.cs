using Android.App;
using MvvmCross.Droid.Views;

namespace Vocabulate.HindiNumbers.Droid
{
    [Activity(Label = "Zabaan: Hindi Numbers", MainLauncher = true, Icon = "@drawable/icon")]
    public class SplashScreen : MvxSplashScreenActivity
    {
        public SplashScreen()
            : base(Resource.Layout.SplashScreen)
        {

        }
    }
}