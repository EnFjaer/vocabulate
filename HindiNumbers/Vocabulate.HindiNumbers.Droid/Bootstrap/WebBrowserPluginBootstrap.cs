using MvvmCross.Platform.Plugins;

namespace Vocabulate.HindiNumbers.Droid.Bootstrap
{
    public class WebBrowserPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.WebBrowser.PluginLoader>
    {
    }
}