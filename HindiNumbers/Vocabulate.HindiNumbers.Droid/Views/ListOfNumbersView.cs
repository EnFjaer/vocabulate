﻿using Android.App;
using Android.OS;
using MvvmCross.Droid.Views;

namespace Vocabulate.HindiNumbers.Droid.Views
{
    [Activity(Label = "Zabaan: Hindi Numbers", Theme = "@android:style/Theme.NoTitleBar")]
    public class ListOfNumbersView : MvxActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.ListOfNumbersView);
        }
    }
}