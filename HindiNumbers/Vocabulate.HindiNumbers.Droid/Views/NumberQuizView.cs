﻿using Android.App;
using Android.OS;
using Android.Widget;
using MvvmCross.Binding.Droid.Views;
using Vocabulate.Common.Droid.Views;
using Vocabulate.HindiNumbers.Core;
using Vocabulate.HindiNumbers.Core.ViewModels;
using Vocabulate.QuizEngine.ChoosersAndGenerators;

namespace Vocabulate.HindiNumbers.Droid.Views
{
    [Activity(Label = "Zabaan: Hindi Numbers", Theme = "@android:style/Theme.NoTitleBar")]
    public class NumberQuizView : QuizView
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            var viewModel = ViewModel as NumberQuizViewModel;

            
            GoogleAnalyticsService.GetInstance().TrackAppPage("NumberQuiz");

            SetContentView(Resource.Layout.NumberQuiz);

            Initialize(FindViewById<MvxGridView>(Resource.Id.quizGrid),
                FindViewById<LinearLayout>(Resource.Id.questionLayout),
                FindViewById<TextView>(Resource.Id.collectionName),
                FindViewById<TextView>(Resource.Id.questionView));
        }

        protected override void SetupViewmodel()
        {
            var quizViewModel = ViewModel as NumberQuizViewModel;
            var numberChooser = new QuestionChooser(KeeperOfNumbers.NumberQuestions, KeeperOfNumbers.AnswerHistory, GoogleAnalyticsService.GetInstance());
            quizViewModel?.Setup(KeeperOfNumbers.NumberQuestions, new AlternativeGenerator(numberChooser, KeeperOfNumbers.NumberQuestions), numberChooser, numberChooser);
        }
    }
}