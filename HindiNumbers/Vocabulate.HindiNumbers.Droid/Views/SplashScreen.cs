using System.IO;
using Android.App;
using Android.OS;
using HockeyApp.Android;
using HockeyApp.Android.Metrics;
using MvvmCross.Droid.Views;
using Vocabulate.HindiNumbers.Core;
using Vocabulate.HindiNumbers.Droid.NumbersImporter;
using Vocabulate.QuizEngine.Database;

namespace Vocabulate.HindiNumbers.Droid.Views
{
    [Activity(Label = "Zabaan: Hindi Numbers", MainLauncher = true, Icon = "@drawable/icon", NoHistory = true)]
    public class SplashScreen : MvxSplashScreenActivity
    {
        private const string AnswerDatabasePath = "./answerDatabase.db3";
        private const string AppIdentifier = "740462822cc74a379e9e6a0dd261348b";

        public SplashScreen()
            : base(Resource.Layout.SplashScreen)
        {
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            InitHockeyApp();
            InitKeeperOfNumbers();
            GoogleAnalyticsService.GetInstance().Initialize(this);
        }

        private void InitHockeyApp()
        {
            CrashManager.Register(this, AppIdentifier);
            MetricsManager.Register(Application, AppIdentifier);
        }

        private void InitKeeperOfNumbers()
        {
            if (!KeeperOfNumbers.IsInitialized)
            {
                var numberLoader = new AndroidNumberLoader(this);
                var numbers = numberLoader.Load("HindiNumbers.csv");
                var path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                path = Path.Combine(path, AnswerDatabasePath);
                var answerConnection = new AnswerDatabaseConnection(path);
                KeeperOfNumbers.Initialize(numbers, answerConnection);
            }
        }
    }
}